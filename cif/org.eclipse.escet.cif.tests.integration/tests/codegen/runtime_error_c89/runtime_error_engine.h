/* Headers for the CIF to C translation of runtime_error.cif
 * Generated file, DO NOT EDIT
 */

#ifndef CIF_C_RUNTIME_ERROR_ENGINE_H
#define CIF_C_RUNTIME_ERROR_ENGINE_H

#include "runtime_error_library.h"

/* Types of the specification.
 * Note that integer ranges are ignored in C.
 */
enum Enumruntime_error_ {
    /** Literal "__some_dummy_enum_literal". */
    _runtime_error___some_dummy_enum_literal,
};
typedef enum Enumruntime_error_ runtime_errorEnum;

extern const char *enum_names[];
extern int EnumTypePrint(runtime_errorEnum value, char *dest, int start, int end);


/* Event declarations. */
enum runtime_errorEventEnum_ {
    /** Initial step. */
    EVT_INITIAL_,

    /** Delay step. */
    EVT_DELAY_,

    /** Event "e". */
    e_,
};
typedef enum runtime_errorEventEnum_ runtime_error_Event_;

/** Names of all the events. */
extern const char *runtime_error_event_names[];

/* Constants. */


/* Input variables. */

/** Input variable "int svg.i". */
extern IntType svg_i_;

extern void runtime_error_AssignInputVariables();

/* Declaration of internal functions. */

/**
 * Function "g.fail".
 *
 * @param g_fail_x_ Function parameter "g.fail.x".
 * @return The return value of the function.
 */
extern IntType g_fail_(IntType g_fail_x_);

/* State variables (use for output only). */
extern RealType model_time; /**< Current model time. */

/** Continuous variable "real g.cont_var1". */
extern RealType g_cont_var1_;

/** Discrete variable "int[0..3] a.x". */
extern IntType a_x_;

/** Discrete variable "int[0..3] a.y". */
extern IntType a_y_;

/** Continuous variable "real g.cont_var2". */
extern RealType g_cont_var2_;

/* Algebraic and derivative functions (use for output only). */
RealType g_cont_var1_deriv(void);
RealType g_cont_var2_deriv(void);
IntType g_alg_var_(void);


/* Code entry points. */
void runtime_error_EngineFirstStep(void);
void runtime_error_EngineTimeStep(double delta);

#if EVENT_OUTPUT
/**
 * External callback function reporting about the execution of an event.
 * @param event Event being executed.
 * @param pre If \c TRUE, event is about to be executed. If \c FALSE, event has been executed.
 * @note Function must be implemented externally.
 */
extern void runtime_error_InfoEvent(runtime_error_Event_ event, BoolType pre);
#endif

#if PRINT_OUTPUT
/**
 * External callback function to output the given text-line to the given filename.
 * @param text Text to print (does not have a EOL character).
 * @param fname Name of the file to print to.
 */
extern void runtime_error_PrintOutput(const char *text, const char *fname);
#endif

#endif

