/* CIF to C translation of runtime_error.cif
 * Generated file, DO NOT EDIT
 */

#include <stdio.h>
#include <stdlib.h>
#include "runtime_error_engine.h"

#ifndef MAX_NUM_ITERS
#define MAX_NUM_ITERS 1000
#endif

/* What to do if a range error is found in an assignment? */
#ifdef KEEP_RUNNING
static inline void RangeErrorDetected(void) { /* Do nothing, error is already reported. */ }
#else
static inline void RangeErrorDetected(void) { exit(1); }
#endif

/* Type support code. */
int EnumTypePrint(runtime_errorEnum value, char *dest, int start, int end) {
    int last = end - 1;
    const char *lit_name = enum_names[value];
    while (start < last && *lit_name) {
        dest[start++] = *lit_name;
        lit_name++;
    }
    dest[start] = '\0';
    return start;
}


/** Event names. */
const char *runtime_error_event_names[] = {
    "initial-step", /**< Initial step. */
    "delay-step",   /**< Delay step. */
    "e",            /**< Event "e". */
};

/** Enumeration names. */
const char *enum_names[] = {
    /** Literal "__some_dummy_enum_literal". */
    "__some_dummy_enum_literal",
};

/* Constants. */


/* Functions. */

/**
 * Function "g.fail".
 *
 * @param g_fail_x_ Function parameter "g.fail.x".
 * @return The return value of the function.
 */
IntType g_fail_(IntType g_fail_x_) {
    // Variable "g.fail.y".
    IntType g_fail_y_;
    g_fail_y_ = IntegerAdd(g_fail_x_, 1);

    // Execute statements in the function body.
    return IntegerDiv(g_fail_y_, g_fail_x_);
    assert(0); /* Falling through the end of the function. */
}

/* Input variables. */

/** Input variable "int svg.i". */
IntType svg_i_;

/* State variables. */

/** Continuous variable "real g.cont_var1". */
RealType g_cont_var1_;

/** Discrete variable "int[0..3] a.x". */
IntType a_x_;

/** Discrete variable "int[0..3] a.y". */
IntType a_y_;

/** Continuous variable "real g.cont_var2". */
RealType g_cont_var2_;

RealType model_time; /**< Current model time. */

/** Initialize constants. */
static void InitConstants(void) {

}

/** Print function. */
#if PRINT_OUTPUT
static void PrintOutput(runtime_error_Event_ event, BoolType pre) {
    StringType text_var1;

    if (pre) {
        if ((IntegerDiv(a_x_, a_y_)) > (0)) {
            StringTypeCopyText(&(text_var1), "txt");
            runtime_error_PrintOutput(text_var1.data, ":stdout");
        }

        {
            char scratch[(MAX_STRING_SIZE + 1) > 128 ? (MAX_STRING_SIZE + 1) : 128]; /* Value scratch space. */
            int index = 0;
            snprintf(scratch, sizeof(scratch), "%d", IntegerDiv(a_x_, a_y_));
            index = StringTypeAppendText(&text_var1, index, FMTFLAGS_NONE, 0, scratch);
        }
        runtime_error_PrintOutput(text_var1.data, ":stdout");
    } else {
        if ((IntegerDiv(a_x_, a_y_)) > (0)) {
            StringTypeCopyText(&(text_var1), "txt");
            runtime_error_PrintOutput(text_var1.data, ":stdout");
        }

        {
            char scratch[(MAX_STRING_SIZE + 1) > 128 ? (MAX_STRING_SIZE + 1) : 128]; /* Value scratch space. */
            int index = 0;
            snprintf(scratch, sizeof(scratch), "%d", IntegerDiv(a_x_, a_y_));
            index = StringTypeAppendText(&text_var1, index, FMTFLAGS_NONE, 0, scratch);
        }
        runtime_error_PrintOutput(text_var1.data, ":stdout");

        IntTypePrint(g_alg_var_(), text_var1.data, 0, MAX_STRING_SIZE);
        runtime_error_PrintOutput(text_var1.data, ":stdout");

        IntTypePrint(g_fail_(0), text_var1.data, 0, MAX_STRING_SIZE);
        runtime_error_PrintOutput(text_var1.data, ":stdout");
    }
}
#endif

/* Edge execution code. */

/**
 * Execute code for edge with index 0 and event "e".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge0(void) {
    #if PRINT_OUTPUT
        PrintOutput(e_, TRUE);
    #endif
    #if EVENT_OUTPUT
        runtime_error_InfoEvent(e_, TRUE);
    #endif

    if (TRUE) {
        a_x_ = a_x_;
    } else if ((RealDivision(a_x_, a_y_)) == (5.0)) {
        IntType rhs2 = (a_x_) + (1);
        #if CHECK_RANGES
        if ((rhs2) > 3) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..3]\"\n", rhs2);
            fprintf(stderr, "            at " "a.x" "\n");
            RangeErrorDetected();
        }
        #endif
        a_x_ = rhs2;
    } else if (TRUE) {
        a_x_ = IntegerDiv(a_x_, a_y_);
    }

    #if EVENT_OUTPUT
        runtime_error_InfoEvent(e_, FALSE);
    #endif
    #if PRINT_OUTPUT
        PrintOutput(e_, FALSE);
    #endif
    return TRUE;
}

/**
 * Normalize and check the new value of a continuous variable after an update.
 * @param new_value Unnormalized new value of the continuous variable.
 * @param var_name Name of the continuous variable in the CIF model.
 * @return The normalized new value of the continuous variable.
 */
static inline RealType UpdateContValue(RealType new_value, const char *var_name) {
    if (isfinite(new_value)) {
        return (new_value == -0.0) ? 0.0 : new_value;
    }

    const char *err_type;
    if (isnan(new_value)) {
        err_type = "NaN";
    } else if (new_value > 0) {
        err_type = "+inf";
    } else {
        err_type = "-inf";
    }
    fprintf(stderr, "Continuous variable \"%s\" has become %s.\n", var_name, err_type);

#ifdef KEEP_RUNNING
    return 0.0;
#else
    exit(1);
#endif
}

/** Repeatedly perform discrete event steps, until no progress can be made any more. */
static void PerformEdges(void) {
    /* Uncontrollables. */
    int count = 0;
    for (;;) {
        count++;
        if (count > MAX_NUM_ITERS) { /* 'Infinite' loop detection. */
            fprintf(stderr, "Warning: Quitting after performing %d uncontrollable events, infinite loop?\n", count);
            break;
        }

        BoolType edgeExecuted = false;



        if (!edgeExecuted) {
            break; /* No edge fired, done with discrete steps. */
        }
    }

    /* Controllables. */
    count = 0;
    for (;;) {
        count++;
        if (count > MAX_NUM_ITERS) { /* 'Infinite' loop detection. */
            fprintf(stderr, "Warning: Quitting after performing %d controllable events, infinite loop?\n", count);
            break;
        }

        BoolType edgeExecuted = false;

        edgeExecuted |= execEdge0(); /* (Try to) perform edge with index 0 and event "e". */

        if (!edgeExecuted) {
            break; /* No edge fired, done with discrete steps. */
        }
    }
}

/** First model call, initializing, and performing discrete events before the first time step. */
void runtime_error_EngineFirstStep(void) {
    InitConstants();

    model_time = 0.0;
    runtime_error_AssignInputVariables();
    g_cont_var1_ = 0.0;
    a_x_ = 0;
    a_y_ = 0;
    g_cont_var2_ = RealDivision(a_x_, a_y_);

    #if PRINT_OUTPUT
        /* pre-initial and post-initial prints. */
        PrintOutput(EVT_INITIAL_, TRUE);
        PrintOutput(EVT_INITIAL_, FALSE);
    #endif

    PerformEdges();

    #if PRINT_OUTPUT
        /* pre-timestep print. */
        PrintOutput(EVT_DELAY_, TRUE);
    #endif
}

/**
 * Engine takes a time step of length \a delta.
 * @param delta Length of the time step.
 */
void runtime_error_EngineTimeStep(double delta) {
    runtime_error_AssignInputVariables();

    /* Update continuous variables. */
    if (delta > 0.0) {
        RealType deriv0 = g_cont_var1_deriv();
        RealType deriv1 = g_cont_var2_deriv();

        g_cont_var1_ = UpdateContValue(g_cont_var1_ + delta * deriv0, "g.cont_var1");
        g_cont_var2_ = UpdateContValue(g_cont_var2_ + delta * deriv1, "g.cont_var2");
        model_time += delta;
    }

    #if PRINT_OUTPUT
        /* post-timestep print. */
        PrintOutput(EVT_DELAY_, FALSE);
    #endif

    PerformEdges();

    #if PRINT_OUTPUT
        /* pre-timestep print. */
        PrintOutput(EVT_DELAY_, TRUE);
    #endif
}

