/* Additional code to allow compilation and test of the generated code.
 *
 * This file is generated, DO NOT EDIT
 */

#include <stdio.h>
#include "runtime_error_engine.h"

/* Assign values to the input variables. */
void runtime_error_AssignInputVariables(void) {
    /* Input variable "svg.i". */
    svg_i_ = 0;
}

void runtime_error_InfoEvent(runtime_error_Event_ event, BoolType pre) {
    const char *prePostText = pre ? "pre" : "post";
    printf("Executing %s-event \"%s\"\n", prePostText, runtime_error_event_names[event]);
}

void runtime_error_PrintOutput(const char *text, const char *fname) {
    printf("Print @ %s: \"%s\"\n", fname, text);
}

int main(void) {
    runtime_error_EngineFirstStep();

    runtime_error_EngineTimeStep(1.0);
    return 0;
}

