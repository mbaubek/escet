/* Headers for the CIF to C translation of _0_code_prefix_auto__derive.cif
 * Generated file, DO NOT EDIT
 */

#ifndef CIF_C__0_CODE_PREFIX_AUTO__DERIVE_ENGINE_H
#define CIF_C__0_CODE_PREFIX_AUTO__DERIVE_ENGINE_H

#include "_0_code_prefix_auto__derive_library.h"

/* Types of the specification.
 * Note that integer ranges are ignored in C.
 */
enum Enum_0_code_prefix_auto__derive_ {
    /** Literal "__some_dummy_enum_literal". */
    __0_code_prefix_auto__derive___some_dummy_enum_literal,
};
typedef enum Enum_0_code_prefix_auto__derive_ _0_code_prefix_auto__deriveEnum;

extern const char *enum_names[];
extern int EnumTypePrint(_0_code_prefix_auto__deriveEnum value, char *dest, int start, int end);


/* Event declarations. */
enum _0_code_prefix_auto__deriveEventEnum_ {
    /** Initial step. */
    EVT_INITIAL_,

    /** Delay step. */
    EVT_DELAY_,
};
typedef enum _0_code_prefix_auto__deriveEventEnum_ _0_code_prefix_auto__derive_Event_;

/** Names of all the events. */
extern const char *_0_code_prefix_auto__derive_event_names[];

/* Constants. */


/* Input variables. */




/* Declaration of internal functions. */


/* State variables (use for output only). */
extern RealType model_time; /**< Current model time. */


/* Algebraic and derivative functions (use for output only). */






/* Code entry points. */
void _0_code_prefix_auto__derive_EngineFirstStep(void);
void _0_code_prefix_auto__derive_EngineTimeStep(double delta);

#if EVENT_OUTPUT
/**
 * External callback function reporting about the execution of an event.
 * @param event Event being executed.
 * @param pre If \c TRUE, event is about to be executed. If \c FALSE, event has been executed.
 * @note Function must be implemented externally.
 */
extern void _0_code_prefix_auto__derive_InfoEvent(_0_code_prefix_auto__derive_Event_ event, BoolType pre);
#endif

#if PRINT_OUTPUT
/**
 * External callback function to output the given text-line to the given filename.
 * @param text Text to print (does not have a EOL character).
 * @param fname Name of the file to print to.
 */
extern void _0_code_prefix_auto__derive_PrintOutput(const char *text, const char *fname);
#endif

#endif

