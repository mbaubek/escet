/* Additional code to allow compilation and test of the generated code.
 *
 * This file is generated, DO NOT EDIT
 */

#include <stdio.h>
#include "_0_code_prefix_auto__derive_engine.h"

/* Assign values to the input variables. */
void _0_code_prefix_auto__derive_AssignInputVariables(void) {

}

void _0_code_prefix_auto__derive_InfoEvent(_0_code_prefix_auto__derive_Event_ event, BoolType pre) {
    const char *prePostText = pre ? "pre" : "post";
    printf("Executing %s-event \"%s\"\n", prePostText, _0_code_prefix_auto__derive_event_names[event]);
}

void _0_code_prefix_auto__derive_PrintOutput(const char *text, const char *fname) {
    printf("Print @ %s: \"%s\"\n", fname, text);
}

int main(void) {
    _0_code_prefix_auto__derive_EngineFirstStep();

    _0_code_prefix_auto__derive_EngineTimeStep(1.0);
    return 0;
}

