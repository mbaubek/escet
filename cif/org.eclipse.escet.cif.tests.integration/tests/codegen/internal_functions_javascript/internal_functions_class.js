/** Tuples. */


            /** Tuple class for CIF tuple type representative "tuple(int; real)". */
            class CifTuple_T2IR {
                /** The 1st field. */
                _field0;

                /** The 2nd field. */
                _field1;

                /**
                 * Constructor for the {@link CifTuple_T2IR} class.
                 *
                 * @param _field0 The 1st field.
                 * @param _field1 The 2nd field.
                 */
                constructor(_field0, _field1) {
                    this._field0 = _field0;
                    this._field1 = _field1;
                }

                copy() {
                    return new CifTuple_T2IR(this._field0, this._field1);
                }

                toString() {
                    var rslt = '(';
                    rslt += internal_functionsUtils.valueToStr(this._field0);
                    rslt += ', ';
                    rslt += internal_functionsUtils.valueToStr(this._field1);
                    rslt += ')';
                    return rslt;
                }
            }


            /** Tuple class for CIF tuple type representative "tuple(int g; int h)". */
            class CifTuple_T2II {
                /** The 1st field. */
                _field0;

                /** The 2nd field. */
                _field1;

                /**
                 * Constructor for the {@link CifTuple_T2II} class.
                 *
                 * @param _field0 The 1st field.
                 * @param _field1 The 2nd field.
                 */
                constructor(_field0, _field1) {
                    this._field0 = _field0;
                    this._field1 = _field1;
                }

                copy() {
                    return new CifTuple_T2II(this._field0, this._field1);
                }

                toString() {
                    var rslt = '(';
                    rslt += internal_functionsUtils.valueToStr(this._field0);
                    rslt += ', ';
                    rslt += internal_functionsUtils.valueToStr(this._field1);
                    rslt += ')';
                    return rslt;
                }
            }

/** internal_functions code generated from a CIF specification. */
class internal_functions_class {
    /** internal_functionsEnum declaration. It contains the single merged enum from the CIF model. */
    internal_functionsEnum = Object.freeze({
        /** Literal "__some_dummy_enum_literal". */
        ___some_dummy_enum_literal: Symbol("__some_dummy_enum_literal")
    })

    /** Should execution timing information be provided? */
    doInfoExec = true;

    /** Should executed event information be provided? */
    doInfoEvent = true;

    /** Should print output be provided? */
    doInfoPrintOutput = true;

    /** Should state output be provided? */
    doStateOutput = false;

    /** Should transition output be provided? */
    doTransitionOutput = true;

    /** Whether the constants have been initialized already. */
    constantsInitialized = false;

    /** Whether the SVG copy and move declarations have been applied already. */
    svgCopiesAndMovesApplied = false;

    /** Whether this is the first time the code is (to be) executed. */
    firstExec;

    /** The names of all the events. */
    EVENT_NAMES = [

    ];


    /** Variable 'time', tracks elapsed time for a session. */
    time;

    /**
     * The frequency in times per second, that the code should
     * be executed (if positive), or execute as fast as possible, that is
     * as many times per second as possible (if negative or zero).
     */
    frequency = 60;

    /**
     * Whether the next execution is the first execution of the session.
     * Used to initialize time-related variables for starting, pausing,
     * resuming or resetting each session.
     */
    first;

    /**
     * Whether the simulation is currently running, and should process
     * user input, or is paused.
     */
    playing;

    /** The start time of the current session. */
    startMilli;

    /**
     * The targeted end time of the current/next cycle, to ensure
     * that the duration of the cycle matches with the configured
     * frequency.
     */
    targetMilli;


    /** Discrete variable "aut.v00". */
    aut_v00_;

    /** Discrete variable "aut.v01". */
    aut_v01_;

    /** Discrete variable "aut.v02". */
    aut_v02_;

    /** Discrete variable "aut.v03". */
    aut_v03_;

    /** Discrete variable "aut.v04". */
    aut_v04_;

    /** Discrete variable "aut.v05". */
    aut_v05_;

    /** Discrete variable "aut.v06". */
    aut_v06_;

    /** Discrete variable "aut.v07". */
    aut_v07_;

    /** Discrete variable "aut.v08". */
    aut_v08_;

    /** Discrete variable "aut.v09". */
    aut_v09_;

    /** Discrete variable "aut.v10". */
    aut_v10_;

    /** Discrete variable "aut.v11". */
    aut_v11_;

    /** Discrete variable "aut.v12". */
    aut_v12_;

    /** Discrete variable "aut.v13". */
    aut_v13_;

    /** Discrete variable "aut.v14". */
    aut_v14_;

    /** Discrete variable "aut.v15". */
    aut_v15_;

    /** Discrete variable "aut.v16". */
    aut_v16_;

    /** Discrete variable "aut.v17". */
    aut_v17_;

    /** Discrete variable "aut.v18". */
    aut_v18_;

    /** Discrete variable "aut.v19". */
    aut_v19_;

    /** Discrete variable "aut.combi". */
    aut_combi_;


    /** SVG output elements. */


    /**
     * SVG input queue with functions for handling clicked SVG
     * input elements, from first clicked at the head of the array
     * to last clicked at the tail of the array.
     */
    svgInQueue;

    /**
     * The SVG input id corresponding to the SVG input element that
     * was clicked. Is 'null' if no SVG input element was clicked so
     * far, or all clicks have already been processed.
     */
    svgInId;

    /**
     * The 0-based index of the event corresponding to the SVG input
     * element that was clicked. Is '-1' if no SVG input element was
     * clicked so far, or all clicks have already been processed.
     */
    svgInEvent;

    /** SVG input click event handlers. */


    /** SVG input event setters. */


    /** Starts the simulation. */
    start() {
        if (!this.playing) {
            this.playing = true;
            this.exec();
        }
    }

    /** Stops the simulation. */
    stop() {
        if (this.playing) {
            this.playing = false;
        }
    }

    /** Resets the object to its initial state. */
    reset() {
        this.stop();
        this.firstExec = true;
        this.time = 0.0;
        this.first = true;
        this.timePaused = null;
        this.initState();
        this.initUI();
        this.updateUI();
    }

    /**
     * Execute the code once. Inputs are read, transitions are executed until
     * none are possible, outputs are written, etc.
     *
     * @param newTime The time in seconds, since the start of the first
     *      execution.
     * @throws {internal_functionsException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    execOnce(newTime) {
        // Pre execution notification.
        this.preExec();

        // Update values of input variables.
        this.updateInputs();

        // Initialize the state.
        if (this.firstExec) {
            this.initState();
        }

        // Calculate time delta.
        var delta = newTime - this.time;

        // Update values of continuous variables.
        if (!this.firstExec) {
                        // No continuous variables, except variable 'time'.
        }

        // Update time.
        this.time = newTime;

        // Apply print declarations.
        if (this.firstExec) {
            // For 'initial' transition.
            if (this.doInfoPrintOutput) this.printOutput(-3, true);
            if (this.doInfoPrintOutput) this.printOutput(-3, false);
            if (this.doStateOutput) internal_functions.log('Initial state: ' + internal_functions.getStateText());
            if (this.doStateOutput || this.doTransitionOutput) this.log('');

        } else {
            // For 'post' of time transition.
            if (this.doInfoPrintOutput) this.printOutput(-2, false);
        }

        // Execute environment events and SVG input events as long as they are possible, emptying the SVG input queue.
        while (true) {
            // Handle next element from SVG input queue, if not already already processing one.
            if (this.svgInEvent == -1 && this.svgInQueue.length > 0) {
                var func = this.svgInQueue.shift(); // Remove head of the queue.
                func(); // Call function, to set the event to allow.
            }

            // Try to execute an SVG input mapping with updates for each environment event, and an edge for each SVG
            // input event.
            var anythingExecuted = false;



            // Stop if no SVG input mapping and no edge was executed, and no more SVG input clicks are to be processed.
            if (!anythingExecuted && this.svgInQueue.length == 0) {
                break;
            }
        }

        // Make sure all outstanding SVG input clicks have been processed.
        console.assert(this.svgInEvent == -1 && this.svgInQueue.length == 0);

        // Execute uncontrollable edges as long as they are possible.
        while (true) {
            // Try to execute an edge for each event.
            var edgeExecuted = false;


            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Execute controllable edges as long as they are possible.
        while (true) {
            // Try to execute an edge for each event.
            var edgeExecuted = false;


            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Apply print declarations for 'pre' of time transition.
        if (this.doInfoPrintOutput) this.printOutput(-2, true);

        // Post execution notification.
        this.postExec();

        // Done.
        this.firstExec = false;
    }

    /**
     * Calls {@link #execWhile}, which repeatedly {@link #execOnce executes the code}.
     *
     * @throws {internal_functionsException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    exec() {
        this.execWhile(1);
    }

    /**
     * Repeatedly {@link #execOnce executes the code}.
     *
     * @param delay The delay before executing, in milliseconds.
     *
     * @throws {internal_functionsException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    execWhile(delay) {
        setTimeout(
            function () {
                // Pre execution timing.
                var now = Date.now();
                var preMilli = now;

                // On first execution, initialize variables for timing.
                if (internal_functions.first) {
                    internal_functions.first = false;
                    internal_functions.startMilli = now;
                    internal_functions.targetMilli = internal_functions.startMilli;
                    preMilli = internal_functions.startMilli;
                }

                // Handle pausing/playing.
                if (!internal_functions.playing) {
                    internal_functions.timePaused = now;
                    return;
                }

                if (internal_functions.timePaused) {
                    internal_functions.startMilli += (now - internal_functions.timePaused);
                    internal_functions.targetMilli += (now - internal_functions.timePaused);
                    internal_functions.timePaused = null;
                }

                // Get cycle time and current 'time'.
                var frequency = internal_functions.frequency;
                var cycleMilli = (frequency <= 0) ? -1 : 1e3 / frequency;
                var timeMilli = preMilli - internal_functions.startMilli;

                // Execute once.
                internal_functions.execOnce(timeMilli / 1e3);

                // Post execution timing.
                var postMilli = Date.now();
                var duration = postMilli - preMilli;
                if (internal_functions.doInfoExec) {
                    internal_functions.infoExec(duration, cycleMilli);
                }

                // Ensure frequency.
                var remainderMilli = 0;
                if (frequency > 0) {
                    internal_functions.targetMilli += cycleMilli;
                    remainderMilli = internal_functions.targetMilli - postMilli;
                }

                // Execute again.
                internal_functions.execWhile(remainderMilli > 0 ? remainderMilli : 0);
            },
        delay);
    }


    /**
     * Initializes the state.
     *
     * @throws {internal_functionsException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    initState() {
        // Initialize constants, if not yet done so.
        if (!this.constantsInitialized) {
            this.constantsInitialized = true;

        }

        // Initialize SVG input.
        this.svgInQueue = [];
        this.svgInId = null;
        this.svgInEvent = -1;


        // CIF model state variables.
        try {
            internal_functions.aut_v00_ = 5;
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to evaluate the initial value of variable \"aut.v00\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            internal_functions.aut_v01_ = internal_functions.inc_(internal_functions.aut_v00_);
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to evaluate the initial value of variable \"aut.v01\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            internal_functions.aut_v02_ = internal_functions.factorial_(5);
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to evaluate the initial value of variable \"aut.v02\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            internal_functions.aut_v03_ = internal_functions.rec1_(7);
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to evaluate the initial value of variable \"aut.v03\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            internal_functions.aut_v04_ = internal_functions.rec2_(7);
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to evaluate the initial value of variable \"aut.v04\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            internal_functions.aut_v05_ = internal_functionsUtils.addInt((internal_functions.multi_return_())._field0, internal_functionsUtils.floor((internal_functions.multi_return_())._field1));
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to evaluate the initial value of variable \"aut.v05\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            internal_functions.aut_v06_ = internal_functions.f0_();
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to evaluate the initial value of variable \"aut.v06\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            internal_functions.aut_v07_ = internal_functions.f1_(1);
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to evaluate the initial value of variable \"aut.v07\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            internal_functions.aut_v08_ = internal_functions.f2_(1, 2);
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to evaluate the initial value of variable \"aut.v08\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            internal_functions.aut_v09_ = internal_functions.f3_(1, 2, 3.0);
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to evaluate the initial value of variable \"aut.v09\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            internal_functions.aut_v10_ = internal_functions.locals_(1);
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to evaluate the initial value of variable \"aut.v10\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            internal_functions.aut_v11_ = internal_functions.rot1_([1, 2, 3, 4]);
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to evaluate the initial value of variable \"aut.v11\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            internal_functions.aut_v12_ = internal_functions.rot2_([1, 2, 3, 4]);
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to evaluate the initial value of variable \"aut.v12\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            internal_functions.aut_v13_ = internal_functions.fa_(1);
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to evaluate the initial value of variable \"aut.v13\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            internal_functions.aut_v14_ = internal_functions.fi_(1);
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to evaluate the initial value of variable \"aut.v14\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            internal_functions.aut_v15_ = internal_functions.fw_();
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to evaluate the initial value of variable \"aut.v15\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            internal_functions.aut_v16_ = internal_functions.fu1_();
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to evaluate the initial value of variable \"aut.v16\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            internal_functions.aut_v17_ = internal_functions.fu2_();
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to evaluate the initial value of variable \"aut.v17\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            internal_functions.aut_v18_ = internal_functions.fu3_();
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to evaluate the initial value of variable \"aut.v18\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            internal_functions.aut_v19_ = internal_functions.fr_();
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to evaluate the initial value of variable \"aut.v19\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            internal_functions.aut_combi_ = internal_functionsUtils.addInt(internal_functionsUtils.addInt(internal_functionsUtils.addInt(internal_functionsUtils.addInt(internal_functionsUtils.addInt(internal_functionsUtils.addInt(internal_functionsUtils.addInt(internal_functionsUtils.addInt(internal_functionsUtils.addInt(internal_functionsUtils.addInt(internal_functionsUtils.addInt(internal_functionsUtils.addInt(internal_functionsUtils.addInt(internal_functionsUtils.addInt(internal_functionsUtils.addInt(internal_functionsUtils.addInt(internal_functionsUtils.addInt(internal_functionsUtils.addInt(internal_functionsUtils.addInt(internal_functions.aut_v00_, internal_functions.aut_v01_), internal_functions.aut_v02_), internal_functions.aut_v03_), internal_functions.aut_v04_), internal_functions.aut_v05_), internal_functions.aut_v06_), internal_functions.aut_v07_), internal_functions.aut_v08_), internal_functionsUtils.floor(internal_functions.aut_v09_)), internal_functions.aut_v10_), internal_functionsUtils.projectList(internal_functions.aut_v11_, 0)), internal_functionsUtils.projectList(internal_functions.aut_v12_, 0)), internal_functions.aut_v13_), internal_functions.aut_v14_), internal_functions.aut_v15_), internal_functions.aut_v16_), internal_functions.aut_v17_), internal_functions.aut_v18_), internal_functions.aut_v19_);
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to evaluate the initial value of variable \"aut.combi\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Initializes the user interface, either when loading the page
     * or when resetting the simulation.
     */
    initUI() {


        // Apply SVG copies and moves, if not done so before.
        if (!this.svgCopiesAndMovesApplied) {
            this.svgCopiesAndMovesApplied = true;

            // Apply SVG copy declarations.


            // Apply SVG move declarations.

        }

        // Prepare SVG output.


        // Prepare SVG input.

    }

    /**
     * Updates the user interface based on the latest state of
     * the model. Is called at the end of each cycle.
     *
     * @throws {internal_functionsException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    updateUI() {
        // Apply CIF/SVG output mappings.

    }

    /**
     * Updates the values of the input variables. Other variables from the
     * state may not be accessed or modified.
     */
    updateInputs() {
        // Nothing is done here by default.
    }

    /** Logs a normal message. */
    log(message) {
        console.log(message);

    }

    /** Logs an warning message. */
    warning(message) {
        console.log(message);

    }

    /** Logs an error message. */
    error(message) {
        console.log(message);

    }

    /** Logs a runtime error of type internal_functionsException. */
    runtimeError(e, isCause = false) {
        console.assert(e instanceof internal_functionsException);
        if (isCause) {
            this.error("CAUSE: " + e.message);
        } else {
            this.error("ERROR: " + e.message);
        }
        if (e.cause) {
            this.runtimeError(e.cause, true);
        }
    }

    /**
     * Informs about the duration of a single execution.
     *
     * @param duration The duration of the execution, in milliseconds.
     * @param cycleTime The desired maximum duration of the execution, in
     *      milliseconds, or '-1' if not available.
     */
    infoExec(duration, cycleTime) {
        // Nothing is done here by default.
    }

    /**
     * Informs that an event will be or has been executed.
     *
     * @param idx The 0-based index of the event.
     * @param pre Whether the event will be executed ('true') or has
     *      been executed ('false').
     */
    infoEvent(idx, pre) {
        if (pre) {
            if (this.doTransitionOutput) internal_functions.log(internal_functionsUtils.fmt('Transition: event %s', internal_functions.getEventName(idx)));
        } else {
            if (this.doStateOutput) internal_functions.log('State: ' + internal_functions.getStateText());
        }
    }

    /**
     * Informs that the code is about to be executed. For the
     * {@link #firstExec} the state has not yet been initialized, except for
     * {@link #time}.
     */
    preExec() {
        // Nothing is done here by default.
    }

    /** Informs that the code was just executed. */
    postExec() {
        this.updateUI();
    }

    /**
     * Returns the name of an event.
     *
     * @param idx The 0-based index of the event.
     * @return The name of the event.
     */
    getEventName(idx) {
        return this.EVENT_NAMES[idx];
    }

    /**
     * Returns a single-line textual representation of the model state.
     *
     * @return The single-line textual representation of the model state.
     */
    getStateText() {
        var state = internal_functionsUtils.fmt('time=%s', internal_functions.time);
        state += internal_functionsUtils.fmt(', aut.combi=%s', internal_functionsUtils.valueToStr(internal_functions.aut_combi_));
        state += internal_functionsUtils.fmt(', aut.v00=%s', internal_functionsUtils.valueToStr(internal_functions.aut_v00_));
        state += internal_functionsUtils.fmt(', aut.v01=%s', internal_functionsUtils.valueToStr(internal_functions.aut_v01_));
        state += internal_functionsUtils.fmt(', aut.v02=%s', internal_functionsUtils.valueToStr(internal_functions.aut_v02_));
        state += internal_functionsUtils.fmt(', aut.v03=%s', internal_functionsUtils.valueToStr(internal_functions.aut_v03_));
        state += internal_functionsUtils.fmt(', aut.v04=%s', internal_functionsUtils.valueToStr(internal_functions.aut_v04_));
        state += internal_functionsUtils.fmt(', aut.v05=%s', internal_functionsUtils.valueToStr(internal_functions.aut_v05_));
        state += internal_functionsUtils.fmt(', aut.v06=%s', internal_functionsUtils.valueToStr(internal_functions.aut_v06_));
        state += internal_functionsUtils.fmt(', aut.v07=%s', internal_functionsUtils.valueToStr(internal_functions.aut_v07_));
        state += internal_functionsUtils.fmt(', aut.v08=%s', internal_functionsUtils.valueToStr(internal_functions.aut_v08_));
        state += internal_functionsUtils.fmt(', aut.v09=%s', internal_functionsUtils.valueToStr(internal_functions.aut_v09_));
        state += internal_functionsUtils.fmt(', aut.v10=%s', internal_functionsUtils.valueToStr(internal_functions.aut_v10_));
        state += internal_functionsUtils.fmt(', aut.v11=%s', internal_functionsUtils.valueToStr(internal_functions.aut_v11_));
        state += internal_functionsUtils.fmt(', aut.v12=%s', internal_functionsUtils.valueToStr(internal_functions.aut_v12_));
        state += internal_functionsUtils.fmt(', aut.v13=%s', internal_functionsUtils.valueToStr(internal_functions.aut_v13_));
        state += internal_functionsUtils.fmt(', aut.v14=%s', internal_functionsUtils.valueToStr(internal_functions.aut_v14_));
        state += internal_functionsUtils.fmt(', aut.v15=%s', internal_functionsUtils.valueToStr(internal_functions.aut_v15_));
        state += internal_functionsUtils.fmt(', aut.v16=%s', internal_functionsUtils.valueToStr(internal_functions.aut_v16_));
        state += internal_functionsUtils.fmt(', aut.v17=%s', internal_functionsUtils.valueToStr(internal_functions.aut_v17_));
        state += internal_functionsUtils.fmt(', aut.v18=%s', internal_functionsUtils.valueToStr(internal_functions.aut_v18_));
        state += internal_functionsUtils.fmt(', aut.v19=%s', internal_functionsUtils.valueToStr(internal_functions.aut_v19_));
        return state;
    }




    /**
     * Function "inc".
     *
     * @param inc_x_ Function parameter "inc.x".
     * @return The return value of the function.
     * @throws {internal_functionsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    inc_(inc_x_) {
        try {
            // Execute statements in the function body.
            return internal_functionsUtils.addInt(inc_x_, 1);
            throw new Error('No return statement at end of function.');
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to execute internal user-defined function \"inc\".", e);
            }
            throw e;
        }
    }

    /**
     * Function "factorial".
     *
     * @param factorial_x_ Function parameter "factorial.x".
     * @return The return value of the function.
     * @throws {internal_functionsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    factorial_(factorial_x_) {
        try {
            // Execute statements in the function body.
            return (internal_functionsUtils.equalObjs(factorial_x_, 0)) ? 1 : (internal_functionsUtils.multiplyInt(factorial_x_, internal_functions.factorial_(internal_functionsUtils.subtractInt(factorial_x_, 1))));
            throw new Error('No return statement at end of function.');
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to execute internal user-defined function \"factorial\".", e);
            }
            throw e;
        }
    }

    /**
     * Function "rec1".
     *
     * @param rec1_x_ Function parameter "rec1.x".
     * @return The return value of the function.
     * @throws {internal_functionsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    rec1_(rec1_x_) {
        try {
            // Execute statements in the function body.
            return (internal_functionsUtils.equalObjs(rec1_x_, 0)) ? 1 : (internal_functions.rec2_(internal_functionsUtils.subtractInt(rec1_x_, 1)));
            throw new Error('No return statement at end of function.');
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to execute internal user-defined function \"rec1\".", e);
            }
            throw e;
        }
    }

    /**
     * Function "rec2".
     *
     * @param rec2_x_ Function parameter "rec2.x".
     * @return The return value of the function.
     * @throws {internal_functionsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    rec2_(rec2_x_) {
        try {
            // Execute statements in the function body.
            return (internal_functionsUtils.equalObjs(rec2_x_, 0)) ? 2 : (internal_functions.rec1_(internal_functionsUtils.subtractInt(rec2_x_, 1)));
            throw new Error('No return statement at end of function.');
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to execute internal user-defined function \"rec2\".", e);
            }
            throw e;
        }
    }

    /**
     * Function "multi_return".
     *
     * @return The return value of the function.
     * @throws {internal_functionsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    multi_return_() {
        try {
            // Execute statements in the function body.
            return new CifTuple_T2IR(1, 1.0);
            throw new Error('No return statement at end of function.');
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to execute internal user-defined function \"multi_return\".", e);
            }
            throw e;
        }
    }

    /**
     * Function "f0".
     *
     * @return The return value of the function.
     * @throws {internal_functionsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    f0_() {
        try {
            // Execute statements in the function body.
            return 1;
            throw new Error('No return statement at end of function.');
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to execute internal user-defined function \"f0\".", e);
            }
            throw e;
        }
    }

    /**
     * Function "f1".
     *
     * @param f1_x_ Function parameter "f1.x".
     * @return The return value of the function.
     * @throws {internal_functionsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    f1_(f1_x_) {
        try {
            // Execute statements in the function body.
            return f1_x_;
            throw new Error('No return statement at end of function.');
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to execute internal user-defined function \"f1\".", e);
            }
            throw e;
        }
    }

    /**
     * Function "f2".
     *
     * @param f2_x_ Function parameter "f2.x".
     * @param f2_y_ Function parameter "f2.y".
     * @return The return value of the function.
     * @throws {internal_functionsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    f2_(f2_x_, f2_y_) {
        try {
            // Execute statements in the function body.
            return internal_functionsUtils.addInt(f2_x_, f2_y_);
            throw new Error('No return statement at end of function.');
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to execute internal user-defined function \"f2\".", e);
            }
            throw e;
        }
    }

    /**
     * Function "f3".
     *
     * @param f3_x_ Function parameter "f3.x".
     * @param f3_y_ Function parameter "f3.y".
     * @param f3_z_ Function parameter "f3.z".
     * @return The return value of the function.
     * @throws {internal_functionsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    f3_(f3_x_, f3_y_, f3_z_) {
        try {
            // Execute statements in the function body.
            return internal_functionsUtils.addReal(internal_functionsUtils.addInt(f3_x_, f3_y_), f3_z_);
            throw new Error('No return statement at end of function.');
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to execute internal user-defined function \"f3\".", e);
            }
            throw e;
        }
    }

    /**
     * Function "locals".
     *
     * @param locals_x_ Function parameter "locals.x".
     * @return The return value of the function.
     * @throws {internal_functionsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    locals_(locals_x_) {
        try {
            // Variable "locals.a".
            var locals_a_ = 5;

            // Variable "locals.c".
            var locals_c_ = locals_a_;

            // Variable "locals.b".
            var locals_b_ = internal_functionsUtils.addInt(locals_c_, locals_x_);

            // Execute statements in the function body.
            return locals_b_;
            throw new Error('No return statement at end of function.');
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to execute internal user-defined function \"locals\".", e);
            }
            throw e;
        }
    }

    /**
     * Function "rot1".
     *
     * @param rot1_x_ Function parameter "rot1.x".
     * @return The return value of the function.
     * @throws {internal_functionsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    rot1_(rot1_x_) {
        try {
            // Variable "rot1.tmp".
            var rot1_tmp_ = internal_functionsUtils.projectList(rot1_x_, 0);

            // Execute statements in the function body.
            {
                var rhs1 = internal_functionsUtils.projectList(rot1_x_, 3);
                var index2 = 0;
                rot1_x_ = internal_functionsUtils.modify(rot1_x_, index2, rhs1);
            }

            {
                var rhs1 = internal_functionsUtils.projectList(rot1_x_, 2);
                var index2 = 1;
                rot1_x_ = internal_functionsUtils.modify(rot1_x_, index2, rhs1);
            }

            {
                var rhs1 = internal_functionsUtils.projectList(rot1_x_, 1);
                var index2 = 2;
                rot1_x_ = internal_functionsUtils.modify(rot1_x_, index2, rhs1);
            }

            {
                var rhs1 = rot1_tmp_;
                var index2 = 3;
                rot1_x_ = internal_functionsUtils.modify(rot1_x_, index2, rhs1);
            }

            return rot1_x_;
            throw new Error('No return statement at end of function.');
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to execute internal user-defined function \"rot1\".", e);
            }
            throw e;
        }
    }

    /**
     * Function "rot2".
     *
     * @param rot2_x_ Function parameter "rot2.x".
     * @return The return value of the function.
     * @throws {internal_functionsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    rot2_(rot2_x_) {
        try {
            // Variable "rot2.rslt".
            var rot2_rslt_ = [0, 0, 0, 0];

            // Execute statements in the function body.
            {
                var rhs1 = internal_functionsUtils.projectList(rot2_x_, 3);
                var index2 = 0;
                rot2_rslt_ = internal_functionsUtils.modify(rot2_rslt_, index2, rhs1);
            }

            {
                var rhs1 = internal_functionsUtils.projectList(rot2_x_, 2);
                var index2 = 1;
                rot2_rslt_ = internal_functionsUtils.modify(rot2_rslt_, index2, rhs1);
            }

            {
                var rhs1 = internal_functionsUtils.projectList(rot2_x_, 1);
                var index2 = 2;
                rot2_rslt_ = internal_functionsUtils.modify(rot2_rslt_, index2, rhs1);
            }

            {
                var rhs1 = internal_functionsUtils.projectList(rot2_x_, 0);
                var index2 = 3;
                rot2_rslt_ = internal_functionsUtils.modify(rot2_rslt_, index2, rhs1);
            }

            return rot2_x_;
            throw new Error('No return statement at end of function.');
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to execute internal user-defined function \"rot2\".", e);
            }
            throw e;
        }
    }

    /**
     * Function "fa".
     *
     * @param fa_x_ Function parameter "fa.x".
     * @return The return value of the function.
     * @throws {internal_functionsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    fa_(fa_x_) {
        try {
            // Variable "fa.y".
            var fa_y_ = [fa_x_, fa_x_, fa_x_];

            // Variable "fa.a".
            var fa_a_ = fa_x_;

            // Variable "fa.b".
            var fa_b_ = internal_functionsUtils.addInt(fa_x_, 1);

            // Variable "fa.t".
            var fa_t_ = new CifTuple_T2II(0, 0);

            // Execute statements in the function body.
            {
                var rhs1 = 1;
                var index2 = 0;
                fa_y_ = internal_functionsUtils.modify(fa_y_, index2, rhs1);
            }
            {
                var rhs1 = 2;
                var index2 = 1;
                fa_y_ = internal_functionsUtils.modify(fa_y_, index2, rhs1);
            }

            fa_y_ = [internal_functionsUtils.addInt(internal_functionsUtils.projectList(fa_y_, 0), 1), internal_functionsUtils.projectList(fa_y_, 1), internal_functionsUtils.projectList(fa_y_, 2)];

            {
                var fa_a_tmp1 = fa_a_;
                fa_a_ = fa_b_;
                fa_b_ = fa_a_tmp1;
            }

            fa_t_ = new CifTuple_T2II(internal_functionsUtils.addInt(fa_a_, fa_b_), internal_functionsUtils.subtractInt(fa_b_, fa_a_));

            {
                var rhs1 = fa_t_;
                fa_a_ = (rhs1)._field0;
                fa_b_ = (rhs1)._field1;
            }

            fa_x_ = internal_functionsUtils.addInt(internal_functionsUtils.addInt(fa_a_, fa_b_), internal_functionsUtils.projectList(fa_y_, 0));

            return fa_x_;
            throw new Error('No return statement at end of function.');
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to execute internal user-defined function \"fa\".", e);
            }
            throw e;
        }
    }

    /**
     * Function "fi".
     *
     * @param fi_x_ Function parameter "fi.x".
     * @return The return value of the function.
     * @throws {internal_functionsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    fi_(fi_x_) {
        try {
            // Execute statements in the function body.
            if (internal_functionsUtils.equalObjs(fi_x_, 1)) {
                fi_x_ = internal_functionsUtils.addInt(fi_x_, 1);
            }

            if (internal_functionsUtils.equalObjs(fi_x_, 1)) {
                fi_x_ = internal_functionsUtils.addInt(fi_x_, 1);
            } else if (internal_functionsUtils.equalObjs(fi_x_, 2)) {
                fi_x_ = internal_functionsUtils.addInt(fi_x_, 2);
            }

            if (internal_functionsUtils.equalObjs(fi_x_, 2)) {
                fi_x_ = internal_functionsUtils.addInt(fi_x_, 1);
            } else if (internal_functionsUtils.equalObjs(fi_x_, 3)) {
                fi_x_ = internal_functionsUtils.addInt(fi_x_, 2);
            } else if (internal_functionsUtils.equalObjs(fi_x_, 4)) {
                fi_x_ = internal_functionsUtils.addInt(fi_x_, 3);
            }

            if (internal_functionsUtils.equalObjs(fi_x_, 2)) {
                fi_x_ = internal_functionsUtils.addInt(fi_x_, 1);
            } else if (internal_functionsUtils.equalObjs(fi_x_, 3)) {
                fi_x_ = internal_functionsUtils.addInt(fi_x_, 2);
            } else {
                fi_x_ = internal_functionsUtils.addInt(fi_x_, 4);
            }

            if (internal_functionsUtils.equalObjs(fi_x_, 6)) {
                fi_x_ = internal_functionsUtils.addInt(fi_x_, 1);
            } else {
                fi_x_ = internal_functionsUtils.addInt(fi_x_, 2);
            }

            if ((fi_x_) > (4)) {
                if ((fi_x_) < (6)) {
                    fi_x_ = internal_functionsUtils.subtractInt(fi_x_, 1);
                } else {
                    fi_x_ = internal_functionsUtils.subtractInt(fi_x_, 2);
                }
            }

            return fi_x_;
            throw new Error('No return statement at end of function.');
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to execute internal user-defined function \"fi\".", e);
            }
            throw e;
        }
    }

    /**
     * Function "fw".
     *
     * @return The return value of the function.
     * @throws {internal_functionsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    fw_() {
        try {
            // Variable "fw.x".
            var fw_x_ = 0;

            // Execute statements in the function body.
            while ((fw_x_) > (0)) {
                while ((fw_x_) < (10)) {
                    if (internal_functionsUtils.equalObjs((fw_x_) % (2), 1)) {
                        continue;
                    }

                    if (internal_functionsUtils.equalObjs(fw_x_, 8)) {
                        break;
                    }
                }
            }

            return fw_x_;
            throw new Error('No return statement at end of function.');
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to execute internal user-defined function \"fw\".", e);
            }
            throw e;
        }
    }

    /**
     * Function "fu1".
     *
     * @return The return value of the function.
     * @throws {internal_functionsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    fu1_() {
        try {
            // Execute statements in the function body.
            if (true) {
                return 1;
            }

            return 0;
            throw new Error('No return statement at end of function.');
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to execute internal user-defined function \"fu1\".", e);
            }
            throw e;
        }
    }

    /**
     * Function "fu2".
     *
     * @return The return value of the function.
     * @throws {internal_functionsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    fu2_() {
        try {
            // Execute statements in the function body.
            return 1;

            return 0;
            throw new Error('No return statement at end of function.');
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to execute internal user-defined function \"fu2\".", e);
            }
            throw e;
        }
    }

    /**
     * Function "fu3".
     *
     * @return The return value of the function.
     * @throws {internal_functionsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    fu3_() {
        try {
            // Execute statements in the function body.
            while (true) {
                return 1;
            }

            return 0;
            throw new Error('No return statement at end of function.');
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to execute internal user-defined function \"fu3\".", e);
            }
            throw e;
        }
    }

    /**
     * Function "fr".
     *
     * @return The return value of the function.
     * @throws {internal_functionsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    fr_() {
        try {
            // Variable "fr.x".
            var fr_x_ = [1, 2, 3];

            // Variable "fr.y".
            var fr_y_ = [2, 3, 4];

            // Execute statements in the function body.
            {
                var rhs1 = fr_y_;
                for (var rng_index0 = 0; rng_index0 < rhs1.length; rng_index0++) {
                    var rng_elem0 = rhs1[rng_index0];
                    if ((rng_elem0) > 3) {
                        internal_functionsUtils.rangeErrInt("fr.x" + "[" + (rng_index0).toString() + "]", internal_functionsUtils.valueToStr(rng_elem0), "list[3] int[0..3]");
                    }
                }
                fr_x_ = rhs1;
            }

            {
                var rhs1 = [-(1), 3, 5];
                for (var rng_index0 = 0; rng_index0 < rhs1.length; rng_index0++) {
                    var rng_elem0 = rhs1[rng_index0];
                    if ((rng_elem0) < 0 || (rng_elem0) > 3) {
                        internal_functionsUtils.rangeErrInt("fr.x" + "[" + (rng_index0).toString() + "]", internal_functionsUtils.valueToStr(rng_elem0), "list[3] int[0..3]");
                    }
                }
                fr_x_ = rhs1;
            }

            return 1;
            throw new Error('No return statement at end of function.');
        } catch (e) {
            if (e instanceof internal_functionsException) {
                e = new internal_functionsException("Failed to execute internal user-defined function \"fr\".", e);
            }
            throw e;
        }
    }

    /**
     * Print output for all relevant print declarations.
     *
     * @param idx The 0-based event index of the transition, or '-2' for
     *      time transitions, or '-3' for the 'initial' transition.
     * @param pre Whether to print output for the pre/source state of the
     *      transition ('true') or for the post/target state of the
     *      transition ('false').
     * @throws {internal_functionsException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    printOutput(idx, pre) {
        if (true) {
            if (!pre) {
                var text;
                try {
                    var value = internal_functions.aut_combi_;
                    text = internal_functionsUtils.valueToStr(value);
                } catch (e) {
                    if (e instanceof internal_functionsException) {
                        e = new internal_functionsException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
    }

    /**
     * Informs that new print output is available.
     *
     * @param text The text being printed.
     * @param target The file or special target to which text is to be printed.
     *      If printed to a file, an absolute or relative local file system
     *      path is given. Paths may contain both '/' and '\\'
     *      as file separators. Use {@link internal_functionsUtils#normalizePrintTarget}
     *      to normalize the path to use '/' file separators. There are two
     *      special targets: ':stdout' to print to the standard output stream,
     *      and ':stderr' to print to the standard error stream.
     */
    infoPrintOutput(text, target) {
        if (target == ':stdout') {
            internal_functions.log(text);
        } else if (target == ':stderr') {
            internal_functions.error(text);
        } else {
            var path = internal_functionsUtils.normalizePrintTarget(target);
            internal_functions.infoPrintOutput(path + ': ' + text, ':stdout');
        }
    }
}
