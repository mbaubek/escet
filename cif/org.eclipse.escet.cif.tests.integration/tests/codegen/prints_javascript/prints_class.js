/** Tuples. */


            /** Tuple class for CIF tuple type representative "tuple(int[1..1]; bool; string)". */
            class CifTuple_T3IBS {
                /** The 1st field. */
                _field0;

                /** The 2nd field. */
                _field1;

                /** The 3rd field. */
                _field2;

                /**
                 * Constructor for the {@link CifTuple_T3IBS} class.
                 *
                 * @param _field0 The 1st field.
                 * @param _field1 The 2nd field.
                 * @param _field2 The 3rd field.
                 */
                constructor(_field0, _field1, _field2) {
                    this._field0 = _field0;
                    this._field1 = _field1;
                    this._field2 = _field2;
                }

                copy() {
                    return new CifTuple_T3IBS(this._field0, this._field1, this._field2);
                }

                toString() {
                    var rslt = '(';
                    rslt += printsUtils.valueToStr(this._field0);
                    rslt += ', ';
                    rslt += printsUtils.valueToStr(this._field1);
                    rslt += ', ';
                    rslt += printsUtils.valueToStr(this._field2);
                    rslt += ')';
                    return rslt;
                }
            }

/** prints code generated from a CIF specification. */
class prints_class {
    /** printsEnum declaration. It contains the single merged enum from the CIF model. */
    printsEnum = Object.freeze({
        /** Literal "A". */
        _A: Symbol("A"),

        /** Literal "B". */
        _B: Symbol("B")
    })

    /** Should execution timing information be provided? */
    doInfoExec = true;

    /** Should executed event information be provided? */
    doInfoEvent = true;

    /** Should print output be provided? */
    doInfoPrintOutput = true;

    /** Should state output be provided? */
    doStateOutput = false;

    /** Should transition output be provided? */
    doTransitionOutput = true;

    /** Whether the constants have been initialized already. */
    constantsInitialized = false;

    /** Whether the SVG copy and move declarations have been applied already. */
    svgCopiesAndMovesApplied = false;

    /** Whether this is the first time the code is (to be) executed. */
    firstExec;

    /** The names of all the events. */
    EVENT_NAMES = [
        "e1",
        "e2"
    ];


    /** Variable 'time', tracks elapsed time for a session. */
    time;

    /**
     * The frequency in times per second, that the code should
     * be executed (if positive), or execute as fast as possible, that is
     * as many times per second as possible (if negative or zero).
     */
    frequency = 60;

    /**
     * Whether the next execution is the first execution of the session.
     * Used to initialize time-related variables for starting, pausing,
     * resuming or resetting each session.
     */
    first;

    /**
     * Whether the simulation is currently running, and should process
     * user input, or is paused.
     */
    playing;

    /** The start time of the current session. */
    startMilli;

    /**
     * The targeted end time of the current/next cycle, to ensure
     * that the duration of the cycle matches with the configured
     * frequency.
     */
    targetMilli;




    /** SVG output elements. */


    /**
     * SVG input queue with functions for handling clicked SVG
     * input elements, from first clicked at the head of the array
     * to last clicked at the tail of the array.
     */
    svgInQueue;

    /**
     * The SVG input id corresponding to the SVG input element that
     * was clicked. Is 'null' if no SVG input element was clicked so
     * far, or all clicks have already been processed.
     */
    svgInId;

    /**
     * The 0-based index of the event corresponding to the SVG input
     * element that was clicked. Is '-1' if no SVG input element was
     * clicked so far, or all clicks have already been processed.
     */
    svgInEvent;

    /** SVG input click event handlers. */


    /** SVG input event setters. */


    /** Starts the simulation. */
    start() {
        if (!this.playing) {
            this.playing = true;
            this.exec();
        }
    }

    /** Stops the simulation. */
    stop() {
        if (this.playing) {
            this.playing = false;
        }
    }

    /** Resets the object to its initial state. */
    reset() {
        this.stop();
        this.firstExec = true;
        this.time = 0.0;
        this.first = true;
        this.timePaused = null;
        this.initState();
        this.initUI();
        this.updateUI();
    }

    /**
     * Execute the code once. Inputs are read, transitions are executed until
     * none are possible, outputs are written, etc.
     *
     * @param newTime The time in seconds, since the start of the first
     *      execution.
     * @throws {printsException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    execOnce(newTime) {
        // Pre execution notification.
        this.preExec();

        // Update values of input variables.
        this.updateInputs();

        // Initialize the state.
        if (this.firstExec) {
            this.initState();
        }

        // Calculate time delta.
        var delta = newTime - this.time;

        // Update values of continuous variables.
        if (!this.firstExec) {
                        // No continuous variables, except variable 'time'.
        }

        // Update time.
        this.time = newTime;

        // Apply print declarations.
        if (this.firstExec) {
            // For 'initial' transition.
            if (this.doInfoPrintOutput) this.printOutput(-3, true);
            if (this.doInfoPrintOutput) this.printOutput(-3, false);
            if (this.doStateOutput) prints.log('Initial state: ' + prints.getStateText());
            if (this.doStateOutput || this.doTransitionOutput) this.log('');

        } else {
            // For 'post' of time transition.
            if (this.doInfoPrintOutput) this.printOutput(-2, false);
        }

        // Execute environment events and SVG input events as long as they are possible, emptying the SVG input queue.
        while (true) {
            // Handle next element from SVG input queue, if not already already processing one.
            if (this.svgInEvent == -1 && this.svgInQueue.length > 0) {
                var func = this.svgInQueue.shift(); // Remove head of the queue.
                func(); // Call function, to set the event to allow.
            }

            // Try to execute an SVG input mapping with updates for each environment event, and an edge for each SVG
            // input event.
            var anythingExecuted = false;



            // Stop if no SVG input mapping and no edge was executed, and no more SVG input clicks are to be processed.
            if (!anythingExecuted && this.svgInQueue.length == 0) {
                break;
            }
        }

        // Make sure all outstanding SVG input clicks have been processed.
        console.assert(this.svgInEvent == -1 && this.svgInQueue.length == 0);

        // Execute uncontrollable edges as long as they are possible.
        while (true) {
            // Try to execute an edge for each event.
            var edgeExecuted = false;


            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Execute controllable edges as long as they are possible.
        while (true) {
            // Try to execute an edge for each event.
            var edgeExecuted = false;


            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Apply print declarations for 'pre' of time transition.
        if (this.doInfoPrintOutput) this.printOutput(-2, true);

        // Post execution notification.
        this.postExec();

        // Done.
        this.firstExec = false;
    }

    /**
     * Calls {@link #execWhile}, which repeatedly {@link #execOnce executes the code}.
     *
     * @throws {printsException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    exec() {
        this.execWhile(1);
    }

    /**
     * Repeatedly {@link #execOnce executes the code}.
     *
     * @param delay The delay before executing, in milliseconds.
     *
     * @throws {printsException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    execWhile(delay) {
        setTimeout(
            function () {
                // Pre execution timing.
                var now = Date.now();
                var preMilli = now;

                // On first execution, initialize variables for timing.
                if (prints.first) {
                    prints.first = false;
                    prints.startMilli = now;
                    prints.targetMilli = prints.startMilli;
                    preMilli = prints.startMilli;
                }

                // Handle pausing/playing.
                if (!prints.playing) {
                    prints.timePaused = now;
                    return;
                }

                if (prints.timePaused) {
                    prints.startMilli += (now - prints.timePaused);
                    prints.targetMilli += (now - prints.timePaused);
                    prints.timePaused = null;
                }

                // Get cycle time and current 'time'.
                var frequency = prints.frequency;
                var cycleMilli = (frequency <= 0) ? -1 : 1e3 / frequency;
                var timeMilli = preMilli - prints.startMilli;

                // Execute once.
                prints.execOnce(timeMilli / 1e3);

                // Post execution timing.
                var postMilli = Date.now();
                var duration = postMilli - preMilli;
                if (prints.doInfoExec) {
                    prints.infoExec(duration, cycleMilli);
                }

                // Ensure frequency.
                var remainderMilli = 0;
                if (frequency > 0) {
                    prints.targetMilli += cycleMilli;
                    remainderMilli = prints.targetMilli - postMilli;
                }

                // Execute again.
                prints.execWhile(remainderMilli > 0 ? remainderMilli : 0);
            },
        delay);
    }


    /**
     * Initializes the state.
     *
     * @throws {printsException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    initState() {
        // Initialize constants, if not yet done so.
        if (!this.constantsInitialized) {
            this.constantsInitialized = true;

        }

        // Initialize SVG input.
        this.svgInQueue = [];
        this.svgInId = null;
        this.svgInEvent = -1;


        // CIF model state variables.
        // No state variables, except variable 'time'.
    }

    /**
     * Initializes the user interface, either when loading the page
     * or when resetting the simulation.
     */
    initUI() {


        // Apply SVG copies and moves, if not done so before.
        if (!this.svgCopiesAndMovesApplied) {
            this.svgCopiesAndMovesApplied = true;

            // Apply SVG copy declarations.


            // Apply SVG move declarations.

        }

        // Prepare SVG output.


        // Prepare SVG input.

    }

    /**
     * Updates the user interface based on the latest state of
     * the model. Is called at the end of each cycle.
     *
     * @throws {printsException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    updateUI() {
        // Apply CIF/SVG output mappings.

    }

    /**
     * Updates the values of the input variables. Other variables from the
     * state may not be accessed or modified.
     */
    updateInputs() {
        // Nothing is done here by default.
    }

    /** Logs a normal message. */
    log(message) {
        console.log(message);

    }

    /** Logs an warning message. */
    warning(message) {
        console.log(message);

    }

    /** Logs an error message. */
    error(message) {
        console.log(message);

    }

    /** Logs a runtime error of type printsException. */
    runtimeError(e, isCause = false) {
        console.assert(e instanceof printsException);
        if (isCause) {
            this.error("CAUSE: " + e.message);
        } else {
            this.error("ERROR: " + e.message);
        }
        if (e.cause) {
            this.runtimeError(e.cause, true);
        }
    }

    /**
     * Informs about the duration of a single execution.
     *
     * @param duration The duration of the execution, in milliseconds.
     * @param cycleTime The desired maximum duration of the execution, in
     *      milliseconds, or '-1' if not available.
     */
    infoExec(duration, cycleTime) {
        // Nothing is done here by default.
    }

    /**
     * Informs that an event will be or has been executed.
     *
     * @param idx The 0-based index of the event.
     * @param pre Whether the event will be executed ('true') or has
     *      been executed ('false').
     */
    infoEvent(idx, pre) {
        if (pre) {
            if (this.doTransitionOutput) prints.log(printsUtils.fmt('Transition: event %s', prints.getEventName(idx)));
        } else {
            if (this.doStateOutput) prints.log('State: ' + prints.getStateText());
        }
    }

    /**
     * Informs that the code is about to be executed. For the
     * {@link #firstExec} the state has not yet been initialized, except for
     * {@link #time}.
     */
    preExec() {
        // Nothing is done here by default.
    }

    /** Informs that the code was just executed. */
    postExec() {
        this.updateUI();
    }

    /**
     * Returns the name of an event.
     *
     * @param idx The 0-based index of the event.
     * @return The name of the event.
     */
    getEventName(idx) {
        return this.EVENT_NAMES[idx];
    }

    /**
     * Returns a single-line textual representation of the model state.
     *
     * @return The single-line textual representation of the model state.
     */
    getStateText() {
        var state = printsUtils.fmt('time=%s', prints.time);

        return state;
    }





    /**
     * Print output for all relevant print declarations.
     *
     * @param idx The 0-based event index of the transition, or '-2' for
     *      time transitions, or '-3' for the 'initial' transition.
     * @param pre Whether to print output for the pre/source state of the
     *      transition ('true') or for the post/target state of the
     *      transition ('false').
     * @throws {printsException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    printOutput(idx, pre) {
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = "bla5";
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = "bla6";
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, "e.txt");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = "bla7";
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = "bla8";
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stderr");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = "bla9";
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (idx == -3) {
            if (!pre) {
                var text;
                try {
                    text = "bla10";
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (false) {
            if (!pre) {
                var text;
                try {
                    text = "bla11";
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (idx >= 0) {
            if (!pre) {
                var text;
                try {
                    text = "bla12";
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (idx == -2) {
            if (!pre) {
                var text;
                try {
                    text = "bla13";
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (idx == 0) {
            if (!pre) {
                var text;
                try {
                    text = "bla14";
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (idx == 1) {
            if (!pre) {
                var text;
                try {
                    text = "bla15";
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if ((idx == 0) || (idx == 1)) {
            if (!pre) {
                var text;
                try {
                    text = "bla16";
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if ((idx == -3) || (idx >= 0) || (idx == -2)) {
            if (!pre) {
                var text;
                try {
                    text = "bla17";
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = "bla18";
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (pre) {
                var text;
                try {
                    text = "bla19";
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"pre\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = "bla20";
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (pre) {
                var text;
                try {
                    text = "bla21";
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"pre\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
            if (!pre) {
                var text;
                try {
                    text = "bla22";
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var whenCond;
                try {
                    whenCond = (prints.time) > (5);
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"when post\" filter.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                if (whenCond) {
                    var text;
                    try {
                        text = "bla24";
                    } catch (e) {
                        if (e instanceof printsException) {
                            e = new printsException("Failed to evaluate print declaration \"post\" text.", e);
                            this.runtimeError(e);
                        }
                        throw e;
                    }
                    this.infoPrintOutput(text, ":stdout");
                }
            }
        }
        if (true) {
            if (pre) {
                var whenCond;
                try {
                    whenCond = (prints.time) > (5);
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"when pre\" filter.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                if (whenCond) {
                    var text;
                    try {
                        text = "bla25";
                    } catch (e) {
                        if (e instanceof printsException) {
                            e = new printsException("Failed to evaluate print declaration \"pre\" text.", e);
                            this.runtimeError(e);
                        }
                        throw e;
                    }
                    this.infoPrintOutput(text, ":stdout");
                }
            }
        }
        if (true) {
            if (!pre) {
                var whenCond;
                try {
                    whenCond = (prints.time) > (5);
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"when post\" filter.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                if (whenCond) {
                    var text;
                    try {
                        text = "bla28";
                    } catch (e) {
                        if (e instanceof printsException) {
                            e = new printsException("Failed to evaluate print declaration \"post\" text.", e);
                            this.runtimeError(e);
                        }
                        throw e;
                    }
                    this.infoPrintOutput(text, ":stdout");
                }
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    var value = true;
                    text = printsUtils.valueToStr(value);
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    var value = 123;
                    text = printsUtils.valueToStr(value);
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    var value = 1.23;
                    text = printsUtils.valueToStr(value);
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = "a\nbc";
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    var value = prints.printsEnum._A;
                    text = printsUtils.valueToStr(value);
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    var value = prints.printsEnum._B;
                    text = printsUtils.valueToStr(value);
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    var value = [1, 3, 2];
                    text = printsUtils.valueToStr(value);
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    var value = ["a\nbc", "def"];
                    text = printsUtils.valueToStr(value);
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    var value = new CifTuple_T3IBS(1, true, "a");
                    text = printsUtils.valueToStr(value);
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = "bla1";
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, "a.txt");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = "bla2";
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, "b.txt");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = "bla3";
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, "c.txt");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = "bla4";
                } catch (e) {
                    if (e instanceof printsException) {
                        e = new printsException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, "d.txt");
            }
        }
    }

    /**
     * Informs that new print output is available.
     *
     * @param text The text being printed.
     * @param target The file or special target to which text is to be printed.
     *      If printed to a file, an absolute or relative local file system
     *      path is given. Paths may contain both '/' and '\\'
     *      as file separators. Use {@link printsUtils#normalizePrintTarget}
     *      to normalize the path to use '/' file separators. There are two
     *      special targets: ':stdout' to print to the standard output stream,
     *      and ':stderr' to print to the standard error stream.
     */
    infoPrintOutput(text, target) {
        if (target == ':stdout') {
            prints.log(text);
        } else if (target == ':stderr') {
            prints.error(text);
        } else {
            var path = printsUtils.normalizePrintTarget(target);
            prints.infoPrintOutput(path + ': ' + text, ':stdout');
        }
    }
}
