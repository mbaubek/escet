Input/output report of the svg_input_update.cif SFunction.

During code generation, CIF variables are made available in the Simulink vectors.
This report lists the variables in each vector, along with their index number.

Modes
-----
No variables are available here.

Continuous states
-----------------
time 1

Inputs
------
i   1
j   2
ia  3
ib  4
ic  5
g.i 6

Outputs
-------
a    1
iabc 2
