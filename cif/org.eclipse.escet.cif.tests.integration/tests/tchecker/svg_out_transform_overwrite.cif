//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

svgfile "svg_out_transform_overwrite.svg";

// Warning for "transform" overwrite.
svgout id "box1" attr "transform" value "translate(5, 6)";

// No warning if there is no "transform" attribute.
svgout id "box2" attr "transform" value "translate(5, 6)";

// No warning if there is an empty "transform" attribute.
svgout id "box3" attr "transform" value "translate(5, 6)";

// No warning if there is a blank "transform" attribute.
svgout id "box4" attr "transform" value "translate(5, 6)";

// No warning for different capitalization.
svgout id "box1" attr "Transform" value "translate(5, 6)";

// No warning for overwriting non-transform attributes.
svgout id "box1" attr "height" value "15";
