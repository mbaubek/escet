Loading CIF specification "controllercheck/bounded_response/bounded_supervised_counter.cif".
Checking for bounded response:
    Converting CIF specification to a BDD representation:
        CIF variables and location pointers:
            Nr     Kind               Type       Name  Group  BDD vars  CIF values  BDD values  Values used
            -----  -----------------  ---------  ----  -----  --------  ----------  ----------  -----------
            1      discrete variable  int[0..5]  p.x   0      3 * 2     6 * 2       8 * 2       75%
            -----  -----------------  ---------  ----  -----  --------  ----------  ----------  -----------
            Total                                      1      6         12          16          75%

        Skipping variable ordering: only one variable present.

        Restricting system behavior using state/event exclusion plant invariants:
            Edge (event: c_c) (guard: p.x = 2) (assignments: p.x := p.x - 1): guard: p.x = 2 -> false [plant: false].

            System:
                Edge: (event: c_a) (guard: p.x = 4) (assignments: p.x := p.x - 1)
                Edge: (event: c_b) (guard: p.x = 3) (assignments: p.x := p.x - 1)
                Edge: (event: c_c) (guard: p.x = 2 -> false) (assignments: p.x := p.x - 1)
                Edge: (event: c_d) (guard: p.x = 1) (assignments: p.x := p.x - 1)
                Edge: (event: u_a) (guard: p.x = 0) (assignments: p.x := p.x + 1)
                Edge: (event: u_b) (guard: p.x = 1) (assignments: p.x := p.x + 1)
                Edge: (event: u_c) (guard: p.x = 2) (assignments: p.x := p.x + 1)
                Edge: (event: u_d) (guard: false) (assignments: p.x := p.x + 1)

    Computing states we can be in at the start of uncontrollable and controllable event loops:
        Round 1:
            Pre input states: p.x = 0
            Pre uncontrollables states: false
            Pre controllables states: false

            Applying event loop for input variable events:
                No input variable events.

            Pre uncontrollables states: false -> p.x = 0

            Applying event loop for uncontrollable events:
                Iteration 1 (states before iteration: p.x = 0):
                    Event u_a: p.x = 0 -> p.x = 1
                    Event u_b: p.x = 1 -> p.x = 2
                    Event u_c: p.x = 2 -> p.x = 3
                    All iterations end states: p.x = 3

                Iteration 2 (states before iteration: p.x = 3):
                    0 edges enabled. No state changes.
                    All iterations end states: p.x = 3

            Pre controllables states: false -> p.x = 3

            Applying event loop for controllable events:
                Iteration 1 (states before iteration: p.x = 3):
                    Event c_b: p.x = 3 -> p.x = 2
                    All iterations end states: p.x = 2

                Iteration 2 (states before iteration: p.x = 2):
                    0 edges enabled. No state changes.
                    All iterations end states: p.x = 2

            Pre input states: p.x = 0 -> p.x = 0 or p.x = 2

        Round 2:
            Pre input states: p.x = 0 or p.x = 2
            Pre uncontrollables states: p.x = 0
            Pre controllables states: p.x = 3

            Applying event loop for input variable events:
                No input variable events.

            Pre uncontrollables states: p.x = 0 -> p.x = 0 or p.x = 2

            Applying event loop for uncontrollable events:
                Iteration 1 (states before iteration: p.x = 0 or p.x = 2):
                    Event u_a: p.x = 0 or p.x = 2 -> p.x = 2 or p.x = 1
                    Event u_b: p.x = 2 or p.x = 1 -> p.x = 2
                    Event u_c: p.x = 2 -> p.x = 3
                    All iterations end states: p.x = 3

                Iteration 2 (states before iteration: p.x = 3):
                    0 edges enabled. No state changes.
                    All iterations end states: p.x = 3

            Applying event loop for controllable events:
                Iteration 1 (states before iteration: p.x = 3):
                    Event c_b: p.x = 3 -> p.x = 2
                    All iterations end states: p.x = 2

                Iteration 2 (states before iteration: p.x = 2):
                    0 edges enabled. No state changes.
                    All iterations end states: p.x = 2

        Round 3:
            Pre input states: p.x = 0 or p.x = 2
            Pre uncontrollables states: p.x = 0 or p.x = 2
            Pre controllables states: p.x = 3

            Applying event loop for input variable events:
                No input variable events.

            Applying event loop for uncontrollable events:
                Iteration 1 (states before iteration: p.x = 0 or p.x = 2):
                    Event u_a: p.x = 0 or p.x = 2 -> p.x = 2 or p.x = 1
                    Event u_b: p.x = 2 or p.x = 1 -> p.x = 2
                    Event u_c: p.x = 2 -> p.x = 3
                    All iterations end states: p.x = 3

                Iteration 2 (states before iteration: p.x = 3):
                    0 edges enabled. No state changes.
                    All iterations end states: p.x = 3

            Applying event loop for controllable events:
                Iteration 1 (states before iteration: p.x = 3):
                    Event c_b: p.x = 3 -> p.x = 2
                    All iterations end states: p.x = 2

                Iteration 2 (states before iteration: p.x = 2):
                    0 edges enabled. No state changes.
                    All iterations end states: p.x = 2

    Computing bound for uncontrollable events:
        Iteration 1 (states before iteration: p.x = 0 or p.x = 2):
            Event u_a: p.x = 0 or p.x = 2 -> p.x = 2 or p.x = 1
            Event u_b: p.x = 2 or p.x = 1 -> p.x = 2
            Event u_c: p.x = 2 -> p.x = 3

        Iteration 2 (states before iteration: p.x = 3):
            0 edges enabled. No state changes.

        Bound: 1.

    Computing bound for controllable events:
        Iteration 1 (states before iteration: p.x = 3):
            Event c_b: p.x = 3 -> p.x = 2

        Iteration 2 (states before iteration: p.x = 2):
            0 edges enabled. No state changes.

        Bound: 1.

    Bounded response check completed.

CONCLUSION:
    [OK] The specification has bounded response:

        - At most 1 iteration is needed for the event loop for uncontrollable events.
        - At most 1 iteration is needed for the event loop for controllable events.

    [UNKNOWN] Non-blocking under control checking was disabled, non-blocking under control property is unknown.
    [UNKNOWN] Finite response checking was disabled, finite response property is unknown.
    [UNKNOWN] Confluence checking was disabled, confluence property is unknown.

The model with the check results has been written to "controllercheck/bounded_response/bounded_supervised_counter.Bxxx.out.cif.real".
