//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

// Uncontrollables bound: 10.
// Controllables bound: 0.

uncontrollable int[0..5] chan;

plant chan_send2:
  location:
    initial;
    marked;
    edge chan!2;
end

plant chan_send1:
  location:
    initial;
    marked;
    edge chan!1;
end

plant chan_rcv2:
  disc int[0..5] x;

  location:
    initial;
    marked;
    edge chan? when x = 0 do x := ?;
end

plant chan_rcv1:
  disc int[0..5] x;

  location:
    initial;
    marked;
    edge chan? when x = 0 do x := ?;
end

plant chan_sync1:
  disc int[0..2000] v, w;
  location:
    initial;
    marked;
    edge chan when chan_rcv1.x = 0 and chan_rcv2.x = 0 do v := v + v + 1;
    edge chan when chan_rcv1.x = 0 and chan_rcv2.x = 1 do v := v + v + 2;
    edge chan when chan_rcv1.x = 0 and chan_rcv2.x = 2 do v := v + v + 4;
    edge chan when chan_rcv1.x = 1 and chan_rcv2.x = 0 do v := v + v + 8;
    edge chan when chan_rcv1.x = 1 and chan_rcv2.x = 1 do v := v + v + 16;
    edge chan when chan_rcv1.x = 1 and chan_rcv2.x = 2 do v := v + v + 32;
    edge chan when chan_rcv1.x = 2 and chan_rcv2.x = 0 do v := v + v + 64;
    edge chan when chan_rcv1.x = 2 and chan_rcv2.x = 1 do v := v + v + 128;
    edge chan when chan_rcv1.x = 2 and chan_rcv2.x = 2 do v := v + v + 256;
    edge chan when chan_rcv1.x > 2 or  chan_rcv2.x > 2 do v := 999;

    // Starting from the initial state, considering the execution scheme:
    //
    // - snd1 + rcv1, v: 0 -> 1
    // - snd1 + rcv2, v: 1 -> 10

    edge z.e when w < v do w := w + 1; // 10 times -> bound.
end

group z:
  uncontrollable e;
end
