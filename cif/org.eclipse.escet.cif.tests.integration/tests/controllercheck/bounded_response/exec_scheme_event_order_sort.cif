//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

// Uncontrollables bound: 0.
// Controllables bound: 13.

plant event_order_sort:
  controllable a, c, b;
  disc int[0..50] v, w;

  location:
    initial;
    marked;

    edge c     when v <= 4 do v := v + v + 4; // 4 -> 12
    edge a     when v <= 0 do v := v + v + 1; // 0 -> 1
    edge b     when v <= 1 do v := v + v + 2; // 1 -> 4

    edge abc.e when w <  v do w := w + 1;     // 12 times
end

group abc:
  controllable e;
end
