Loading CIF specification "controllercheck/bounded_response/option_max_cycle_states_4.cif".
Checking for bounded response:
    Converting CIF specification to a BDD representation:
        CIF variables and location pointers:
            Nr     Kind               Type        Name       Group  BDD vars  CIF values  BDD values  Values used
            -----  -----------------  ----------  ---------  -----  --------  ----------  ----------  -----------
            1      discrete variable  int[0..25]  p.counter  0      5 * 2     26 * 2      32 * 2      ~81%
            -----  -----------------  ----------  ---------  -----  --------  ----------  ----------  -----------
            Total                                            1      10        52          64          ~81%

        Skipping variable ordering: only one variable present.

        Restricting system behavior using state/event exclusion plant invariants:
            No guards changed.

    Computing states we can be in at the start of uncontrollable and controllable event loops:
        Round 1:
            Pre input states: true
            Pre uncontrollables states: false
            Pre controllables states: false

            Applying event loop for input variable events:
                No input variable events.

            Pre uncontrollables states: false -> true

            Applying event loop for uncontrollable events:
                No uncontrollable events.

            Pre controllables states: false -> true

            Applying event loop for controllable events:
                Iteration 1 (states before iteration: true):
                    Event c_dec: true -> p.counter != 25
                    Event c_inc: p.counter != 25 -> <bdd 9n 11p>
                    All iterations end states: <bdd 9n 11p>

                Iteration 2 (states before iteration: <bdd 9n 11p>):
                    Event c_dec: <bdd 9n 11p> -> p.counter != 25
                    Event c_inc: p.counter != 25 -> <bdd 9n 11p>
                    All iterations end states: <bdd 9n 11p>

        Round 2:
            Pre input states: true
            Pre uncontrollables states: true
            Pre controllables states: true

            Applying event loop for input variable events:
                No input variable events.

            Applying event loop for uncontrollable events:
                No uncontrollable events.

            Applying event loop for controllable events:
                Iteration 1 (states before iteration: true):
                    Event c_dec: true -> p.counter != 25
                    Event c_inc: p.counter != 25 -> <bdd 9n 11p>
                    All iterations end states: <bdd 9n 11p>

                Iteration 2 (states before iteration: <bdd 9n 11p>):
                    Event c_dec: <bdd 9n 11p> -> p.counter != 25
                    Event c_inc: p.counter != 25 -> <bdd 9n 11p>
                    All iterations end states: <bdd 9n 11p>

    Computing bound for uncontrollable events:
        No uncontrollable events.

        Bound: 0.

    Computing bound for controllable events:
        Iteration 1 (states before iteration: true):
            Event c_dec: true -> p.counter != 25
            Event c_inc: p.counter != 25 -> <bdd 9n 11p>

        Iteration 2 (states before iteration: <bdd 9n 11p>):
            Event c_dec: <bdd 9n 11p> -> p.counter != 25
            Event c_inc: p.counter != 25 -> <bdd 9n 11p>

        Bound: n/a (cycle).

    Bounded response check completed.

CONCLUSION:
    [ERROR] The specification does NOT have bounded response:

        - No transitions are possible for uncontrollable events.
        - An infinite sequence of transitions is possible for controllable events.

          Edges enabled in cycle(s):
            (event: c_dec) (guard: p.counter != 0) (assignments: p.counter := p.counter - 1)
            (event: c_inc) (guard: p.counter != 25) (assignments: p.counter := p.counter + 1)

          States in cycle(s) at end of event loop executions, limited to 4 'printed cycle states':
            p.counter=4
            p.counter=8
            p.counter=16
            p.counter=20
            ...

    [UNKNOWN] Non-blocking under control checking was disabled, non-blocking under control property is unknown.
    [UNKNOWN] Finite response checking was disabled, finite response property is unknown.
    [UNKNOWN] Confluence checking was disabled, confluence property is unknown.

The model with the check results has been written to "controllercheck/bounded_response/option_max_cycle_states_4.Bxxx.out.cif.real".
