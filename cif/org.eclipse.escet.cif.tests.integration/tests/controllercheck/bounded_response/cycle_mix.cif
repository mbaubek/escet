//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

// Uncontrollables bound: 1.
// Controllables bound: 1.

uncontrollable u_e1, u_e2, u_e3, u_c1, u_c2;
controllable c_e1, c_e2, c_e3, c_c1, c_c2;

plant p:
  location loc1:
    initial;
    marked;
    edge u_e1 goto loc3;
    edge c_c1 goto loc2;
  location loc2:
    edge c_c2 goto loc1;
  location loc3:
    edge c_e1 goto loc1;
    edge u_c1 goto loc4;
    edge u_e2 goto loc5;
  location loc4:
    edge u_c2 goto loc3;
  location loc5:
    edge u_e3 goto loc6;
  location loc6:
    edge c_e2 goto loc7;
  location loc7:
    edge c_e3 goto loc8;
  location loc8:
    marked;

  // Execution of program, round 1:
  // - Initial state: 'loc1'
  // - No input variables/events.
  // - Uncontrollable events:
  //   - Iteration 1:
  //     - Events 'u_c1' and 'u_c2' not possible in state 'loc1'.
  //     - Event 'u_e1': 'loc1' -> 'loc3'.
  //     - Event 'u_e2': 'loc3' -> 'loc5'.
  //     - Event 'u_e3': 'loc5' -> 'loc6'.
  //   - Iteration 2:
  //     - No uncontrollable events possible in 'loc6'.
  // - Controllable events:
  //   - Iteration 1:
  //     - Events 'c_c1', 'c_c2', 'c_e1' are not possible in 'loc6'.
  //     - Event 'c_e2': 'loc6' -> 'loc7'.
  //     - Event 'c_e3': 'loc7' -> 'loc8'.
  //   - Iteration 2:
  //     - No controllable events possible in 'loc8'.
  //
  // Execution of program, round 2:
  // - Can start from initial state, or end state of round 1, so from 'loc1 or loc8'.
  // - Same things are possible again from 'loc1'.
  // - Nothing is possible form 'loc8'.
  // - Uncontrollable events loop can thus start at 'loc1 or loc8' and end at 'loc6 or loc8'.
  // - Uncontrollable events loop can thus start at 'loc6 or loc8' and end at 'loc8'.
  //
  // Execution of program, round 3:
  // - Can start from initial state, the end state of round 1, or the end state of round 2, so from 'loc1 or loc8'.
  // - This is the same as for the 2nd round, so the result is again the same.
  // - We've reached a fixed point.
  // - In all cases, we only need a single iteration for each event loop.
  // - Both bounds are thus '1'.
end
