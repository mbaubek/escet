Reading CIF file "datasynth/inv_state_evt_exclusion_plant_comp.cif".

Preprocessing CIF specification (includes checking that the specification is supported).

Converting CIF specification to internal format (BDDs):
    CIF variables and location pointers:
        Nr     Kind               Type        Name  Group  BDD vars  CIF values  BDD values  Values used
        -----  -----------------  ----------  ----  -----  --------  ----------  ----------  -----------
        1      discrete variable  int[0..25]  a.x   0      5 * 2     26 * 2      32 * 2      ~81%
        -----  -----------------  ----------  ----  -----  --------  ----------  ----------  -----------
        Total                                       1      10        52          64          ~81%

    Skipping variable ordering: only one variable present.

Starting data-based synthesis.

Synthesis input:
    Invariant (components state plant inv):      true
    Invariant (locations state plant invariant): true
    Invariant (system state plant invariant):    true

    Invariant (components state req invariant):  true
    Invariant (locations state req invariant):   true
    Invariant (system state req invariant):      true

    Initial   (discrete variable 0):             a.x = 1
    Initial   (discrete variables):              a.x = 1
    Initial   (components init predicate):       true
    Initial   (aut/locs init predicate):         true
    Initial   (auts/locs init predicate):        true
    Initial   (uncontrolled system):             a.x = 1
    Initial   (system, combined init/plant inv): a.x = 1
    Initial   (system, combined init/state inv): a.x = 1

    Marked    (components marker predicate):     true
    Marked    (aut/locs marker predicate):       true
    Marked    (auts/locs marker predicate):      true
    Marked    (uncontrolled system):             true
    Marked    (system, combined mark/plant inv): true
    Marked    (system, combined mark/state inv): true

    State/event exclusion plants:
        Event "f" needs:
            (2 <= a.x and a.x <= 7 or 10 <= a.x and a.x <= 25) and (not(a.x = 4 or a.x = 5) and not(a.x = 2 or a.x = 3 or (a.x = 6 or a.x = 7)))
            0 <= a.x and a.x <= 15
        Event "g" needs:
            not(a.x = 0 or a.x = 2 or (a.x = 8 or a.x = 10)) and not(a.x = 4 or a.x = 6) and (not(a.x = 1 or a.x = 9) and (a.x != 5 and not(a.x = 3 or a.x = 7)))
            not(a.x = 16 or a.x = 18 or (a.x = 20 or (a.x = 22 or a.x = 24))) and not(a.x = 17 or (a.x = 21 or a.x = 25)) and (a.x != 19 and (a.x != 23 and a.x != 15))
        Event "h" needs:
            (4 <= a.x and a.x <= 7 or 12 <= a.x and a.x <= 25) and not(4 <= a.x and a.x <= 7)
            (0 <= a.x and a.x <= 15 or a.x = 18 or (a.x = 19 or (a.x = 22 or a.x = 23))) and not(a.x = 18 or a.x = 19) and (not(a.x = 22 or a.x = 23) and not(a.x = 14 or a.x = 15))
        Event "j" needs:
            (2 <= a.x and a.x <= 7 or 10 <= a.x and a.x <= 25) and (not(a.x = 4 or a.x = 5) and not(a.x = 2 or a.x = 3 or (a.x = 6 or a.x = 7)))
            0 <= a.x and a.x <= 15
        Event "k" needs:
            not(a.x = 0 or a.x = 2 or (a.x = 8 or a.x = 10)) and not(a.x = 4 or a.x = 6) and (not(a.x = 1 or a.x = 9) and (a.x != 5 and not(a.x = 3 or a.x = 7)))
            not(a.x = 16 or a.x = 18 or (a.x = 20 or (a.x = 22 or a.x = 24))) and not(a.x = 17 or (a.x = 21 or a.x = 25)) and (a.x != 19 and (a.x != 23 and a.x != 15))
        Event "l" needs:
            (4 <= a.x and a.x <= 7 or 12 <= a.x and a.x <= 25) and not(4 <= a.x and a.x <= 7)
            (0 <= a.x and a.x <= 15 or a.x = 18 or (a.x = 19 or (a.x = 22 or a.x = 23))) and not(a.x = 18 or a.x = 19) and (not(a.x = 22 or a.x = 23) and not(a.x = 14 or a.x = 15))

    State/event exclusion requirements:
        None

    Uncontrolled system (state/event exclusion plants not applied yet):
        State: (controlled-behavior: ?)
            Edge: (event: e) (guard: 0 <= a.x and (a.x <= 23 and not(20 <= a.x and a.x <= 23))) (assignments: a.x := a.x + 1)
            Edge: (event: f) (guard: true)
            Edge: (event: g) (guard: true)
            Edge: (event: h) (guard: true)
            Edge: (event: i) (guard: 0 <= a.x and (a.x <= 23 and not(20 <= a.x and a.x <= 23))) (assignments: a.x := a.x + 1)
            Edge: (event: j) (guard: true)
            Edge: (event: k) (guard: true)
            Edge: (event: l) (guard: true)

Checking input for potential problems.

Restricting edge guards to prevent runtime errors:
    No guards changed.

Restricting uncontrolled system behavior using state/event exclusion plant invariants:
    Edge (event: f) (guard: true): guard: true -> a.x = 12 or (a.x = 13 or a.x = 10) or (a.x = 11 or (a.x = 14 or a.x = 15)) [plant: a.x = 12 or (a.x = 13 or a.x = 10) or (a.x = 11 or (a.x = 14 or a.x = 15))].
    Edge (event: g) (guard: true): guard: true -> a.x = 12 or a.x = 14 or (a.x = 13 or a.x = 11) [plant: a.x = 12 or a.x = 14 or (a.x = 13 or a.x = 11)].
    Edge (event: h) (guard: true): guard: true -> a.x = 12 or a.x = 13 [plant: a.x = 12 or a.x = 13].
    Edge (event: j) (guard: true): guard: true -> a.x = 12 or (a.x = 13 or a.x = 10) or (a.x = 11 or (a.x = 14 or a.x = 15)) [plant: a.x = 12 or (a.x = 13 or a.x = 10) or (a.x = 11 or (a.x = 14 or a.x = 15))].
    Edge (event: k) (guard: true): guard: true -> a.x = 12 or a.x = 14 or (a.x = 13 or a.x = 11) [plant: a.x = 12 or a.x = 14 or (a.x = 13 or a.x = 11)].
    Edge (event: l) (guard: true): guard: true -> a.x = 12 or a.x = 13 [plant: a.x = 12 or a.x = 13].

    Uncontrolled system:
        State: (controlled-behavior: ?)
            Edge: (event: e) (guard: 0 <= a.x and (a.x <= 23 and not(20 <= a.x and a.x <= 23))) (assignments: a.x := a.x + 1)
            Edge: (event: f) (guard: true -> a.x = 12 or (a.x = 13 or a.x = 10) or (a.x = 11 or (a.x = 14 or a.x = 15)))
            Edge: (event: g) (guard: true -> a.x = 12 or a.x = 14 or (a.x = 13 or a.x = 11))
            Edge: (event: h) (guard: true -> a.x = 12 or a.x = 13)
            Edge: (event: i) (guard: 0 <= a.x and (a.x <= 23 and not(20 <= a.x and a.x <= 23))) (assignments: a.x := a.x + 1)
            Edge: (event: j) (guard: true -> a.x = 12 or (a.x = 13 or a.x = 10) or (a.x = 11 or (a.x = 14 or a.x = 15)))
            Edge: (event: k) (guard: true -> a.x = 12 or a.x = 14 or (a.x = 13 or a.x = 11))
            Edge: (event: l) (guard: true -> a.x = 12 or a.x = 13)

Initializing edges for being applied.

Restricting uncontrolled system behavior using state plant invariants:
    No restrictions needed.

Initializing controlled behavior:
    Controlled-behavior predicate: true.
    Controlled-initialization predicate: a.x = 1.

Restricting behavior using state requirements:
    Controlled behavior not changed.

Extending controlled-behavior predicate using variable ranges:
    Controlled behavior: true -> true [range: true, variable: discrete variable "a.x" of type "int[0..25]" (group: 0, domain: 0+1, BDD variables: 5, CIF/BDD values: 26/32)].

    Extended controlled-behavior predicate using variable ranges: true.

Restricting behavior using state/event exclusion requirements:
    Guards and controlled behavior not changed.

Restricting behavior using implicit runtime error requirements:
    Controlled behavior not changed.

Re-initializing edges for being applied.

Checking pre-synthesis for events that are never enabled.

Synthesis round 1:
    Computing backward controlled-behavior predicate:
        Saturation matrix:
                /---------- 1  a.x#0
                |/--------- 2  a.x#0+
                ||/-------- 3  a.x#1
                |||/------- 4  a.x#1+
                ||||/------ 5  a.x#2
                |||||/----- 6  a.x#2+
                ||||||/---- 7  a.x#3
                |||||||/--- 8  a.x#3+
                ||||||||/-- 9  a.x#4
                |||||||||/- 10 a.x#4+
            1 e rwrwrwrwrw
            2 g r r r r r
            3 i rwrwrwrwrw
            4 k r r r r r
            5 f   r r r r
            6 h   r r r r
            7 j   r r r r
            8 l   r r r r

        Backward controlled-behavior: true [marker predicate]
        Backward controlled-behavior: true -> true [restricted to current/previous controlled-behavior predicate: true]

        Backward controlled-behavior: true [fixed point].

        Controlled behavior not changed.

    Computing backward uncontrolled bad-state predicate:
        Saturation matrix:
                /---------- 1  a.x#0
                |/--------- 2  a.x#0+
                ||/-------- 3  a.x#1
                |||/------- 4  a.x#1+
                ||||/------ 5  a.x#2
                |||||/----- 6  a.x#2+
                ||||||/---- 7  a.x#3
                |||||||/--- 8  a.x#3+
                ||||||||/-- 9  a.x#4
                |||||||||/- 10 a.x#4+
            1 i rwrwrwrwrw
            2 k r r r r r
            3 j   r r r r
            4 l   r r r r

        Backward uncontrolled bad-state: false [current/previous controlled behavior predicate]

        Controlled behavior not changed.

    Computing forward controlled-behavior predicate:
        Saturation matrix:
                /---------- 1  a.x#0
                |/--------- 2  a.x#0+
                ||/-------- 3  a.x#1
                |||/------- 4  a.x#1+
                ||||/------ 5  a.x#2
                |||||/----- 6  a.x#2+
                ||||||/---- 7  a.x#3
                |||||||/--- 8  a.x#3+
                ||||||||/-- 9  a.x#4
                |||||||||/- 10 a.x#4+
            1 e rwrwrwrwrw
            2 g r r r r r
            3 i rwrwrwrwrw
            4 k r r r r r
            5 f   r r r r
            6 h   r r r r
            7 j   r r r r
            8 l   r r r r

        Forward controlled-behavior: a.x = 1 [initialization predicate]
        Forward controlled-behavior: a.x = 1 -> a.x = 2 or a.x = 1 [forward reach using bounded saturation (transition: 1 of 8) (level: 1 of 10) with edge: (event: e) (guard: 0 <= a.x and (a.x <= 23 and not(20 <= a.x and a.x <= 23))) (assignments: a.x := a.x + 1)]
        Forward controlled-behavior: a.x = 2 or a.x = 1 -> a.x = 2 or (a.x = 1 or a.x = 3) [forward reach using bounded saturation (transition: 3 of 8) (level: 1 of 10) with edge: (event: i) (guard: 0 <= a.x and (a.x <= 23 and not(20 <= a.x and a.x <= 23))) (assignments: a.x := a.x + 1)]
        Forward controlled-behavior: a.x = 2 or (a.x = 1 or a.x = 3) -> a.x = 4 or a.x = 2 or (a.x = 1 or a.x = 3) [forward reach using bounded saturation (transition: 1 of 8) (level: 1 of 10) with edge: (event: e) (guard: 0 <= a.x and (a.x <= 23 and not(20 <= a.x and a.x <= 23))) (assignments: a.x := a.x + 1)]
        Forward controlled-behavior: a.x = 4 or a.x = 2 or (a.x = 1 or a.x = 3) -> a.x = 4 or a.x = 2 or (a.x = 1 or (a.x = 5 or a.x = 3)) [forward reach using bounded saturation (transition: 3 of 8) (level: 1 of 10) with edge: (event: i) (guard: 0 <= a.x and (a.x <= 23 and not(20 <= a.x and a.x <= 23))) (assignments: a.x := a.x + 1)]
        Forward controlled-behavior: a.x = 4 or a.x = 2 or (a.x = 1 or (a.x = 5 or a.x = 3)) -> a.x = 4 or (a.x = 2 or a.x = 6) or (a.x = 1 or (a.x = 5 or a.x = 3)) [forward reach using bounded saturation (transition: 1 of 8) (level: 1 of 10) with edge: (event: e) (guard: 0 <= a.x and (a.x <= 23 and not(20 <= a.x and a.x <= 23))) (assignments: a.x := a.x + 1)]
        Forward controlled-behavior: a.x = 4 or (a.x = 2 or a.x = 6) or (a.x = 1 or (a.x = 5 or a.x = 3)) -> a.x = 4 or (a.x = 2 or a.x = 6) or (a.x = 1 or a.x = 3 or (a.x = 5 or a.x = 7)) [forward reach using bounded saturation (transition: 3 of 8) (level: 1 of 10) with edge: (event: i) (guard: 0 <= a.x and (a.x <= 23 and not(20 <= a.x and a.x <= 23))) (assignments: a.x := a.x + 1)]
        Forward controlled-behavior: a.x = 4 or (a.x = 2 or a.x = 6) or (a.x = 1 or a.x = 3 or (a.x = 5 or a.x = 7)) -> a.x = 8 or a.x = 4 or (a.x = 2 or a.x = 6) or (a.x = 1 or a.x = 3 or (a.x = 5 or a.x = 7)) [forward reach using bounded saturation (transition: 1 of 8) (level: 1 of 10) with edge: (event: e) (guard: 0 <= a.x and (a.x <= 23 and not(20 <= a.x and a.x <= 23))) (assignments: a.x := a.x + 1)]
        Forward controlled-behavior: a.x = 8 or a.x = 4 or (a.x = 2 or a.x = 6) or (a.x = 1 or a.x = 3 or (a.x = 5 or a.x = 7)) -> a.x = 8 or a.x = 4 or (a.x = 2 or a.x = 6) or (a.x = 1 or a.x = 9 or (a.x = 5 or (a.x = 3 or a.x = 7))) [forward reach using bounded saturation (transition: 3 of 8) (level: 1 of 10) with edge: (event: i) (guard: 0 <= a.x and (a.x <= 23 and not(20 <= a.x and a.x <= 23))) (assignments: a.x := a.x + 1)]
        Forward controlled-behavior: a.x = 8 or a.x = 4 or (a.x = 2 or a.x = 6) or (a.x = 1 or a.x = 9 or (a.x = 5 or (a.x = 3 or a.x = 7))) -> a.x = 8 or a.x = 4 or (a.x = 2 or (a.x = 10 or a.x = 6)) or (a.x = 1 or a.x = 9 or (a.x = 5 or (a.x = 3 or a.x = 7))) [forward reach using bounded saturation (transition: 1 of 8) (level: 1 of 10) with edge: (event: e) (guard: 0 <= a.x and (a.x <= 23 and not(20 <= a.x and a.x <= 23))) (assignments: a.x := a.x + 1)]
        Forward controlled-behavior: a.x = 8 or a.x = 4 or (a.x = 2 or (a.x = 10 or a.x = 6)) or (a.x = 1 or a.x = 9 or (a.x = 5 or (a.x = 3 or a.x = 7))) -> a.x = 8 or a.x = 4 or (a.x = 2 or (a.x = 10 or a.x = 6)) or (a.x = 1 or (a.x = 3 or a.x = 9) or (a.x = 11 or (a.x = 5 or a.x = 7))) [forward reach using bounded saturation (transition: 3 of 8) (level: 1 of 10) with edge: (event: i) (guard: 0 <= a.x and (a.x <= 23 and not(20 <= a.x and a.x <= 23))) (assignments: a.x := a.x + 1)]
        Forward controlled-behavior: a.x = 8 or a.x = 4 or (a.x = 2 or (a.x = 10 or a.x = 6)) or (a.x = 1 or (a.x = 3 or a.x = 9) or (a.x = 11 or (a.x = 5 or a.x = 7))) -> a.x = 8 or (a.x = 4 or a.x = 12) or (a.x = 2 or (a.x = 10 or a.x = 6)) or (a.x = 1 or (a.x = 3 or a.x = 9) or (a.x = 11 or (a.x = 5 or a.x = 7))) [forward reach using bounded saturation (transition: 1 of 8) (level: 1 of 10) with edge: (event: e) (guard: 0 <= a.x and (a.x <= 23 and not(20 <= a.x and a.x <= 23))) (assignments: a.x := a.x + 1)]
        Forward controlled-behavior: a.x = 8 or (a.x = 4 or a.x = 12) or (a.x = 2 or (a.x = 10 or a.x = 6)) or (a.x = 1 or (a.x = 3 or a.x = 9) or (a.x = 11 or (a.x = 5 or a.x = 7))) -> a.x = 8 or (a.x = 4 or a.x = 12) or (a.x = 2 or (a.x = 10 or a.x = 6)) or (a.x = 1 or (a.x = 5 or a.x = 9) or (a.x = 13 or a.x = 3 or (a.x = 11 or a.x = 7))) [forward reach using bounded saturation (transition: 3 of 8) (level: 1 of 10) with edge: (event: i) (guard: 0 <= a.x and (a.x <= 23 and not(20 <= a.x and a.x <= 23))) (assignments: a.x := a.x + 1)]
        Forward controlled-behavior: a.x = 8 or (a.x = 4 or a.x = 12) or (a.x = 2 or (a.x = 10 or a.x = 6)) or (a.x = 1 or (a.x = 5 or a.x = 9) or (a.x = 13 or a.x = 3 or (a.x = 11 or a.x = 7))) -> not(a.x = 0 or a.x = 16) and a.x != 24 and (a.x != 20 and not(a.x = 18 or a.x = 22)) and (not(a.x = 17 or (a.x = 21 or a.x = 25)) and a.x != 19 and (a.x != 23 and a.x != 15)) [forward reach using bounded saturation (transition: 1 of 8) (level: 1 of 10) with edge: (event: e) (guard: 0 <= a.x and (a.x <= 23 and not(20 <= a.x and a.x <= 23))) (assignments: a.x := a.x + 1)]
        Forward controlled-behavior: not(a.x = 0 or a.x = 16) and a.x != 24 and (a.x != 20 and not(a.x = 18 or a.x = 22)) and (not(a.x = 17 or (a.x = 21 or a.x = 25)) and a.x != 19 and (a.x != 23 and a.x != 15)) -> not(a.x = 0 or a.x = 16) and a.x != 24 and (a.x != 20 and (not(a.x = 18 or a.x = 22) and (0 <= a.x and a.x <= 16 or a.x = 18 or (a.x = 20 or (a.x = 22 or a.x = 24))))) [forward reach using bounded saturation (transition: 3 of 8) (level: 1 of 10) with edge: (event: i) (guard: 0 <= a.x and (a.x <= 23 and not(20 <= a.x and a.x <= 23))) (assignments: a.x := a.x + 1)]
        Forward controlled-behavior: not(a.x = 0 or a.x = 16) and a.x != 24 and (a.x != 20 and (not(a.x = 18 or a.x = 22) and (0 <= a.x and a.x <= 16 or a.x = 18 or (a.x = 20 or (a.x = 22 or a.x = 24))))) -> a.x != 0 and a.x != 24 and (a.x != 20 and (not(a.x = 18 or a.x = 22) and (0 <= a.x and a.x <= 16 or a.x = 18 or (a.x = 20 or (a.x = 22 or a.x = 24))))) [forward reach using bounded saturation (transition: 1 of 8) (level: 1 of 10) with edge: (event: e) (guard: 0 <= a.x and (a.x <= 23 and not(20 <= a.x and a.x <= 23))) (assignments: a.x := a.x + 1)]
        Forward controlled-behavior: a.x != 0 and a.x != 24 and (a.x != 20 and (not(a.x = 18 or a.x = 22) and (0 <= a.x and a.x <= 16 or a.x = 18 or (a.x = 20 or (a.x = 22 or a.x = 24))))) -> a.x != 0 and (a.x != 24 and a.x != 20) and (not(a.x = 18 or a.x = 22) and a.x != 25 and (a.x != 21 and not(a.x = 19 or a.x = 23))) [forward reach using bounded saturation (transition: 3 of 8) (level: 1 of 10) with edge: (event: i) (guard: 0 <= a.x and (a.x <= 23 and not(20 <= a.x and a.x <= 23))) (assignments: a.x := a.x + 1)]
        Forward controlled-behavior: a.x != 0 and (a.x != 24 and a.x != 20) and (not(a.x = 18 or a.x = 22) and a.x != 25 and (a.x != 21 and not(a.x = 19 or a.x = 23))) -> a.x != 0 and (a.x != 24 and a.x != 20) and (a.x != 22 and a.x != 25 and (a.x != 21 and not(a.x = 19 or a.x = 23))) [forward reach using bounded saturation (transition: 1 of 8) (level: 1 of 10) with edge: (event: e) (guard: 0 <= a.x and (a.x <= 23 and not(20 <= a.x and a.x <= 23))) (assignments: a.x := a.x + 1)]
        Forward controlled-behavior: a.x != 0 and (a.x != 24 and a.x != 20) and (a.x != 22 and a.x != 25 and (a.x != 21 and not(a.x = 19 or a.x = 23))) -> a.x != 0 and (a.x != 24 and a.x != 20) and (a.x != 22 and (a.x != 25 and not(a.x = 21 or a.x = 23))) [forward reach using bounded saturation (transition: 3 of 8) (level: 1 of 10) with edge: (event: i) (guard: 0 <= a.x and (a.x <= 23 and not(20 <= a.x and a.x <= 23))) (assignments: a.x := a.x + 1)]
        Forward controlled-behavior: a.x != 0 and (a.x != 24 and a.x != 20) and (a.x != 22 and (a.x != 25 and not(a.x = 21 or a.x = 23))) -> a.x != 0 and a.x != 24 and (a.x != 22 and (a.x != 25 and not(a.x = 21 or a.x = 23))) [forward reach using bounded saturation (transition: 1 of 8) (level: 1 of 10) with edge: (event: e) (guard: 0 <= a.x and (a.x <= 23 and not(20 <= a.x and a.x <= 23))) (assignments: a.x := a.x + 1)]

        Forward controlled-behavior: a.x != 0 and a.x != 24 and (a.x != 22 and (a.x != 25 and not(a.x = 21 or a.x = 23))) [fixed point].

        Controlled behavior: true -> a.x != 0 and a.x != 24 and (a.x != 22 and (a.x != 25 and not(a.x = 21 or a.x = 23))).

    Need another round.

Synthesis round 2:
    Computing backward controlled-behavior predicate:
        Saturation matrix:
                /---------- 1  a.x#0
                |/--------- 2  a.x#0+
                ||/-------- 3  a.x#1
                |||/------- 4  a.x#1+
                ||||/------ 5  a.x#2
                |||||/----- 6  a.x#2+
                ||||||/---- 7  a.x#3
                |||||||/--- 8  a.x#3+
                ||||||||/-- 9  a.x#4
                |||||||||/- 10 a.x#4+
            1 e rwrwrwrwrw
            2 g r r r r r
            3 i rwrwrwrwrw
            4 k r r r r r
            5 f   r r r r
            6 h   r r r r
            7 j   r r r r
            8 l   r r r r

        Backward controlled-behavior: true [marker predicate]
        Backward controlled-behavior: true -> a.x != 0 and a.x != 24 and (a.x != 22 and (a.x != 25 and not(a.x = 21 or a.x = 23))) [restricted to current/previous controlled-behavior predicate: a.x != 0 and a.x != 24 and (a.x != 22 and (a.x != 25 and not(a.x = 21 or a.x = 23)))]

        Backward controlled-behavior: a.x != 0 and a.x != 24 and (a.x != 22 and (a.x != 25 and not(a.x = 21 or a.x = 23))) [fixed point].

        Controlled behavior not changed.

    Computing backward uncontrolled bad-state predicate:
        Saturation matrix:
                /---------- 1  a.x#0
                |/--------- 2  a.x#0+
                ||/-------- 3  a.x#1
                |||/------- 4  a.x#1+
                ||||/------ 5  a.x#2
                |||||/----- 6  a.x#2+
                ||||||/---- 7  a.x#3
                |||||||/--- 8  a.x#3+
                ||||||||/-- 9  a.x#4
                |||||||||/- 10 a.x#4+
            1 i rwrwrwrwrw
            2 k r r r r r
            3 j   r r r r
            4 l   r r r r

        Backward uncontrolled bad-state: a.x = 0 or (a.x = 24 or a.x = 22) or (a.x = 25 or (a.x = 21 or a.x = 23)) [current/previous controlled behavior predicate]

        Controlled behavior not changed.

    Finished: controlled behavior is stable.

Computing final controlled system guards:
    No guards changed.

Cleaning up cached predicate of edges that were used when applying edges.

Final synthesis result:
    State: (controlled-behavior: a.x != 0 and a.x != 24 and (a.x != 22 and (a.x != 25 and not(a.x = 21 or a.x = 23))))
        Edge: (event: e) (guard: 0 <= a.x and (a.x <= 23 and not(20 <= a.x and a.x <= 23))) (assignments: a.x := a.x + 1)
        Edge: (event: f) (guard: true -> a.x = 12 or (a.x = 13 or a.x = 10) or (a.x = 11 or (a.x = 14 or a.x = 15)))
        Edge: (event: g) (guard: true -> a.x = 12 or a.x = 14 or (a.x = 13 or a.x = 11))
        Edge: (event: h) (guard: true -> a.x = 12 or a.x = 13)
        Edge: (event: i) (guard: 0 <= a.x and (a.x <= 23 and not(20 <= a.x and a.x <= 23))) (assignments: a.x := a.x + 1)
        Edge: (event: j) (guard: true -> a.x = 12 or (a.x = 13 or a.x = 10) or (a.x = 11 or (a.x = 14 or a.x = 15)))
        Edge: (event: k) (guard: true -> a.x = 12 or a.x = 14 or (a.x = 13 or a.x = 11))
        Edge: (event: l) (guard: true -> a.x = 12 or a.x = 13)

Computing initialization predicate of the controlled system.

Controlled system: exactly 20 states.

Determining initialization predicate for output model:
    Initial (synthesis result):            a.x != 0 and a.x != 24 and (a.x != 22 and (a.x != 25 and not(a.x = 21 or a.x = 23)))
    Initial (uncontrolled system):         a.x = 1
    Initial (controlled system):           a.x = 1
    Initial (removed by supervisor):       false
    Initial (added by supervisor):         true

    Initial (output model):                n/a

Determining supervisor guards for output model:
    Event e: guard: 0 <= a.x and (a.x <= 23 and not(20 <= a.x and a.x <= 23)).
    Event f: guard: a.x = 12 or (a.x = 13 or a.x = 10) or (a.x = 11 or (a.x = 14 or a.x = 15)).
    Event g: guard: a.x = 12 or a.x = 14 or (a.x = 13 or a.x = 11).
    Event h: guard: a.x = 12 or a.x = 13.

Checking post-synthesis for events that are never enabled.

Simplifying supervisor guards for output model:
    Simplification under the assumption of the plants.

    Event e: guard: 0 <= a.x and (a.x <= 23 and not(20 <= a.x and a.x <= 23)) -> true [assume 0 <= a.x and (a.x <= 23 and not(20 <= a.x and a.x <= 23))].

Constructing output CIF specification.

Checking output CIF specification.

Writing output CIF file "datasynth/inv_state_evt_exclusion_plant_comp.ctrlsys.real.cif".
