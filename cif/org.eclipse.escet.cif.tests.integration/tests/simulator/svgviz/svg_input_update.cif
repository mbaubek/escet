//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

// NOTE: This test case tests code generation, but no SVG input is actually
// selected. Perform manual simulation using svg_input_update.tooldef to
// actually test SVG input.

svgfile "svg_input_update.svg";

input bool i, j;

group g:
  input int[-1..4] i;
end

// Set 'i' to 'true'.
svgin id "x1" do i := true;

// Set 'i' to 'false'.
svgin id "x2" do i := false;

// Flip bit 'i'.
svgin id "x3" do i := not i;

// Flip bit 'i', using 'if' update.
svgin id "x4" do if i: i := false else i := true end;

// Swap bits 'i' and 'j'.
svgin id "x5" do (i, j) := (j, i);

// Increment 'g.i' (error once reaches value '5').
svgin id "x6" do g.i := g.i + 1;

// Set 'g.i' to '-1' (forbidden by target state invariant).
svgin id "x7" do g.i := -1;
invariant g.i != -1;

// Debug SVG output is disabled in ToolDef; enable to test SVG input debugging.
