ORGANIZATION_BLOCK MAIN
{ S7_Optimized_Access := 'true' }
    VAR_TEMP
        curValue: TIME;
        isProgress: BOOL;
        timeOut: BOOL;
        dummyVar1: DINT;
        dummyVar2: DINT;
        dummyVar3: DINT;
        dummyVar4: DINT;
        dummyVar5: DINT;
    END_VAR

BEGIN
    (* Header text file for:
     *  -> (-*-) CIF PLC code generator.
     *)

    (* An overview of the CIF model automata with their events and variables is written after the main code. *)



    (* --- Read PLC inputs. ---------------------------------------------------- *)
    (* Read PLC input and write it to input variable "hw_button.bit". *)
    "DB".hw_button_bit := in_hw_button_bit;
    (* Read PLC input and write it to input variable "hw_button.bot". *)
    "DB".hw_button_bot := in_hw_button_bot;
    (* Read PLC input and write it to input variable "hw_button.bat". *)
    "DB".hw_button_bat := REAL_TO_LREAL(in_hw_button_bat);

    (* --- Initialize state or update continuous variables. -------------------- *)
    IF "DB".firstRun THEN
        "DB".firstRun := FALSE;

        (* Initialize the state variables. *)
        (* Initialize current-location variable for automaton "timer". *)
        "DB".timer_1 := timer_Idle;
        (* Initialize current-location variable for automaton "sup". *)
        "DB".sup := sup_s1;
        (* Initialize discrete variable "hw_button.r". *)
        "DB".hw_button_r := 0.0;
        (* Initialize current-location variable for automaton "hw_button". *)
        "DB".hw_button := hw_button_Released;
        (* Initialize current-location variable for automaton "hw_lamp". *)
        "DB".hw_lamp := hw_lamp_Off;
        (* Initialize continuous variable "timer.t". *)
        "DB".timer_t := 0.0;
        (* Reset timer of "timer_t". *)
        "DB".preset_timer_t := LINT_TO_TIME(LREAL_TO_LINT("DB".timer_t * 1000.0));
        ton_timer_t.TON(IN := FALSE, PT := "DB".preset_timer_t);
        ton_timer_t.TON(IN := TRUE, PT := "DB".preset_timer_t);
    ELSE
        (* Update remaining time of continuous variable "timer.t". *)
        ton_timer_t.TON(IN := TRUE, PT := "DB".preset_timer_t, Q => timeOut, ET => curValue);
        "DB".timer_t := SEL_LREAL(G := timeOut, IN0 := MAX(IN1 := LINT_TO_LREAL(TIME_TO_LINT("DB".preset_timer_t - curValue)) / 1000.0, IN2 := 0.0), IN1 := 0.0);
    END_IF;

    (* --- Process uncontrollable events. -------------------------------------- *)
    isProgress := TRUE;
    (* Perform events until none can be done anymore. *)
    WHILE isProgress DO
        isProgress := FALSE;

        isProgress := tryUncon_lb_group(isProgress);
        isProgress := tryUncon_specification(isProgress);
    END_WHILE;

    (* --- Process controllable events. ---------------------------------------- *)
    isProgress := TRUE;
    (* Perform events until none can be done anymore. *)
    WHILE isProgress DO
        isProgress := FALSE;

        isProgress := tryCon_lb_group(isProgress);
        isProgress := tryCon_specification(isProgress);
    END_WHILE;

    (* --- Write PLC outputs. -------------------------------------------------- *)
    (* Write algebraic variable "hw_lamp.bit" to PLC output. *)
    out_hw_lamp_bit := "DB".hw_lamp = hw_lamp_On;

    (*------------------------------------------------------
     * CIF model overview:
     *
     * ----
     * Automaton "hw_button":
     *
     * - Input variable "hw_button.bat".
     * - Input variable "hw_button.bit".
     * - Input variable "hw_button.bot".
     * - Discrete variable "hw_button.r".
     *
     * - PLC current-location variable for automaton "hw_button".
     *   The current location of the automaton. It amongst others contains the edges that are considered for taking an event.
     *
     * - PLC edge selection variable "edge_hw_button".
     *   Unique edge number of the edge within the automaton that was selected to perform the considered event.
     *
     * - Uncontrollable event "lb_group.push".
     * - Uncontrollable event "lb_group.release".
     *
     * - No use of controllable events.
     *
     * ----
     * Automaton "hw_lamp":
     *
     * - PLC current-location variable for automaton "hw_lamp".
     *   The current location of the automaton. It amongst others contains the edges that are considered for taking an event.
     *
     * - PLC edge selection variable "edge_hw_lamp".
     *   Unique edge number of the edge within the automaton that was selected to perform the considered event.
     *
     * - No use of uncontrollable events.
     *
     * - Controllable event "lb_group.off".
     * - Controllable event "lb_group.on".
     *
     * ----
     * Automaton "sup":
     *
     * - PLC current-location variable for automaton "sup".
     *   The current location of the automaton. It amongst others contains the edges that are considered for taking an event.
     *
     * - PLC edge selection variable "edge_sup".
     *   Unique edge number of the edge within the automaton that was selected to perform the considered event.
     *
     * - Uncontrollable event "lb_group.push".
     * - Uncontrollable event "lb_group.release".
     * - Uncontrollable event "timeout".
     *
     * - Controllable event "lb_group.off".
     * - Controllable event "lb_group.on".
     * - Controllable event "start".
     *
     * ----
     * Automaton "timer":
     *
     * - Continuous variable "timer.t".
     *
     * - PLC current-location variable for automaton "timer".
     *   The current location of the automaton. It amongst others contains the edges that are considered for taking an event.
     *
     * - PLC edge selection variable "edge_timer".
     *   Unique edge number of the edge within the automaton that was selected to perform the considered event.
     *
     * - Uncontrollable event "timeout".
     *
     * - Controllable event "start".
     *------------------------------------------------------ *)
END_ORGANIZATION_BLOCK
