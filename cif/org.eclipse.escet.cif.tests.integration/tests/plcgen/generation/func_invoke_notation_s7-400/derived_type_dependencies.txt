Direct dependencies:
Each line has the form 'TYPE <- DEPENDENCY_1 DEPENDENCY_2 ...'.
To be able to use type 'TYPE', all 'DEPENDENCY_*' types must be available already.

    TupleStruct2 <-

None of the derived types has a dependency on another derived type.
