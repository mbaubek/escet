FUNCTION tryEvent_c_sync09: BOOL
{ S7_Optimized_Access := 'true' }
    VAR_INPUT
        isProgress: BOOL;
    END_VAR
    VAR_TEMP
        funcIsProgress: BOOL;
        current_sync09a_count: DINT;
        current_sync09b_count1: DINT;
        current_sync09b_count2: DINT;
        edge_sync09a: BOOL;
        edge_sync09b: BYTE;
        eventEnabled: BOOL;
        dummyVar1: DINT;
        dummyVar2: DINT;
    END_VAR

BEGIN
    funcIsProgress := isProgress;
    (*************************************************************
     * Try to perform controllable event "c_sync09".
     *
     * - Automaton "sync09a" must always synchronize.
     * - Automaton "sync09b" must always synchronize.
     *************************************************************)
    eventEnabled := TRUE;
    (*******************************
     * Check each synchronizing automaton for having an edge with a true guard.
     *******************************)
    (***********
     * Test edge of automaton "sync09a" to synchronize for event "c_sync09".
     * This automaton must have an edge with a true guard to allow the event.
     *
     * Edge being tested:
     * - Location:
     *   - 1st edge in the location
     ***********)
    IF "DB".sync09a_count < 30 THEN
        edge_sync09a := 0;
    ELSE
        (* The automaton has no edge with a true guard. Skip to the next event. *)
        eventEnabled := FALSE;
    END_IF;
    IF eventEnabled THEN
        (***********
         * Test edges of automaton "sync09b" to synchronize for event "c_sync09".
         * This automaton must have an edge with a true guard to allow the event.
         *
         * Edges being tested:
         * - Location:
         *   - 1st edge in the location
         *   - 2nd edge in the location
         *   - 3rd edge in the location
         ***********)
        IF TRUE AND "DB".sync09a_count < 5 THEN
            edge_sync09b := 0;
        ELSIF TRUE AND "DB".sync09a_count < 10 THEN
            edge_sync09b := 1;
        ELSIF TRUE AND "DB".sync09a_count < 20 THEN
            edge_sync09b := 2;
        ELSE
            (* The automaton has no edge with a true guard. Skip to the next event. *)
            eventEnabled := FALSE;
        END_IF;
    END_IF;
    (* All checks have been done. If variable "eventEnabled" still holds, event "c_sync09" can occur. *)
    IF eventEnabled THEN
        funcIsProgress := TRUE;
        (* Make temporary copies of assigned variables to preserve the old values while assigning new values. *)
        current_sync09a_count := "DB".sync09a_count;
        current_sync09b_count1 := "DB".sync09b_count1;
        current_sync09b_count2 := "DB".sync09b_count2;
        (*******************************
         * Perform the assignments of each synchronizing automaton.
         *******************************)
        (* Perform assignments of automaton "sync09a". *)
        IF edge_sync09a = 0 THEN
            (* Perform assignments of the 1st edge of automaton "sync09a". *)
            (* Perform update of discrete variable "sync09a.count". *)
            "DB".sync09a_count := current_sync09a_count + 1;
        END_IF;
        (* Perform assignments of automaton "sync09b". *)
        IF edge_sync09b = 0 THEN
            (* Perform assignments of the 1st edge of automaton "sync09b". *)
            (* Perform update of discrete variable "sync09b.count1". *)
            "DB".sync09b_count1 := current_sync09b_count1 + 1;
            (* Perform update of discrete variable "sync09b.count2". *)
            "DB".sync09b_count2 := current_sync09b_count2 + 1;
        ELSIF edge_sync09b = 1 THEN
            (* Perform assignments of the 2nd edge of automaton "sync09b". *)
            (* Perform update of discrete variable "sync09b.count1". *)
            "DB".sync09b_count1 := current_sync09b_count1 + 1;
            (* Perform update of discrete variable "sync09b.count2". *)
            "DB".sync09b_count2 := current_sync09b_count2 + 2;
        ELSIF edge_sync09b = 2 THEN
            (* Perform assignments of the 3rd edge of automaton "sync09b". *)
            (* Perform update of discrete variable "sync09b.count1". *)
            "DB".sync09b_count1 := current_sync09b_count1 + 1;
            (* Perform update of discrete variable "sync09b.count2". *)
            "DB".sync09b_count2 := current_sync09b_count2 + 3;
        END_IF;
    END_IF;

    (* Return event execution progress. *)
    tryEvent_c_sync09 := funcIsProgress;
    RETURN;
END_FUNCTION
