FUNCTION tryEvent_c_sync06b: BOOL
{ S7_Optimized_Access := 'true' }
    VAR_INPUT
        isProgress: BOOL;
    END_VAR
    VAR_TEMP
        funcIsProgress: BOOL;
        current_sync06a_count2: DINT;
        current_sync06a_x: DINT;
        current_sync06b: BOOL;
        edge_sync06a: BOOL;
        eventEnabled: BOOL;
        dummyVar1: DINT;
        dummyVar2: DINT;
        dummyVar3: DINT;
    END_VAR

BEGIN
    funcIsProgress := isProgress;
    (*************************************************************
     * Try to perform controllable event "c_sync06b".
     *
     * - Automaton "sync06a" must always synchronize.
     *
     * - Automaton "sync06b" may synchronize.
     *************************************************************)
    eventEnabled := TRUE;
    (*******************************
     * Check each synchronizing automaton for having an edge with a true guard.
     *******************************)
    (***********
     * Test edge of automaton "sync06a" to synchronize for event "c_sync06b".
     * This automaton must have an edge with a true guard to allow the event.
     *
     * Edge being tested:
     * - Location:
     *   - 2nd edge in the location
     ***********)
    IF "DB".sync06a_count2 < 10 THEN
        edge_sync06a := 0;
    ELSE
        (* The automaton has no edge with a true guard. Skip to the next event. *)
        eventEnabled := FALSE;
    END_IF;
    (* All checks have been done. If variable "eventEnabled" still holds, event "c_sync06b" can occur. *)
    IF eventEnabled THEN
        funcIsProgress := TRUE;
        (* Make temporary copies of assigned variables to preserve the old values while assigning new values. *)
        current_sync06a_count2 := "DB".sync06a_count2;
        current_sync06a_x := "DB".sync06a_x;
        current_sync06b := "DB".sync06b;
        (*******************************
         * Perform the assignments of each synchronizing automaton.
         *******************************)
        (* Perform assignments of automaton "sync06a". *)
        IF edge_sync06a = 0 THEN
            (* Perform assignments of the 2nd edge of automaton "sync06a". *)
            (* Perform update of discrete variable "sync06a.count2". *)
            "DB".sync06a_count2 := current_sync06a_count2 + 1;
            (* Perform update of discrete variable "sync06a.x". *)
            "DB".sync06a_x := current_sync06a_x + current_sync06a_x + current_sync06a_count2;
        END_IF;
        (*******************************
         * Perform the assignments of each optionally synchronizing automaton.
         *******************************)
        (* Perform assignments of automaton "sync06b". *)
        IF current_sync06b = sync06b_loc1 THEN
            (***********
             * Location "loc1":
             *
             * Perform assignments of the 1st edge in location "sync06b.loc1".
             ***********)
            (* Perform update of current-location variable for automaton "sync06b". *)
            "DB".sync06b := sync06b_loc2;
        ELSIF current_sync06b = sync06b_loc2 THEN
            (***********
             * Location "loc2":
             *
             * Perform assignments of the 1st edge in location "sync06b.loc2".
             ***********)
            (* Perform update of current-location variable for automaton "sync06b". *)
            "DB".sync06b := sync06b_loc1;
        END_IF;
    END_IF;

    (* Return event execution progress. *)
    tryEvent_c_sync06b := funcIsProgress;
    RETURN;
END_FUNCTION
