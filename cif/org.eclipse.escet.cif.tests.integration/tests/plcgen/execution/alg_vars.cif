//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

enum E = L0, L1, L2;

automaton a:
  controllable evt;

  // Algebraic variables.
  alg bool abt = true;
  alg bool abf = false;

  alg int[-3..3] aineg2 = -1 - 1;
  alg int[-3..3] aineg1 = -1;
  alg int[-3..3] ai0 = 0;
  alg int[-3..3] aipos1 = 1;
  alg int[-3..3] aipos2 = 2;

  alg real arneg2 = -1.0 - 1.0;
  alg real arneg1 = -1.0;
  alg real ar0 = 0;
  alg real arpos1 = 1.0;
  alg real arpos2 = 2.0;

  alg E ae0 = L0;
  alg E ae1 = L1;
  alg E ae2 = L2;

  alg int[2..4] a4 = a2 * 2; // Value 4, third.
  alg int[1..2] a2 = a1 + 1; // Value 2, second.
  alg int[1..3] a3 = a4 - 1; // Value 3, fourth.
  alg int[0..1] a1 = 1;      // Value 1, first.

  alg bool aloc1 = loc1;
  alg bool aloc2 = loc2;
  alg real ast1 = vrpos1;
  alg real ast2 = vrpos1 + vrpos2 + ast1;

  // Variables.
  disc bool vbt, vbf;
  disc int vineg2, vineg1, vi0, vipos1, vipos2;
  disc real vrneg2, vrneg1, vr0, vrpos1 = 9.0, vrpos2 = 10.0;
  disc E ve0, ve1, ve2;
  disc int v1, v2, v3, v4;
  disc bool vloc1, vloc2;
  disc real vst1, vst2;

  location loc1:
    initial;
    edge evt do
          // Algebraic variables of various types, with various values.
          vbt     := abt,
          vbf     := abf,

          vineg2  := aineg2,
          vineg1  := aineg1,
          vi0     := ai0,
          vipos1  := aipos1,
          vipos2  := aipos2,

          vrneg2  := arneg2,
          vrneg1  := arneg1,
          vr0     := ar0,
          vrpos1  := arpos1,
          vrpos2  := arpos2,

          ve0     := ae0,
          ve1     := ae1,
          ve2     := ae2,

          // Algebraic variable order.
          v1 := a1,
          v2 := a2,
          v3 := a3,
          v4 := a4,

          // Algebraic variables over state.
          vloc1 := aloc1,
          vloc2 := aloc2,

          vst1 := ast1,
          vst2 := ast2

      goto loc2;

    location loc2;
end

// ##############################################
// Instructions.
// ##############################################
//
// Steps:
// - Generate code using the CIF PLC code generator (stable).
//   Make sure there are no errors during code generation.
//   Ignore any warnings produced during code generation.
// - Import the generated code into the PLC development environment.
//   Follow the documentation of the CIF PLC code generator (stable),
//   in case it provides detailed instructions for your target.
// - Make sure the code compiles. Ignore any warnings that are produced.
//   Follow the documentation of the CIF PLC code generator (stable),
//   in case it provides detailed instructions for your target.
// - Execute the code. Check the values of the variables below. All other
//   variables may be ignored.
//
// Notes on comparing values:
// - If you're testing an S7 target, you can automate the comparison by
//   using the '_results_compare_s7.tooldef' script.
// - For each variable, the expected value is given after the '=' symbol.
//   In case a '->' is given on a line, additionally accepted alternative
//   values are indicated, surrounded by single quotes (') and separated
//   by forward slashes (/).
// - If enumerations are eliminated, the value may be a number instead of
//   an enumeration literal. Depending on how the number is encoded, it
//   may be displayed differently in the PLC development environment.

// ##############################################
// Expected result.
// ##############################################
//
// a_v1 = 1
// a_v2 = 2
// a_v3 = 3
// a_v4 = 4
//
// a_vbf = false
// a_vbt = true
//
// a_ve0 = L0    -> '0' / '16#00'
// a_ve1 = L1    -> '1' / '16#01'
// a_ve2 = L2    -> '2' / '16#02'
//
// a_vi0 = 0
// a_vineg1 = -1
// a_vineg2 = -2
// a_vipos1 = 1
// a_vipos2 = 2
//
// a_vloc1 = true
// a_vloc2 = false
//
// a_vr0 = 0.0
// a_vrneg1 = -1.0
// a_vrneg2 = -2.0
// a_vrpos1 = 1.0
// a_vrpos2 = 2.0
//
// a_vst1 = 9.0
// a_vst2 = 28.0
