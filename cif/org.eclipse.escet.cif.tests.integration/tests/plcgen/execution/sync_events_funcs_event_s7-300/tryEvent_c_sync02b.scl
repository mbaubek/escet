FUNCTION tryEvent_c_sync02b: BOOL
{ S7_Optimized_Access := 'false' }
    VAR_INPUT
        isProgress: BOOL;
    END_VAR
    VAR_TEMP
        funcIsProgress: BOOL;
        current_sync02a: BYTE;
        current_sync02a_count: DINT;
        current_sync02b: BYTE;
        current_sync02b_count: DINT;
        edge_sync02a: BOOL;
        edge_sync02b: BOOL;
        eventEnabled: BOOL;
        dummyVar1: DINT;
        dummyVar2: DINT;
        dummyVar3: DINT;
    END_VAR

BEGIN
    funcIsProgress := isProgress;
    (*************************************************************
     * Try to perform controllable event "c_sync02b".
     *
     * - Automaton "sync02a" must always synchronize.
     * - Automaton "sync02b" must always synchronize.
     *************************************************************)
    eventEnabled := TRUE;
    (*******************************
     * Check each synchronizing automaton for having an edge with a true guard.
     *******************************)
    (***********
     * Test edge of automaton "sync02a" to synchronize for event "c_sync02b".
     * This automaton must have an edge with a true guard to allow the event.
     *
     * Edge being tested:
     * - Location "loc3":
     *   - 1st edge in the location
     ***********)
    IF "DB".sync02a = sync02a_loc3 THEN
        edge_sync02a := 0;
    ELSE
        (* The automaton has no edge with a true guard. Skip to the next event. *)
        eventEnabled := FALSE;
    END_IF;
    IF eventEnabled THEN
        (***********
         * Test edge of automaton "sync02b" to synchronize for event "c_sync02b".
         * This automaton must have an edge with a true guard to allow the event.
         *
         * Edge being tested:
         * - Location "loc3":
         *   - 1st edge in the location
         ***********)
        IF "DB".sync02b = sync02b_loc3 THEN
            edge_sync02b := 0;
        ELSE
            (* The automaton has no edge with a true guard. Skip to the next event. *)
            eventEnabled := FALSE;
        END_IF;
    END_IF;
    (* All checks have been done. If variable "eventEnabled" still holds, event "c_sync02b" can occur. *)
    IF eventEnabled THEN
        funcIsProgress := TRUE;
        (* Make temporary copies of assigned variables to preserve the old values while assigning new values. *)
        current_sync02a := "DB".sync02a;
        current_sync02a_count := "DB".sync02a_count;
        current_sync02b := "DB".sync02b;
        current_sync02b_count := "DB".sync02b_count;
        (*******************************
         * Perform the assignments of each synchronizing automaton.
         *******************************)
        (* Perform assignments of automaton "sync02a". *)
        IF edge_sync02a = 0 THEN
            (* Perform assignments of the 1st edge in location "sync02a.loc3". *)
            (* Perform update of discrete variable "sync02a.count". *)
            "DB".sync02a_count := current_sync02a_count + 1;
            (* Perform update of current-location variable for automaton "sync02a". *)
            "DB".sync02a := sync02a_loc1;
        END_IF;
        (* Perform assignments of automaton "sync02b". *)
        IF edge_sync02b = 0 THEN
            (* Perform assignments of the 1st edge in location "sync02b.loc3". *)
            (* Perform update of discrete variable "sync02b.count". *)
            "DB".sync02b_count := current_sync02b_count + 2;
            (* Perform update of current-location variable for automaton "sync02b". *)
            "DB".sync02b := sync02b_loc1;
        END_IF;
    END_IF;

    (* Return event execution progress. *)
    tryEvent_c_sync02b := funcIsProgress;
    RETURN;
END_FUNCTION
