Direct dependencies:
Each line has the form 'TYPE <- DEPENDENCY_1 DEPENDENCY_2 ...'.
To be able to use type 'TYPE', all 'DEPENDENCY_*' types must be available already.

    TupleStruct2 <-
    TupleStruct2_1 <-
    TupleStruct2_2 <- g_trrr
    TupleStruct2_3 <- TupleStruct4_1
    TupleStruct2_4 <- TupleStruct2_1
    TupleStruct2_5 <- TupleStruct2_1
    TupleStruct3 <-
    TupleStruct3_1 <- TupleStruct2
    TupleStruct4 <-
    TupleStruct4_1 <- TupleStruct2
    g_tbb1 <-
    g_tbi <-
    g_tcomplex <- TupleStruct2_2 TupleStruct3 TupleStruct4
    g_teeee <-
    g_trrr <-
    g_ttiitrr <- TupleStruct2 TupleStruct2_1

Groups of dependent derived types:
A derived type in a non-first group depends on at least one type from the previous group,
and possibly also groups before that.

    Group 1:
        TupleStruct2
        TupleStruct2_1
        TupleStruct3
        TupleStruct4
        g_tbb1
        g_tbi
        g_teeee
        g_trrr

    Group 2:
        TupleStruct2_2
        TupleStruct2_4
        TupleStruct2_5
        TupleStruct3_1
        TupleStruct4_1
        g_ttiitrr

    Group 3:
        TupleStruct2_3
        g_tcomplex
