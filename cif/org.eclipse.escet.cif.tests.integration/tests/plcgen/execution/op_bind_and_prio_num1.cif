//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

automaton a:
  controllable evt;

  // Fixed values.
  disc int four = 4;
  disc int seven = 7;
  disc real rfour = 4.0;
  disc real rseven = 7.0;
  disc int x3 = 3;
  disc int x4 = 4;
  disc int x19 = 19;
  disc int x23 = 23;

  // Variables.
  disc int neg_neg1, neg_neg2, neg_neg3, neg_neg4;
  disc real neg_neg5, neg_neg6, neg_neg7, neg_neg8;
  disc int neg_div1, neg_div2, neg_div3, neg_div4;
  disc int neg_mod1, neg_mod2, neg_mod3, neg_mod4;
  disc int neg_add1, neg_add2, neg_add3, neg_add4;
  disc real neg_add5, neg_add6, neg_add7, neg_add8;
  disc int neg_sub1, neg_sub2, neg_sub3, neg_sub4;
  disc real neg_sub5, neg_sub6, neg_sub7, neg_sub8;
  disc int neg_mul1, neg_mul2, neg_mul3, neg_mul4;
  disc real neg_mul5, neg_mul6, neg_mul7, neg_mul8;
  disc real neg_divide1, neg_divide2, neg_divide3, neg_divide4;
  disc real neg_divide5, neg_divide6, neg_divide7, neg_divide8;

  disc int div_neg1, div_neg2, div_neg3, div_neg4;
  disc int div_div01, div_div02, div_div03, div_div04, div_div05, div_div06, div_div07, div_div08;
  disc int div_div09, div_div10, div_div11, div_div12, div_div13, div_div14, div_div15, div_div16;
  disc int div_mod01, div_mod02, div_mod03, div_mod04, div_mod05, div_mod06, div_mod07, div_mod08;
  disc int div_mod09, div_mod10, div_mod11, div_mod12, div_mod13, div_mod14, div_mod15, div_mod16;
  disc int div_add01, div_add02, div_add03, div_add04, div_add05, div_add06, div_add07, div_add08;
  disc int div_add09, div_add10, div_add11, div_add12, div_add13, div_add14, div_add15, div_add16;
  disc int div_sub01, div_sub02, div_sub03, div_sub04, div_sub05, div_sub06, div_sub07, div_sub08;
  disc int div_sub09, div_sub10, div_sub11, div_sub12, div_sub13, div_sub14, div_sub15, div_sub16;
  disc int div_mul01, div_mul02, div_mul03, div_mul04, div_mul05, div_mul06, div_mul07, div_mul08;
  disc int div_mul09, div_mul10, div_mul11, div_mul12, div_mul13, div_mul14, div_mul15, div_mul16;

  disc int mod_neg1, mod_neg2, mod_neg3, mod_neg4;
  disc int mod_div01, mod_div02, mod_div03, mod_div04, mod_div05, mod_div06, mod_div07, mod_div08;
  disc int mod_div09, mod_div10, mod_div11, mod_div12, mod_div13, mod_div14, mod_div15, mod_div16;
  disc int mod_mod01, mod_mod02, mod_mod03, mod_mod04, mod_mod05, mod_mod06, mod_mod07, mod_mod08;
  disc int mod_mod09, mod_mod10, mod_mod11, mod_mod12, mod_mod13, mod_mod14, mod_mod15, mod_mod16;
  disc int mod_add01, mod_add02, mod_add03, mod_add04, mod_add05, mod_add06, mod_add07, mod_add08;
  disc int mod_add09, mod_add10, mod_add11, mod_add12, mod_add13, mod_add14, mod_add15, mod_add16;
  disc int mod_sub01, mod_sub02, mod_sub03, mod_sub04, mod_sub05, mod_sub06, mod_sub07, mod_sub08;
  disc int mod_sub09, mod_sub10, mod_sub11, mod_sub12, mod_sub13, mod_sub14, mod_sub15, mod_sub16;
  disc int mod_mul01, mod_mul02, mod_mul03, mod_mul04, mod_mul05, mod_mul06, mod_mul07, mod_mul08;
  disc int mod_mul09, mod_mul10, mod_mul11, mod_mul12, mod_mul13, mod_mul14, mod_mul15, mod_mul16;

  location loc1:
    initial;
    edge evt do
          // unary '-' / unary '-'
          neg_neg1 := --four,
          neg_neg2 := ---four,
          neg_neg3 := ----four,
          neg_neg4 := -----four,

          neg_neg5 := --rfour,
          neg_neg6 := ---rfour,
          neg_neg7 := ----rfour,
          neg_neg8 := -----rfour,

          // unary '-' / 'div'
          neg_div1 := -( seven div  four),
          neg_div2 := -( seven div -four),
          neg_div3 := -(-seven div  four),
          neg_div4 := -(-seven div -four),

          // unary '-' / 'mod'
          neg_mod1 := -( seven mod  four),
          neg_mod2 := -( seven mod -four),
          neg_mod3 := -(-seven mod  four),
          neg_mod4 := -(-seven mod -four),

          // unary '-' / binary '+'
          neg_add1 := -( seven +  four),
          neg_add2 := -( seven + -four),
          neg_add3 := -(-seven +  four),
          neg_add4 := -(-seven + -four),

          neg_add5 := -( rseven +  rfour),
          neg_add6 := -( rseven + -rfour),
          neg_add7 := -(-rseven +  rfour),
          neg_add8 := -(-rseven + -rfour),

          // unary '-' / binary '-'
          neg_sub1 := -( seven -  four),
          neg_sub2 := -( seven - -four),
          neg_sub3 := -(-seven -  four),
          neg_sub4 := -(-seven - -four),

          neg_sub5 := -( rseven -  rfour),
          neg_sub6 := -( rseven - -rfour),
          neg_sub7 := -(-rseven -  rfour),
          neg_sub8 := -(-rseven - -rfour),

          // unary '-' / '*'
          neg_mul1 := -( seven *  four),
          neg_mul2 := -( seven * -four),
          neg_mul3 := -(-seven *  four),
          neg_mul4 := -(-seven * -four),

          neg_mul5 := -( rseven *  rfour),
          neg_mul6 := -( rseven * -rfour),
          neg_mul7 := -(-rseven *  rfour),
          neg_mul8 := -(-rseven * -rfour),

          // unary '-' / '/'
          neg_divide1 := -( seven /  four),
          neg_divide2 := -( seven / -four),
          neg_divide3 := -(-seven /  four),
          neg_divide4 := -(-seven / -four),

          neg_divide5 := -( rseven /  rfour),
          neg_divide6 := -( rseven / -rfour),
          neg_divide7 := -(-rseven /  rfour),
          neg_divide8 := -(-rseven / -rfour),

          // 'div' / unary '-'
          div_neg1 := ( seven) div ( four),
          div_neg2 := ( seven) div (-four),
          div_neg3 := (-seven) div ( four),
          div_neg4 := (-seven) div (-four),

          // 'div' / 'div'
          div_div01 := ( x23 div  x3) div ( x19 div  x4),
          div_div02 := ( x23 div -x3) div ( x19 div  x4),
          div_div03 := (-x23 div  x3) div ( x19 div  x4),
          div_div04 := (-x23 div -x3) div ( x19 div  x4),

          div_div05 := ( x23 div  x3) div ( x19 div -x4),
          div_div06 := ( x23 div -x3) div ( x19 div -x4),
          div_div07 := (-x23 div  x3) div ( x19 div -x4),
          div_div08 := (-x23 div -x3) div ( x19 div -x4),

          div_div09 := ( x23 div  x3) div (-x19 div  x4),
          div_div10 := ( x23 div -x3) div (-x19 div  x4),
          div_div11 := (-x23 div  x3) div (-x19 div  x4),
          div_div12 := (-x23 div -x3) div (-x19 div  x4),

          div_div13 := ( x23 div  x3) div (-x19 div -x4),
          div_div14 := ( x23 div -x3) div (-x19 div -x4),
          div_div15 := (-x23 div  x3) div (-x19 div -x4),
          div_div16 := (-x23 div -x3) div (-x19 div -x4),

          // 'div' / 'mod'
          div_mod01 := ( x23 mod  x3) div ( x19 mod  x4),
          div_mod02 := ( x23 mod -x3) div ( x19 mod  x4),
          div_mod03 := (-x23 mod  x3) div ( x19 mod  x4),
          div_mod04 := (-x23 mod -x3) div ( x19 mod  x4),

          div_mod05 := ( x23 mod  x3) div ( x19 mod -x4),
          div_mod06 := ( x23 mod -x3) div ( x19 mod -x4),
          div_mod07 := (-x23 mod  x3) div ( x19 mod -x4),
          div_mod08 := (-x23 mod -x3) div ( x19 mod -x4),

          div_mod09 := ( x23 mod  x3) div (-x19 mod  x4),
          div_mod10 := ( x23 mod -x3) div (-x19 mod  x4),
          div_mod11 := (-x23 mod  x3) div (-x19 mod  x4),
          div_mod12 := (-x23 mod -x3) div (-x19 mod  x4),

          div_mod13 := ( x23 mod  x3) div (-x19 mod -x4),
          div_mod14 := ( x23 mod -x3) div (-x19 mod -x4),
          div_mod15 := (-x23 mod  x3) div (-x19 mod -x4),
          div_mod16 := (-x23 mod -x3) div (-x19 mod -x4),

          // 'div' / binary '+'
          div_add01 := ( x23 +  x3) div ( x19 +  x4),
          div_add02 := ( x23 + -x3) div ( x19 +  x4),
          div_add03 := (-x23 +  x3) div ( x19 +  x4),
          div_add04 := (-x23 + -x3) div ( x19 +  x4),

          div_add05 := ( x23 +  x3) div ( x19 + -x4),
          div_add06 := ( x23 + -x3) div ( x19 + -x4),
          div_add07 := (-x23 +  x3) div ( x19 + -x4),
          div_add08 := (-x23 + -x3) div ( x19 + -x4),

          div_add09 := ( x23 +  x3) div (-x19 +  x4),
          div_add10 := ( x23 + -x3) div (-x19 +  x4),
          div_add11 := (-x23 +  x3) div (-x19 +  x4),
          div_add12 := (-x23 + -x3) div (-x19 +  x4),

          div_add13 := ( x23 +  x3) div (-x19 + -x4),
          div_add14 := ( x23 + -x3) div (-x19 + -x4),
          div_add15 := (-x23 +  x3) div (-x19 + -x4),
          div_add16 := (-x23 + -x3) div (-x19 + -x4),

          // 'div' / binary '-'
          div_sub01 := ( x23 -  x3) div ( x19 -  x4),
          div_sub02 := ( x23 - -x3) div ( x19 -  x4),
          div_sub03 := (-x23 -  x3) div ( x19 -  x4),
          div_sub04 := (-x23 - -x3) div ( x19 -  x4),

          div_sub05 := ( x23 -  x3) div ( x19 - -x4),
          div_sub06 := ( x23 - -x3) div ( x19 - -x4),
          div_sub07 := (-x23 -  x3) div ( x19 - -x4),
          div_sub08 := (-x23 - -x3) div ( x19 - -x4),

          div_sub09 := ( x23 -  x3) div (-x19 -  x4),
          div_sub10 := ( x23 - -x3) div (-x19 -  x4),
          div_sub11 := (-x23 -  x3) div (-x19 -  x4),
          div_sub12 := (-x23 - -x3) div (-x19 -  x4),

          div_sub13 := ( x23 -  x3) div (-x19 - -x4),
          div_sub14 := ( x23 - -x3) div (-x19 - -x4),
          div_sub15 := (-x23 -  x3) div (-x19 - -x4),
          div_sub16 := (-x23 - -x3) div (-x19 - -x4),

          // 'div' / '*'
          div_mul01 := ( x23 *  x3) div ( x19 *  x4),
          div_mul02 := ( x23 * -x3) div ( x19 *  x4),
          div_mul03 := (-x23 *  x3) div ( x19 *  x4),
          div_mul04 := (-x23 * -x3) div ( x19 *  x4),

          div_mul05 := ( x23 *  x3) div ( x19 * -x4),
          div_mul06 := ( x23 * -x3) div ( x19 * -x4),
          div_mul07 := (-x23 *  x3) div ( x19 * -x4),
          div_mul08 := (-x23 * -x3) div ( x19 * -x4),

          div_mul09 := ( x23 *  x3) div (-x19 *  x4),
          div_mul10 := ( x23 * -x3) div (-x19 *  x4),
          div_mul11 := (-x23 *  x3) div (-x19 *  x4),
          div_mul12 := (-x23 * -x3) div (-x19 *  x4),

          div_mul13 := ( x23 *  x3) div (-x19 * -x4),
          div_mul14 := ( x23 * -x3) div (-x19 * -x4),
          div_mul15 := (-x23 *  x3) div (-x19 * -x4),
          div_mul16 := (-x23 * -x3) div (-x19 * -x4),

          // 'mod' / unary '-'
          mod_neg1 := ( seven) mod ( four),
          mod_neg2 := ( seven) mod (-four),
          mod_neg3 := (-seven) mod ( four),
          mod_neg4 := (-seven) mod (-four),

          // 'mod' / 'div'
          mod_div01 := ( x23 div  x3) mod ( x19 div  x4),
          mod_div02 := ( x23 div -x3) mod ( x19 div  x4),
          mod_div03 := (-x23 div  x3) mod ( x19 div  x4),
          mod_div04 := (-x23 div -x3) mod ( x19 div  x4),

          mod_div05 := ( x23 div  x3) mod ( x19 div -x4),
          mod_div06 := ( x23 div -x3) mod ( x19 div -x4),
          mod_div07 := (-x23 div  x3) mod ( x19 div -x4),
          mod_div08 := (-x23 div -x3) mod ( x19 div -x4),

          mod_div09 := ( x23 div  x3) mod (-x19 div  x4),
          mod_div10 := ( x23 div -x3) mod (-x19 div  x4),
          mod_div11 := (-x23 div  x3) mod (-x19 div  x4),
          mod_div12 := (-x23 div -x3) mod (-x19 div  x4),

          mod_div13 := ( x23 div  x3) mod (-x19 div -x4),
          mod_div14 := ( x23 div -x3) mod (-x19 div -x4),
          mod_div15 := (-x23 div  x3) mod (-x19 div -x4),
          mod_div16 := (-x23 div -x3) mod (-x19 div -x4),

          // 'mod' / 'mod'
          mod_mod01 := ( x23 mod  x3) mod ( x19 mod  x4),
          mod_mod02 := ( x23 mod -x3) mod ( x19 mod  x4),
          mod_mod03 := (-x23 mod  x3) mod ( x19 mod  x4),
          mod_mod04 := (-x23 mod -x3) mod ( x19 mod  x4),

          mod_mod05 := ( x23 mod  x3) mod ( x19 mod -x4),
          mod_mod06 := ( x23 mod -x3) mod ( x19 mod -x4),
          mod_mod07 := (-x23 mod  x3) mod ( x19 mod -x4),
          mod_mod08 := (-x23 mod -x3) mod ( x19 mod -x4),

          mod_mod09 := ( x23 mod  x3) mod (-x19 mod  x4),
          mod_mod10 := ( x23 mod -x3) mod (-x19 mod  x4),
          mod_mod11 := (-x23 mod  x3) mod (-x19 mod  x4),
          mod_mod12 := (-x23 mod -x3) mod (-x19 mod  x4),

          mod_mod13 := ( x23 mod  x3) mod (-x19 mod -x4),
          mod_mod14 := ( x23 mod -x3) mod (-x19 mod -x4),
          mod_mod15 := (-x23 mod  x3) mod (-x19 mod -x4),
          mod_mod16 := (-x23 mod -x3) mod (-x19 mod -x4),

          // 'mod' / binary '+'
          mod_add01 := ( x23 +  x3) mod ( x19 +  x4),
          mod_add02 := ( x23 + -x3) mod ( x19 +  x4),
          mod_add03 := (-x23 +  x3) mod ( x19 +  x4),
          mod_add04 := (-x23 + -x3) mod ( x19 +  x4),

          mod_add05 := ( x23 +  x3) mod ( x19 + -x4),
          mod_add06 := ( x23 + -x3) mod ( x19 + -x4),
          mod_add07 := (-x23 +  x3) mod ( x19 + -x4),
          mod_add08 := (-x23 + -x3) mod ( x19 + -x4),

          mod_add09 := ( x23 +  x3) mod (-x19 +  x4),
          mod_add10 := ( x23 + -x3) mod (-x19 +  x4),
          mod_add11 := (-x23 +  x3) mod (-x19 +  x4),
          mod_add12 := (-x23 + -x3) mod (-x19 +  x4),

          mod_add13 := ( x23 +  x3) mod (-x19 + -x4),
          mod_add14 := ( x23 + -x3) mod (-x19 + -x4),
          mod_add15 := (-x23 +  x3) mod (-x19 + -x4),
          mod_add16 := (-x23 + -x3) mod (-x19 + -x4),

          // 'mod' / binary '-'
          mod_sub01 := ( x23 -  x3) mod ( x19 -  x4),
          mod_sub02 := ( x23 - -x3) mod ( x19 -  x4),
          mod_sub03 := (-x23 -  x3) mod ( x19 -  x4),
          mod_sub04 := (-x23 - -x3) mod ( x19 -  x4),

          mod_sub05 := ( x23 -  x3) mod ( x19 - -x4),
          mod_sub06 := ( x23 - -x3) mod ( x19 - -x4),
          mod_sub07 := (-x23 -  x3) mod ( x19 - -x4),
          mod_sub08 := (-x23 - -x3) mod ( x19 - -x4),

          mod_sub09 := ( x23 -  x3) mod (-x19 -  x4),
          mod_sub10 := ( x23 - -x3) mod (-x19 -  x4),
          mod_sub11 := (-x23 -  x3) mod (-x19 -  x4),
          mod_sub12 := (-x23 - -x3) mod (-x19 -  x4),

          mod_sub13 := ( x23 -  x3) mod (-x19 - -x4),
          mod_sub14 := ( x23 - -x3) mod (-x19 - -x4),
          mod_sub15 := (-x23 -  x3) mod (-x19 - -x4),
          mod_sub16 := (-x23 - -x3) mod (-x19 - -x4),

          // 'mod' / '*'
          mod_mul01 := ( x23 *  x3) mod ( x19 *  x4),
          mod_mul02 := ( x23 * -x3) mod ( x19 *  x4),
          mod_mul03 := (-x23 *  x3) mod ( x19 *  x4),
          mod_mul04 := (-x23 * -x3) mod ( x19 *  x4),

          mod_mul05 := ( x23 *  x3) mod ( x19 * -x4),
          mod_mul06 := ( x23 * -x3) mod ( x19 * -x4),
          mod_mul07 := (-x23 *  x3) mod ( x19 * -x4),
          mod_mul08 := (-x23 * -x3) mod ( x19 * -x4),

          mod_mul09 := ( x23 *  x3) mod (-x19 *  x4),
          mod_mul10 := ( x23 * -x3) mod (-x19 *  x4),
          mod_mul11 := (-x23 *  x3) mod (-x19 *  x4),
          mod_mul12 := (-x23 * -x3) mod (-x19 *  x4),

          mod_mul13 := ( x23 *  x3) mod (-x19 * -x4),
          mod_mul14 := ( x23 * -x3) mod (-x19 * -x4),
          mod_mul15 := (-x23 *  x3) mod (-x19 * -x4),
          mod_mul16 := (-x23 * -x3) mod (-x19 * -x4)

      goto loc2;

  location loc2;
end

// ##############################################
// Instructions.
// ##############################################
//
// Steps:
// - Generate code using the CIF PLC code generator (stable).
//   Make sure there are no errors during code generation.
//   Ignore any warnings produced during code generation.
// - Import the generated code into the PLC development environment.
//   Follow the documentation of the CIF PLC code generator (stable),
//   in case it provides detailed instructions for your target.
// - Make sure the code compiles. Ignore any warnings that are produced.
//   Follow the documentation of the CIF PLC code generator (stable),
//   in case it provides detailed instructions for your target.
// - Execute the code. Check the values of the variables below. All other
//   variables may be ignored.
//
// Notes on comparing values:
// - If you're testing an S7 target, you can automate the comparison by
//   using the '_results_compare_s7.tooldef' script.
// - For each variable, the expected value is given after the '=' symbol.
// - Real numbers that are not whole numbers may have different precision and
//   rounding in the PLC. Still, their values should match, except for the
//   differences in precision (number of digits after the decimal point) and
//   rounding (last digit after the decimal point).

// ##############################################
// Expected result.
// ##############################################
//
// a_div_add01 = 1
// a_div_add02 = 0
// a_div_add03 = 0
// a_div_add04 = -1
// a_div_add05 = 1
// a_div_add06 = 1
// a_div_add07 = -1
// a_div_add08 = -1
// a_div_add09 = -1
// a_div_add10 = -1
// a_div_add11 = 1
// a_div_add12 = 1
// a_div_add13 = -1
// a_div_add14 = 0
// a_div_add15 = 0
// a_div_add16 = 1
// a_div_div01 = 1
// a_div_div02 = -1
// a_div_div03 = -1
// a_div_div04 = 1
// a_div_div05 = -1
// a_div_div06 = 1
// a_div_div07 = 1
// a_div_div08 = -1
// a_div_div09 = -1
// a_div_div10 = 1
// a_div_div11 = 1
// a_div_div12 = -1
// a_div_div13 = 1
// a_div_div14 = -1
// a_div_div15 = -1
// a_div_div16 = 1
// a_div_mod01 = 0
// a_div_mod02 = 0
// a_div_mod03 = 0
// a_div_mod04 = 0
// a_div_mod05 = 0
// a_div_mod06 = 0
// a_div_mod07 = 0
// a_div_mod08 = 0
// a_div_mod09 = 0
// a_div_mod10 = 0
// a_div_mod11 = 0
// a_div_mod12 = 0
// a_div_mod13 = 0
// a_div_mod14 = 0
// a_div_mod15 = 0
// a_div_mod16 = 0
// a_div_mul01 = 0
// a_div_mul02 = 0
// a_div_mul03 = 0
// a_div_mul04 = 0
// a_div_mul05 = 0
// a_div_mul06 = 0
// a_div_mul07 = 0
// a_div_mul08 = 0
// a_div_mul09 = 0
// a_div_mul10 = 0
// a_div_mul11 = 0
// a_div_mul12 = 0
// a_div_mul13 = 0
// a_div_mul14 = 0
// a_div_mul15 = 0
// a_div_mul16 = 0
// a_div_neg1 = 1
// a_div_neg2 = -1
// a_div_neg3 = -1
// a_div_neg4 = 1
// a_div_sub01 = 1
// a_div_sub02 = 1
// a_div_sub03 = -1
// a_div_sub04 = -1
// a_div_sub05 = 0
// a_div_sub06 = 1
// a_div_sub07 = -1
// a_div_sub08 = 0
// a_div_sub09 = 0
// a_div_sub10 = -1
// a_div_sub11 = 1
// a_div_sub12 = 0
// a_div_sub13 = -1
// a_div_sub14 = -1
// a_div_sub15 = 1
// a_div_sub16 = 1
//
// a_mod_add01 = 3
// a_mod_add02 = 20
// a_mod_add03 = -20
// a_mod_add04 = -3
// a_mod_add05 = 11
// a_mod_add06 = 5
// a_mod_add07 = -5
// a_mod_add08 = -11
// a_mod_add09 = 11
// a_mod_add10 = 5
// a_mod_add11 = -5
// a_mod_add12 = -11
// a_mod_add13 = 3
// a_mod_add14 = 20
// a_mod_add15 = -20
// a_mod_add16 = -3
// a_mod_div01 = 3
// a_mod_div02 = -3
// a_mod_div03 = -3
// a_mod_div04 = 3
// a_mod_div05 = 3
// a_mod_div06 = -3
// a_mod_div07 = -3
// a_mod_div08 = 3
// a_mod_div09 = 3
// a_mod_div10 = -3
// a_mod_div11 = -3
// a_mod_div12 = 3
// a_mod_div13 = 3
// a_mod_div14 = -3
// a_mod_div15 = -3
// a_mod_div16 = 3
// a_mod_mod01 = 2
// a_mod_mod02 = 2
// a_mod_mod03 = -2
// a_mod_mod04 = -2
// a_mod_mod05 = 2
// a_mod_mod06 = 2
// a_mod_mod07 = -2
// a_mod_mod08 = -2
// a_mod_mod09 = 2
// a_mod_mod10 = 2
// a_mod_mod11 = -2
// a_mod_mod12 = -2
// a_mod_mod13 = 2
// a_mod_mod14 = 2
// a_mod_mod15 = -2
// a_mod_mod16 = -2
// a_mod_mul01 = 69
// a_mod_mul02 = -69
// a_mod_mul03 = -69
// a_mod_mul04 = 69
// a_mod_mul05 = 69
// a_mod_mul06 = -69
// a_mod_mul07 = -69
// a_mod_mul08 = 69
// a_mod_mul09 = 69
// a_mod_mul10 = -69
// a_mod_mul11 = -69
// a_mod_mul12 = 69
// a_mod_mul13 = 69
// a_mod_mul14 = -69
// a_mod_mul15 = -69
// a_mod_mul16 = 69
// a_mod_neg1 = 3
// a_mod_neg2 = 3
// a_mod_neg3 = -3
// a_mod_neg4 = -3
// a_mod_sub01 = 5
// a_mod_sub02 = 11
// a_mod_sub03 = -11
// a_mod_sub04 = -5
// a_mod_sub05 = 20
// a_mod_sub06 = 3
// a_mod_sub07 = -3
// a_mod_sub08 = -20
// a_mod_sub09 = 20
// a_mod_sub10 = 3
// a_mod_sub11 = -3
// a_mod_sub12 = -20
// a_mod_sub13 = 5
// a_mod_sub14 = 11
// a_mod_sub15 = -11
// a_mod_sub16 = -5
//
// a_neg_add1 = -11
// a_neg_add2 = -3
// a_neg_add3 = 3
// a_neg_add4 = 11
// a_neg_add5 = -11.0
// a_neg_add6 = -3.0
// a_neg_add7 = 3.0
// a_neg_add8 = 11.0
// a_neg_div1 = -1
// a_neg_div2 = 1
// a_neg_div3 = 1
// a_neg_div4 = -1
// a_neg_divide1 = -1.75
// a_neg_divide2 = 1.75
// a_neg_divide3 = 1.75
// a_neg_divide4 = -1.75
// a_neg_divide5 = -1.75
// a_neg_divide6 = 1.75
// a_neg_divide7 = 1.75
// a_neg_divide8 = -1.75
// a_neg_mod1 = -3
// a_neg_mod2 = -3
// a_neg_mod3 = 3
// a_neg_mod4 = 3
// a_neg_mul1 = -28
// a_neg_mul2 = 28
// a_neg_mul3 = 28
// a_neg_mul4 = -28
// a_neg_mul5 = -28.0
// a_neg_mul6 = 28.0
// a_neg_mul7 = 28.0
// a_neg_mul8 = -28.0
// a_neg_neg1 = 4
// a_neg_neg2 = -4
// a_neg_neg3 = 4
// a_neg_neg4 = -4
// a_neg_neg5 = 4.0
// a_neg_neg6 = -4.0
// a_neg_neg7 = 4.0
// a_neg_neg8 = -4.0
// a_neg_sub1 = -3
// a_neg_sub2 = -11
// a_neg_sub3 = 11
// a_neg_sub4 = 3
// a_neg_sub5 = -3.0
// a_neg_sub6 = -11.0
// a_neg_sub7 = 11.0
// a_neg_sub8 = 3.0
