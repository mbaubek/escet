ORGANIZATION_BLOCK MAIN
{ S7_Optimized_Access := 'false' }
    VAR_TEMP
        loopCount: DINT;
        current_a_x: DINT;
        current_a_x_1: DINT;
        current_a_y: DINT;
        current_a_y_1: DINT;
        edge_a: BOOL;
        eventEnabled: BOOL;
        isProgress: BOOL;
    END_VAR

BEGIN
    (* Generated by CIF PLC code generator.
     *)

    (* An overview of the CIF model automata with their events and variables is written after the main code. *)



    (* --- Initialize state. --------------------------------------------------- *)
    IF "DB".firstRun THEN
        "DB".firstRun := FALSE;
        "DB".loopsExhausted := 0;

        (* Initialize the state variables. *)
        (* Initialize discrete variable "a.x". *)
        "DB".a_x := 0;
        (* Initialize discrete variable "a.y". *)
        "DB".a_y := 8;
    END_IF;

    (* --- Process uncontrollable events. -------------------------------------- *)
    isProgress := TRUE;
    (* Perform events until none can be done anymore. *)
    (* Track the number of iterations and abort if there are too many. *)
    loopCount := 0;
    WHILE isProgress AND loopCount < 4 DO
        loopCount := loopCount + 1;
        isProgress := FALSE;

        (*************************************************************
         * Try to perform uncontrollable event "a.u".
         *
         * - Automaton "a" must always synchronize.
         *************************************************************)
        eventEnabled := TRUE;
        (*******************************
         * Check each synchronizing automaton for having an edge with a true guard.
         *******************************)
        (***********
         * Test edge of automaton "a" to synchronize for event "a.u".
         * This automaton must have an edge with a true guard to allow the event.
         *
         * Edge being tested:
         * - Location:
         *   - 1st edge in the location
         ***********)
        IF "DB".a_x < 3 AND "DB".a_y > 0 THEN
            edge_a := 0;
        ELSE
            (* The automaton has no edge with a true guard. Skip to the next event. *)
            eventEnabled := FALSE;
        END_IF;
        (* All checks have been done. If variable "eventEnabled" still holds, event "a.u" can occur. *)
        IF eventEnabled THEN
            isProgress := TRUE;
            (* Make temporary copies of assigned variables to preserve the old values while assigning new values. *)
            current_a_x := "DB".a_x;
            current_a_y := "DB".a_y;
            (*******************************
             * Perform the assignments of each synchronizing automaton.
             *******************************)
            (* Perform assignments of automaton "a". *)
            IF edge_a = 0 THEN
                (* Perform assignments of the 1st edge of automaton "a". *)
                (* Perform update of discrete variable "a.x". *)
                "DB".a_x := current_a_x + 1;
                (* Perform update of discrete variable "a.y". *)
                "DB".a_y := current_a_y - 1;
            END_IF;
        END_IF;
    END_WHILE;
    (* Register the first 9999 aborted loops. *)
    IF loopCount >= 4 AND isProgress THEN
        "DB".loopsExhausted := MIN(IN1 := "DB".loopsExhausted + 1, IN2 := 9999);
    END_IF;

    (* --- Process controllable events. ---------------------------------------- *)
    isProgress := TRUE;
    (* Perform events until none can be done anymore. *)
    (* Track the number of iterations and abort if there are too many. *)
    loopCount := 0;
    WHILE isProgress AND loopCount < 4 DO
        loopCount := loopCount + 1;
        isProgress := FALSE;

        (*************************************************************
         * Try to perform controllable event "a.c".
         *
         * - Automaton "a" must always synchronize.
         *************************************************************)
        eventEnabled := TRUE;
        (*******************************
         * Check each synchronizing automaton for having an edge with a true guard.
         *******************************)
        (***********
         * Test edge of automaton "a" to synchronize for event "a.c".
         * This automaton must have an edge with a true guard to allow the event.
         *
         * Edge being tested:
         * - Location:
         *   - 2nd edge in the location
         ***********)
        IF "DB".a_x > 0 AND "DB".a_y > 0 THEN
            edge_a := 0;
        ELSE
            (* The automaton has no edge with a true guard. Skip to the next event. *)
            eventEnabled := FALSE;
        END_IF;
        (* All checks have been done. If variable "eventEnabled" still holds, event "a.c" can occur. *)
        IF eventEnabled THEN
            isProgress := TRUE;
            (* Make temporary copies of assigned variables to preserve the old values while assigning new values. *)
            current_a_x_1 := "DB".a_x;
            current_a_y_1 := "DB".a_y;
            (*******************************
             * Perform the assignments of each synchronizing automaton.
             *******************************)
            (* Perform assignments of automaton "a". *)
            IF edge_a = 0 THEN
                (* Perform assignments of the 2nd edge of automaton "a". *)
                (* Perform update of discrete variable "a.x". *)
                "DB".a_x := current_a_x_1 - 1;
                (* Perform update of discrete variable "a.y". *)
                "DB".a_y := current_a_y_1 - 1;
            END_IF;
        END_IF;
    END_WHILE;
    (* Register the first 9999 aborted loops. *)
    IF loopCount >= 4 AND isProgress THEN
        "DB".loopsExhausted := MIN(IN1 := "DB".loopsExhausted + 1, IN2 := 9999);
    END_IF;

    (*------------------------------------------------------
     * CIF model overview:
     *
     * ----
     * Automaton "a":
     *
     * - Discrete variable "a.x".
     * - Discrete variable "a.y".
     *
     * - PLC edge selection variable "edge_a".
     *   Unique edge number of the edge within the automaton that was selected to perform the considered event.
     *
     * - Uncontrollable event "a.u".
     *
     * - Controllable event "a.c".
     *------------------------------------------------------ *)
END_ORGANIZATION_BLOCK
