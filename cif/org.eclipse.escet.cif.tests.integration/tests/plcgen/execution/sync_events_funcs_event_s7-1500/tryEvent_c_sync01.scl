FUNCTION tryEvent_c_sync01: BOOL
{ S7_Optimized_Access := 'true' }
    VAR_INPUT
        isProgress: BOOL;
    END_VAR
    VAR_TEMP
        funcIsProgress: BOOL;
        current_sync01a: BOOL;
        current_sync01a_count: DINT;
        current_sync01b: BOOL;
        current_sync01b_count: DINT;
        edge_sync01a: BOOL;
        edge_sync01b: BOOL;
        eventEnabled: BOOL;
        dummyVar1: DINT;
        dummyVar2: DINT;
        dummyVar3: DINT;
    END_VAR

BEGIN
    funcIsProgress := isProgress;
    (*************************************************************
     * Try to perform controllable event "c_sync01".
     *
     * - Automaton "sync01a" must always synchronize.
     * - Automaton "sync01b" must always synchronize.
     *************************************************************)
    eventEnabled := TRUE;
    (*******************************
     * Check each synchronizing automaton for having an edge with a true guard.
     *******************************)
    (***********
     * Test edges of automaton "sync01a" to synchronize for event "c_sync01".
     * This automaton must have an edge with a true guard to allow the event.
     *
     * Edges being tested:
     * - Location "loc1":
     *   - 1st edge in the location
     * - Location "loc2":
     *   - 1st edge in the location
     ***********)
    IF "DB".sync01a = sync01a_loc1 AND "DB".sync01a_count < 10 THEN
        edge_sync01a := 0;
    ELSIF "DB".sync01a = sync01a_loc2 THEN
        edge_sync01a := 1;
    ELSE
        (* The automaton has no edge with a true guard. Skip to the next event. *)
        eventEnabled := FALSE;
    END_IF;
    IF eventEnabled THEN
        (***********
         * Test edges of automaton "sync01b" to synchronize for event "c_sync01".
         * This automaton must have an edge with a true guard to allow the event.
         *
         * Edges being tested:
         * - Location "loc1":
         *   - 1st edge in the location
         * - Location "loc2":
         *   - 1st edge in the location
         ***********)
        IF "DB".sync01b = sync01b_loc1 THEN
            edge_sync01b := 0;
        ELSIF "DB".sync01b = sync01b_loc2 THEN
            edge_sync01b := 1;
        ELSE
            (* The automaton has no edge with a true guard. Skip to the next event. *)
            eventEnabled := FALSE;
        END_IF;
    END_IF;
    (* All checks have been done. If variable "eventEnabled" still holds, event "c_sync01" can occur. *)
    IF eventEnabled THEN
        funcIsProgress := TRUE;
        (* Make temporary copies of assigned variables to preserve the old values while assigning new values. *)
        current_sync01a := "DB".sync01a;
        current_sync01a_count := "DB".sync01a_count;
        current_sync01b := "DB".sync01b;
        current_sync01b_count := "DB".sync01b_count;
        (*******************************
         * Perform the assignments of each synchronizing automaton.
         *******************************)
        (* Perform assignments of automaton "sync01a". *)
        IF edge_sync01a = 0 THEN
            (* Perform assignments of the 1st edge in location "sync01a.loc1". *)
            (* Perform update of current-location variable for automaton "sync01a". *)
            "DB".sync01a := sync01a_loc2;
        ELSIF edge_sync01a = 1 THEN
            (* Perform assignments of the 1st edge in location "sync01a.loc2". *)
            (* Perform update of discrete variable "sync01a.count". *)
            "DB".sync01a_count := current_sync01a_count + 1;
            (* Perform update of current-location variable for automaton "sync01a". *)
            "DB".sync01a := sync01a_loc1;
        END_IF;
        (* Perform assignments of automaton "sync01b". *)
        IF edge_sync01b = 0 THEN
            (* Perform assignments of the 1st edge in location "sync01b.loc1". *)
            (* Perform update of current-location variable for automaton "sync01b". *)
            "DB".sync01b := sync01b_loc2;
        ELSIF edge_sync01b = 1 THEN
            (* Perform assignments of the 1st edge in location "sync01b.loc2". *)
            (* Perform update of discrete variable "sync01b.count". *)
            "DB".sync01b_count := current_sync01b_count + 2;
            (* Perform update of current-location variable for automaton "sync01b". *)
            "DB".sync01b := sync01b_loc1;
        END_IF;
    END_IF;

    (* Return event execution progress. *)
    tryEvent_c_sync01 := funcIsProgress;
    RETURN;
END_FUNCTION
