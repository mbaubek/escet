//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

// Void channel, single sender, single receiver.

uncontrollable void chan1;

automaton sender1:
  location loc1:
    initial;
    marked;
    edge chan1! goto loc2;

  location loc2;
end

automaton receiver1:
  location loc1:
    initial;
    marked;
    edge chan1? goto loc2;

  location loc2;
end

// Boolean channel, single sender, multiple receivers.

uncontrollable bool chan2;

automaton sender2:
  disc bool x;

  location:
    initial;
    marked;
    edge chan2!x do x := not x;
end

automaton receiver2a:
  disc bool v;

  location loc1:
    initial;
    marked;
    edge chan2? do v := ? goto loc2;

  location loc2;
end

automaton receiver2b:
  disc bool v;

  location loc1:
    initial;
    marked;
    edge chan2? do v := ? goto loc2;

  location loc2;
end

automaton receiver2c:
  disc bool v;

  location loc1:
    initial;
    marked;
    edge chan2? do v := ? goto loc2;

  location loc2;
end

// Integer channel, multiple senders, multiple receivers.

uncontrollable int chan3;

automaton sender3a:
  location loc1:
    initial;
    marked;
    edge chan3!1 goto loc2;

  location loc2;
end

automaton sender3b:
  location loc1:
    initial;
    marked;
    edge chan3!2 goto loc2;

  location loc2;
end

automaton receiver3a:
  disc int v;

  location loc1:
    initial;
    marked;
    edge chan3? do v := ? goto loc2;

  location loc2;
end

automaton receiver3b:
  disc int v;

  location loc1:
    initial;
    marked;
    edge chan3? do v := ? goto loc2;

  location loc2;
end

automaton receiver3c:
  disc int v;

  location loc1:
    initial;
    marked;
    edge chan3? do v := ? goto loc2;

  location loc2;
end

// Real channel, senders/receivers/syncers, and guards.

uncontrollable real chan4;

automaton sender4a:
  disc real x = 10;

  location:
    initial;
    marked;
    edge chan4!x when syncer4.loc1 do x := x + 10;
end

automaton sender4b:
  disc real x = 100;

  location:
    initial;
    marked;
    edge chan4!x when syncer4.loc2 do x := x + 100;
end

automaton syncer4:
  location loc1:
    initial;
    edge chan4 goto loc2;

  location loc2:
    edge chan4 goto loc1;
end

automaton receiver4a:
  disc real v;

  location:
    initial;
    marked;
    edge chan4? when v < 30 do v := v + ?;
end

automaton receiver4b:
  disc real v;

  location:
    initial;
    marked;
    edge chan4? when v < 500 do v := v + ?;
end

automaton receiver4c:
  disc real v;

  location:
    initial;
    marked;
    edge chan4? when v < 2000 do v := v + ?;
end

// Enum channel, send/receive from different locations.

enum E = L0, L1, L2;

uncontrollable E chan5;

automaton sender5:
  location loc1:
    initial;
    marked;
    edge chan5!L0 goto loc2;

  location loc2:
    edge chan5!L1 goto loc3;

  location loc3:
    edge chan5!L2 goto loc4;

  location loc4;
end

automaton receiver5:
  uncontrollable e;
  disc E v;

  location loc1a:
    initial;
    marked;
    edge chan5? do v := ? goto loc1b;

  location loc1b:
    edge e when v = L0 goto loc2a;

  location loc2a:
    edge chan5? do v := ? goto loc2b;

  location loc2b:
    edge e when v = L1 goto loc3a;

  location loc3a:
    edge chan5? do v := ? goto loc3b;

  location loc3b:
    edge e when v = L2 goto loc4;

  location loc4;
end

// Integer channel, senders/receivers/syncers/monitors.

uncontrollable int chan6;

automaton sender6:
  disc int x = 0;

  location:
    initial;
    marked;
    edge chan6!x when x < 10 do x := x + 1;
end

automaton syncer6:
  disc int c1;
  disc int c2;

  location loc1:
    initial;
    edge chan6 do c1 := c1 + 1 goto loc2;

  location loc2:
    edge chan6 do c2 := c2 + 1 goto loc1;
end

automaton monitor6:
  monitor chan6;
  disc int c;

  location:
    initial;
    edge chan6 when sender6.x mod 2 = 0 do c := c + 1;
end

automaton receiver6a:
  disc int v;

  location:
    initial;
    marked;
    edge chan6? when sender6.x mod 3 = 0 do v := v + ?;
    edge chan6? when sender6.x mod 3 = 0 do v := v + ? + 999;
end

automaton receiver6b:
  disc int v;

  location:
    initial;
    marked;
    edge chan6? when sender6.x mod 3 = 1 do v := v + ?;
    edge chan6? when sender6.x mod 3 = 1 do v := v + ? + 999;
end

automaton receiver6c:
  disc int v;

  location:
    initial;
    marked;
    edge chan6? when sender6.x mod 3 = 2 do v := v + ?;
    edge chan6? when sender6.x mod 3 = 2 do v := v + ? + 999;
end

// Integer channel, senders/receivers/syncers/monitors, disabled.

uncontrollable int chan7;

automaton sender7:
  disc int x = 0;

  location:
    initial;
    marked;
    edge chan7!x do x := x + 1;
end

automaton syncer7:
  disc int x = 0;

  location:
    initial;
    edge chan7 when false do x := x + 1;
end

automaton monitor7:
  disc int x = 0;

  location:
    initial;
    edge chan7 when false do x := x + 1;
end

automaton receiver7:
  disc int x = 0;

  location:
    initial;
    marked;
    edge chan7? do x := x + 1;
end

// Integer channel, senders/receivers/syncers/monitors, multiple edges in same location.

uncontrollable int chan8;

automaton sender8:
  disc int x = 0;
  disc int y;

  location:
    initial;
    marked;
    edge chan8!x when x mod 2 = 0, x < 10 do x := x + 1, y := y + 1;
    edge chan8!x when x mod 2 = 1, x < 10 do x := x + 1, y := y + 2;
end

automaton syncer8:
  disc int x = 0;
  disc int y;

  location:
    initial;
    edge chan8 when x mod 2 = 1 do x := x + 1, y := y + 1;
    edge chan8 when x mod 2 = 0 do x := x + 1, y := y + 2;
end

automaton monitor8:
  monitor chan8;
  disc int x = 0;
  disc int y;

  location:
    initial;
    edge chan8 when x mod 2 = 0 do x := x + 1, y := y + 1;
end

automaton receiver8:
  disc int x = 0;
  disc int y;

  location:
    initial;
    marked;
    edge chan8? when x mod 2 = 1 do x := x + 1, y := y + 2;
    edge chan8? when x mod 2 = 0 do x := x + 1, y := y + 1;
end

// ##############################################
// Instructions.
// ##############################################
//
// Steps:
// - Generate code using the CIF PLC code generator (stable).
//   Make sure there are no errors during code generation.
//   Ignore any warnings produced during code generation.
// - Import the generated code into the PLC development environment.
//   Follow the documentation of the CIF PLC code generator (stable),
//   in case it provides detailed instructions for your target.
// - Make sure the code compiles. Ignore any warnings that are produced.
//   Follow the documentation of the CIF PLC code generator (stable),
//   in case it provides detailed instructions for your target.
// - Execute the code. Check the values of the variables below. All other
//   variables may be ignored.
//
// Notes on comparing values:
// - If you're testing an S7 target, you can automate the comparison by
//   using the '_results_compare_s7.tooldef' script.
// - For each variable, the expected value is given after the '=' symbol.
//   In case a '->' is given on a line, additionally accepted alternative
//   values are indicated, surrounded by single quotes (') and separated
//   by forward slashes (/).
// - If enumerations are eliminated, the value may be a number instead of
//   an enumeration literal. Depending on how the number is encoded, it
//   may be displayed differently in the PLC development environment.

// ##############################################
// Expected result.
// ##############################################
//
// monitor6_c = 5
// monitor7_x = 0
// monitor8_x = 1
// monitor8_y = 1
//
// receiver1 = receiver1_loc2    -> 'sender1_loc2' / '1' / 'true'
// receiver2a = receiver2a_loc2    -> 'sender1_loc2' / '1' / 'true'
// receiver2a_v = false
// receiver2b = receiver2b_loc2    -> 'sender1_loc2' / '1' / 'true'
// receiver2b_v = true
// receiver2c = receiver2c_loc2    -> 'sender1_loc2' / '1' / 'true'
// receiver2c_v = false
// receiver3a = receiver3a_loc2    -> 'sender1_loc2' / '1' / 'true'
// receiver3a_v = 1
// receiver3b = receiver3b_loc2    -> 'sender1_loc2' / '1' / 'true'
// receiver3b_v = 2
// receiver3c = receiver3c_loc1    -> 'sender1_loc1' / '0' / 'false'
// receiver3c_v = 0
// receiver4a_v = 110.0
// receiver4b_v = 550.0
// receiver4c_v = 2420.0
// receiver5 = receiver5_loc4    -> '6' / '16#06'
// receiver5_v = L2    -> '2' / '16#02'
// receiver6a_v = 18
// receiver6b_v = 12
// receiver6c_v = 15
// receiver7_x = 0
// receiver8_x = 10
// receiver8_y = 15
//
// sender1 = sender1_loc2    -> '1' / 'true'
// sender2_x = true
// sender3a = sender3a_loc2    -> 'sender1_loc2' / '1' / 'true'
// sender3b = sender3b_loc2    -> 'sender1_loc2' / '1' / 'true'
// sender4a_x = 80.0
// sender4b_x = 800.0
// sender5 = sender5_loc4    -> '3' / '16#03'
// sender6_x = 10
// sender7_x = 0
// sender8_x = 10
// sender8_y = 15
//
// syncer4 = syncer4_loc1    -> 'sender1_loc1' / '0' / 'false'
// syncer6 = syncer6_loc1    -> 'sender1_loc1' / '0' / 'false'
// syncer6_c1 = 5
// syncer6_c2 = 5
// syncer7_x = 0
// syncer8_x = 10
// syncer8_y = 15
