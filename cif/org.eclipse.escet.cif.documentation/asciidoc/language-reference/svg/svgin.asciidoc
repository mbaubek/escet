//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

include::../_part_attributes.asciidoc[]

[[lang-ref-svg-svgin]]
== CIF/SVG input mappings

indexterm:[SVG input mapping]
indexterm:[SVG,input mapping]
indexterm:[SVG declaration,SVG input mapping]
indexterm:[I/O declaration,SVG input mapping]
indexterm:[see: svgin,SVG input mapping]
CIF/SVG input mappings allow for interaction with <<lang-tut-svg-chapter-visualization-svg,SVG>> images.
The input mappings allow for interaction, by clicking on elements of an SVG image, which then affect the behavior of the specification.
See the CIF tutorial lesson on <<lang-tut-svg-chapter-interaction,SVG interaction>> to better understand this interactivity.

Concretely, a CIF/SVG input mapping is a <<lang-ref-svg-cifsvg,CIF/SVG declaration>> that couples an <<lang-tut-svg-chapter-visualization-svg-id,id>> of an SVG element to an event or one or more updates in the CIF specification.

[[lang-ref-svg-svgin-interactive-element]]
=== Interactive elements

Every input mapping must <<lang-ref-svg-svgout-id,specify the id>> of an SVG element, and an SVG element with that id must exist in the SVG image.
Specifying an input mapping for an SVG element turns that SVG element into an _interactive SVG element_.
The SVG input mapping then determines what happens when such an interactive element is clicked.
There are two types of interactions: enabling an event and executing some updates.

Note that it is also possible to make a group of SVG elements (an `svg:g` element) an interactive element.
By doing so, all the elements that are part of the group together become one single interactive element.

One practical example where this is very useful, is for buttons with a text label.
In SVG, this requires the use of a rectangle and a text label.
By grouping the rectangle and text label together together in an SVG group, and adding an input mapping to that group, the rectangle and label can both be clicked.
Clicking on either of them results in the corresponding input mapping being applied.

It is strongly recommended not to make a child (or descendant in general) of an interactive SVG element interactive as well.

indexterm:[SVG input mapping,single event input mapping]

[[lang-ref-svg-svgin-event-single]]
=== Single event input mapping

The simplest form of an SVG input mapping maps to a single event.
It has the following syntax:

[source, cif]
----
svgin id <id> event <event>;
svgin id <id> event <event> file <file>;
----

The `svgin` keyword is followed by a specification of an SVG element <<lang-tut-svg-chapter-visualization-svg-id,id>>.

The `id` is followed by a reference to an event in the specification.
This event is disabled until the interactive element, the one with the given id, is clicked.
Clicking the interactive element enables the event such that at most one transition may be taken for it.
After taking the transition, the event is again disabled until the element is clicked once more.
If after clicking the element the event is not enabled in the current state of the model, for instance because an automaton in the model can't synchronize on it, the click is ignored.

It is also allowed to declare an optional local <<lang-ref-svg-svgfile,SVG file declaration>>, that only applies to that specific input declaration.

indexterm:[SVG input mapping,if event input mapping]

[[lang-ref-svg-svgin-event-if]]
=== `if` event input mapping

An _if event input mapping_ is similar to a single event input mapping, but allows to map to different events depending on the state of the model.
Its syntax is as follows:

[source, cif]
----
svgin id <id> event if <expr>: <event> else <event> end;
svgin id <id> event if <expr>: <event> elif <expr>: <event> ... else <event> end;
svgin id <id> event if <expr>: <event> elif <expr>: <event> ... end;

svgin id <id> event if <expr>: <event> else <event> end file <file>;
svgin id <id> event if <expr>: <event> elif <expr>: <event> ... else <event> end file <file>;
svgin id <id> event if <expr>: <event> elif <expr>: <event> ... end file <file>;
----

An `if` event input mapping essentially has an <<lang-tut-values-chapter-if-expression,`if` expression>> that results in an event rather than a value.
The conditions (`<expr>`) are guard expressions that evaluate to a boolean value.
The guard expressions may be arbitrary expressions, similar to the use of expressions for values in output mappings.

If a condition holds, the corresponding event is chosen by the mapping.
The entries are processed in the order they are specified: if the first guard (of the `if`) holds, the first event is chosen, otherwise if the second guard (of an `elif`) holds, the second event is chosen, etc.
The `if` part is mandatory for an `if` event input mapping, but the `elif` parts are optional.
If it has `elif` parts, it may have as many of them as necessary.

The last part of the `if` is allowed to be an `else`, to indicate that if none of the previous guards hold, the event of the `else` is to be chosen.
Using an `else` entry is optional, but if used, there may only be one such entry, and it must be the last entry of the input mapping.

An `if` input mapping must be complete.
If such a mapping has an `else`, it is always complete.
If it doesn't have an `else`, at least one of its `if` or `elif` guards must hold.
If none of the guards hold, the mapping results in a runtime error.

The event that is chosen by the mapping becomes enabled, and at most one transitions may be taken for it, like for single event input mappings.

It is also allowed to declare an optional local <<lang-ref-svg-svgfile,SVG file declaration>>, that only applies to that specific input declaration.

indexterm:[SVG input mapping,update input mapping]

[[lang-ref-svg-svgin-update]]
=== Update input mapping

With SVG input mappings it is also possible to update the values of <<lang-tut-reuse2-chapter-input-variables,input variables>>:

[source, cif]
----
svgin id <id> do <updates>;
svgin id <id> do <updates> file <file>;
----

Compared to a single event input mapping or `if` event input mapping, no event is specified.
Instead, updates can be specified, like on edges.
Clicking on the interactive SVG element then leads to the updates being executed, rather than an event of the model being enabled.

It is also allowed to declare an optional local <<lang-ref-svg-svgfile,SVG file declaration>>, that only applies to that specific input declaration.

Be aware that the initial value of an input variable cannot be set in the specification.
Instead, CIF tools like the <<tools-cifsim-chapter-index,CIF simulator>> allow to <<tools-cifsim-chapter-init,set the initial value via an option>>.

indexterm:[SVG input mapping,uniqueness]

=== Uniqueness

Similar to output mappings, all input mappings must be unique, per SVG image.
That is, no two input mappings for the same SVG image may use the same SVG element id.

Note that it is allowed to have an input mapping and an output mapping (or even multiple output mappings) for the same SVG element.
It is also allowed for multiple SVG input mappings to enable the same event, or update the same input variables.
