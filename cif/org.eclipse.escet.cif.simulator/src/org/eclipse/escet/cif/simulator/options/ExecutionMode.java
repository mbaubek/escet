//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.simulator.options;

/** Execution mode. */
public enum ExecutionMode {
    /**
     * Enable execution mode. Transitions are computed in a way that adheres to the execution scheme defined by the CIF
     * controller properties checker.
     */
    ON,

    /**
     * Disable execution mode. The computation of transitions is not limited by the execution scheme defined by the CIF
     * controller properties checker.
     */
    OFF;
}
