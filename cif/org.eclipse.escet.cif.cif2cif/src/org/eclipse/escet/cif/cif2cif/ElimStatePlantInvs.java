//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.cif2cif;

import java.util.EnumSet;

import org.eclipse.escet.cif.metamodel.cif.SupKind;

/**
 * In-place transformation that replaces state plant invariants by extra initialization predicates and guards on edges.
 *
 * <p>
 * This transformation has several preconditions. See {@link ElimStateInvs}.
 * </p>
 */
public class ElimStatePlantInvs extends ElimStateInvs {
    /** Constructor for the {@link ElimStatePlantInvs} class. */
    public ElimStatePlantInvs() {
        super(EnumSet.of(SupKind.PLANT));
    }
}
