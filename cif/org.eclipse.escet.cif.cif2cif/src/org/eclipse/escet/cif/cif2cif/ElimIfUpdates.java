//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.cif2cif;

import java.util.List;

import org.eclipse.escet.cif.common.CifScopeUtils;
import org.eclipse.escet.cif.common.CifUpdateUtils;
import org.eclipse.escet.cif.common.CifUpdateUtils.UnsupportedUpdateException;
import org.eclipse.escet.cif.metamodel.cif.Specification;
import org.eclipse.escet.cif.metamodel.cif.automata.Assignment;
import org.eclipse.escet.cif.metamodel.cif.automata.Edge;
import org.eclipse.escet.cif.metamodel.cif.cifsvg.SvgIn;
import org.eclipse.escet.cif.metamodel.java.CifWalker;

/**
 * In-place transformation that eliminates 'if' updates on edges and in SVG input mappings.
 *
 * <p>
 * For instance:
 *
 * <pre>
 * if g: if g2: x := 5 else y := 6 end end,
 * z := 7
 * </pre>
 *
 * This would become:
 *
 * <pre>
 * x := if g: if g2: 5 else x end else x end,
 * y := if g: if g2: y else 6 end else y end,
 * z := 7
 * </pre>
 * </p>
 *
 * <p>
 * Precondition: Specifications with component definitions/instantiations are currently not supported.
 * </p>
 *
 * <p>
 * Precondition: Multi-assignments, as well as partial variable assignments (projected addressables), are currently not
 * supported by this transformation. That is, only variable references are supported as addressables.
 * </p>
 *
 * <p>
 * Furthermore, the 'if' structure may be replicated for multiple variables, which may result in a blow-up of the
 * specification size.
 * </p>
 *
 * <p>
 * Performance: As a result of this transformation, variables may be assigned their old value, resulting in superfluous
 * assignments.
 * </p>
 */
public class ElimIfUpdates extends CifWalker implements CifToCifTransformation {
    /**
     * The message to use for the unsupported exception in case muti-assignments or partial variable assignments are
     * encountered.
     */
    private static final String UNSUPPORTED_MESSAGE = "Eliminating 'if' updates, from a CIF specification with " +
            "multi-assignments and/or partial variable assignments (projected addressables), is currently not " +
            "supported.";

    @Override
    public void transform(Specification spec) {
        // Check no component definition/instantiation precondition.
        if (CifScopeUtils.hasCompDefInst(spec)) {
            String msg = "Eliminating 'if' updates, from a CIF specification with component definitions is currently " +
                    "not supported.";
            throw new CifToCifPreconditionException(msg);
        }

        // Perform actual transformation.
        walkSpecification(spec);
    }

    @Override
    protected void preprocessEdge(Edge edge) {
        // Optimization for edges without updates.
        if (edge.getUpdates().isEmpty()) {
            return;
        }

        // Transform updates to assignments.
        List<Assignment> assignments;
        try {
            assignments = CifUpdateUtils.updatesToAssignmentsPerVar(edge.getUpdates());
        } catch (UnsupportedUpdateException e) {
            throw new CifToCifPreconditionException(UNSUPPORTED_MESSAGE);
        }

        // Replace all updates.
        edge.getUpdates().clear();
        edge.getUpdates().addAll(assignments);
    }

    @Override
    protected void preprocessSvgIn(SvgIn svgIn) {
        // Optimization for SVG input mappings without updates.
        if (svgIn.getUpdates().isEmpty()) {
            return;
        }

        // Transform updates to assignments.
        List<Assignment> assignments;
        try {
            assignments = CifUpdateUtils.updatesToAssignmentsPerVar(svgIn.getUpdates());
        } catch (UnsupportedUpdateException e) {
            throw new CifToCifPreconditionException(UNSUPPORTED_MESSAGE);
        }

        // Replace all updates.
        svgIn.getUpdates().clear();
        svgIn.getUpdates().addAll(assignments);
    }
}
