//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.checkers.checks;

import org.eclipse.escet.cif.checkers.CifCheck;
import org.eclipse.escet.cif.checkers.CifCheckViolations;
import org.eclipse.escet.cif.metamodel.cif.InputParameter;
import org.eclipse.escet.cif.metamodel.cif.declarations.InputVariable;

/** CIF check that does not allow input variables and input parameters. */
public class VarNoInputCheck extends CifCheck {
    @Override
    protected void preprocessInputVariable(InputVariable var, CifCheckViolations violations) {
        if (var.eContainer() instanceof InputParameter) {
            violations.add(var, "Parameter is an input parameter");
        } else {
            violations.add(var, "Variable is an input variable");
        }
    }
}
