//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2022, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.checkers.checks;

import org.eclipse.escet.cif.checkers.CifCheckNoCompDefInst;
import org.eclipse.escet.cif.checkers.CifCheckViolations;
import org.eclipse.escet.cif.common.CifMath;
import org.eclipse.escet.cif.common.CifValueUtils;
import org.eclipse.escet.cif.common.CifValueUtils.Count;
import org.eclipse.escet.cif.metamodel.cif.Specification;
import org.eclipse.escet.cif.metamodel.cif.automata.Automaton;
import org.eclipse.escet.cif.metamodel.cif.declarations.DiscVariable;
import org.eclipse.escet.cif.metamodel.cif.declarations.InputVariable;
import org.eclipse.escet.common.java.Assert;

/**
 * CIF check that disallows specifications with more than {@link Integer#MAX_VALUE} possible initial states. It
 * considers discrete variables, input variables and automata.
 */
public class SpecNoTooManyPossibleInitialStatesCheck extends CifCheckNoCompDefInst {
    /** The current best approximation of the number of possible initial states. */
    private Count count = new CifValueUtils.Count(1, true); // If no variables/automata, then we have one initial state.

    @Override
    protected void preprocessDiscVariable(DiscVariable discVar, CifCheckViolations violations) {
        Count varCount = CifValueUtils.getPossibleInitialValuesCount(discVar);
        Assert.check(varCount.value() > 0);
        count = count.combine(varCount);
    }

    @Override
    protected void preprocessInputVariable(InputVariable inputVar, CifCheckViolations violations) {
        Count varCount = CifValueUtils.getPossibleInitialValuesCount(inputVar);
        Assert.check(varCount.value() > 0);
        count = count.combine(varCount);
    }

    @Override
    protected void preprocessAutomaton(Automaton aut, CifCheckViolations violations) {
        Count autCount = CifValueUtils.getPossibleInitialLocationsCount(aut);
        count = count.combine(autCount);
    }

    @Override
    protected void postprocessSpecification(Specification spec, CifCheckViolations violations) {
        double value = count.value();
        if (value > Integer.MAX_VALUE) {
            String countTxt;
            if (Double.isInfinite(value)) {
                violations.add(spec, "The specification has practically infinitely many possible initial states");
            } else {
                countTxt = CifMath.realToStr(value);
                if (!count.isPrecise()) {
                    countTxt = "approximately " + countTxt;
                }
                violations.add(spec,
                        "The specification has %s possible initial states, more than the maximum of 2,147,483,647",
                        countTxt);
            }
        }
    }
}
