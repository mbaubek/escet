//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.plcgen.options;

import static org.eclipse.escet.common.java.Strings.fmt;

/**
 * The available forms of the code for an event transition.
 *
 * <p>
 * The main program repeats trying to perform events until they are all blocked. This enumeration defines where the code
 * of each transition is kept.
 * </p>
 */
public enum EventTransitionForm {
    /** PLC code of an event transition is inlined in the main program. */
    CODE_IN_MAIN("code-in-main",
            "to put all event transitions code in the main program",
            "All event code in main program"),

    /**
     * Controllable events in a single scope are taken together and converted to a POU. In the same way a POU is created
     * for uncontrollable events in a single scope.
     */
    FUNCTIONS_FOR_SCOPE("functions-for-scope",
            "to combine the event transition code by event controllability for all events in a single scope",
            "Event code is combined into a function by event controllability and CIF scope"),

    /** PLC code of an event transition is stored in a POU. */
    CODE_IN_FUNCTION("code-in-function",
            "to put the event transition code for each event in its own function",
            "Each event in a separate function");

    /** Descriptive name of the option value. */
    public final String name;

    /** Short description of what the option generates. */
    public final String shortDescription;

    /** Description of the transition form. */
    public final String description;

    /**
     * Get the short description of the option.
     *
     * @return The short description of the option.
     */
    public String getOptionDescription() {
        return fmt("\"%s\" %s", name, shortDescription);
    }

    /**
     * Constructor of the {@link EventTransitionForm}.
     *
     * @param name Descriptive name of the option value.
     * @param shortDescription Short description of what the option generates.
     * @param description Longer description of the value.
     */
    private EventTransitionForm(String name, String shortDescription, String description) {
        this.name = name;
        this.shortDescription = shortDescription;
        this.description = description;
    }
}
