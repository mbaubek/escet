//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2023, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.plcgen.model.functions;

import org.eclipse.escet.cif.plcgen.model.types.PlcElementaryType;

/** Function description for functions where the result type cannot be derived from the parameter type. */
public class PlcExplicitOutTypeFunctionDescription extends PlcBasicFuncDescription {
    /**
     * Constructor of the {@link PlcExplicitOutTypeFunctionDescription} class.
     *
     * @param operation Semantic function operation that is performed in the function application.
     * @param funcName Name of the function.
     * @param childType Type of the function application child expression.
     * @param resultType Type of the function application result.
     */
    public PlcExplicitOutTypeFunctionDescription(PlcFuncOperation operation, String funcName,
            PlcElementaryType childType, PlcElementaryType resultType)
    {
        super(operation, funcName,
                new PlcParameterDescription[] {
                        new PlcParameterDescription("IN", PlcParamDirection.INPUT_ONLY, childType)},
                PlcBasicFuncDescription.PlcFuncNotation.NOT_INFIX, resultType, PlcFuncTypeExtension.NEVER);
    }
}
