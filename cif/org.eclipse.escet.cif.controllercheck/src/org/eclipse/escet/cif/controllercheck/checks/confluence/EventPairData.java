//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2022, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.controllercheck.checks.confluence;

import org.eclipse.escet.cif.bdd.spec.CifBddEdge;
import org.eclipse.escet.cif.bdd.spec.CifBddEdgeApplyDirection;
import org.eclipse.escet.cif.bdd.spec.CifBddSpec;
import org.eclipse.escet.cif.bdd.utils.BddUtils;
import org.eclipse.escet.cif.metamodel.cif.declarations.Event;
import org.eclipse.escet.common.java.Termination;

import com.github.javabdd.BDD;
import com.github.javabdd.BDDFactory;
import com.github.javabdd.BDDVarSet;

/** Data for an event pair being checked, sharing between checks. */
class EventPairData {
    /** The first event. */
    final Event event1;

    /** The second event. */
    final Event event2;

    /** The first edge. */
    final CifBddEdge edge1;

    /** The second edge. */
    final CifBddEdge edge2;

    /** The name of first event. */
    final String evt1Name;

    /** The name of second event. */
    final String evt2Name;

    /** The BDD factory to use. */
    final BDDFactory factory;

    /** The zero variables to old variables identity relations. */
    private final BDD zeroToOldVarRelations;

    /** The BDD variable set containing all old variables. */
    final BDDVarSet varSetOld;

    /** States where the guards of both edges are enabled, without zero variables. */
    private BDD commonEnabledGuardsNoZero;

    /** States where the guards of both edges are enabled, with zero variable relations. */
    private BDD commonEnabledGuardsWithZero;

    /**
     * The 'source states', the states where both events are enabled (but no transitions are taken for them just yet),
     * expressed in terms of 'zero' variables.
     */
    private BDD commonEnabledZeroStates;

    /** The states reachable from the common enabled states, by taking the first edge. */
    private BDD event1Done;

    /** The states reachable from the common enabled states, by taking the second edge. */
    private BDD event2Done;

    /** The states reachable from the common enabled states, by taking the first and second edges. */
    private BDD event12Done;

    /** The states reachable from the common enabled states, by taking the second and first edges. */
    private BDD event21Done;

    /**
     * Constructor for the {@link EventPairData} class.
     *
     * @param cifBddSpec The CIF/BDD specification.
     * @param event1 The first event.
     * @param event2 The second event.
     * @param edge1 The first edge.
     * @param edge2 The second edge.
     * @param evt1Name The name of first event.
     * @param evt2Name The name of second event.
     * @param zeroToOldVarRelations The zero variables to old variables identity relations.
     */
    EventPairData(CifBddSpec cifBddSpec, Event event1, Event event2, CifBddEdge edge1, CifBddEdge edge2,
            String evt1Name, String evt2Name, BDD zeroToOldVarRelations)
    {
        this.event1 = event1;
        this.event2 = event2;
        this.edge1 = edge1;
        this.edge2 = edge2;
        this.evt1Name = evt1Name;
        this.evt2Name = evt2Name;
        this.factory = cifBddSpec.factory;
        this.zeroToOldVarRelations = zeroToOldVarRelations;
        this.varSetOld = cifBddSpec.varSetOld;
    }

    /**
     * Returns the states where the guards of both edges are enabled, without zero variables.
     *
     * @return The states where the guards of both edges are enabled, without zero variables.
     */
    BDD getCommonEnabledGuardsNoZero() {
        if (commonEnabledGuardsNoZero == null) {
            commonEnabledGuardsNoZero = edge1.guard.and(edge2.guard);
        }
        return commonEnabledGuardsNoZero;
    }

    /**
     * Returns the states where the guards of both edges are enabled, with zero variable relations.
     *
     * @param termination The cooperative termination query function.
     * @return The states where the guards of both edges are enabled, with zero variable relations.
     */
    private BDD getCommonEnabledGuardsWithZero(Termination termination) {
        if (commonEnabledGuardsWithZero == null) {
            BDD commonEnabledGuardsNoZero = getCommonEnabledGuardsNoZero();
            termination.throwIfRequested();

            commonEnabledGuardsWithZero = zeroToOldVarRelations.and(commonEnabledGuardsNoZero);
        }
        return commonEnabledGuardsWithZero;
    }

    /**
     * Returns the 'source states', the states where both events are enabled (but no transitions are taken for them just
     * yet), expressed in terms of 'zero' variables.
     *
     * @param termination The cooperative termination query function.
     * @return The 'source states', expressed in terms of 'zero' variables.
     */
    BDD getCommonEnabledZeroStates(Termination termination) {
        if (commonEnabledZeroStates == null) {
            BDD commonEnabledGuardsWithZero = getCommonEnabledGuardsWithZero(termination);
            termination.throwIfRequested();

            commonEnabledZeroStates = commonEnabledGuardsWithZero.exist(varSetOld);
        }
        return commonEnabledZeroStates;
    }

    /**
     * Returns the states reachable from the common enabled states, by taking the first edge.
     *
     * @param termination The cooperative termination query function.
     * @return The states reachable from the common enabled states with zero variable relations, by taking the first
     *     edge.
     */
    BDD getEvent1Done(Termination termination) {
        if (event1Done == null) {
            BDD commonEnabledGuardsWithZero = getCommonEnabledGuardsWithZero(termination);
            termination.throwIfRequested();

            event1Done = edge1.apply(commonEnabledGuardsWithZero.id(), CifBddEdgeApplyDirection.FORWARD, null);
        }
        return event1Done;
    }

    /**
     * Returns the states reachable from the common enabled states, by taking the second edge.
     *
     * @param termination The cooperative termination query function.
     * @return The states reachable from the common enabled states with zero variable relations, by taking the second
     *     edge.
     */
    BDD getEvent2Done(Termination termination) {
        if (event2Done == null) {
            BDD commonEnabledGuardsWithZero = getCommonEnabledGuardsWithZero(termination);
            termination.throwIfRequested();

            event2Done = edge2.apply(commonEnabledGuardsWithZero.id(), CifBddEdgeApplyDirection.FORWARD, null);
        }
        return event2Done;
    }

    /**
     * Returns the states reachable from the common enabled states, by taking the first and second edges.
     *
     * @param termination The cooperative termination query function.
     * @return The states reachable from the common enabled states with zero variable relations, by taking the first and
     *     second edges.
     */
    BDD getEvent12Done(Termination termination) {
        if (event12Done == null) {
            BDD event1Done = getEvent1Done(termination);
            termination.throwIfRequested();

            BDD event1Enabled2 = event1Done.and(edge2.guard);
            termination.throwIfRequested();

            event12Done = (event1Enabled2.isZero()) ? factory.zero()
                    : edge2.apply(event1Enabled2.id(), CifBddEdgeApplyDirection.FORWARD, null);
            termination.throwIfRequested();

            event1Enabled2.free();
        }
        return event12Done;
    }

    /**
     * Returns the states reachable from the common enabled states, by taking the second and first edges.
     *
     * @param termination The cooperative termination query function.
     * @return The states reachable from the common enabled states with zero variable relations, by taking the second
     *     and first edges.
     */
    BDD getEvent21Done(Termination termination) {
        if (event21Done == null) {
            BDD event2Done = getEvent2Done(termination);
            termination.throwIfRequested();

            BDD event2Enabled1 = event2Done.and(edge1.guard);
            termination.throwIfRequested();

            event21Done = (event2Enabled1.isZero()) ? factory.zero()
                    : edge1.apply(event2Enabled1.id(), CifBddEdgeApplyDirection.FORWARD, null);
            termination.throwIfRequested();

            event2Enabled1.free();
        }
        return event21Done;
    }

    /** Free all BDDs of this event pair data. This method can safely be invoked multiple times. */
    void free() {
        commonEnabledGuardsNoZero = BddUtils.free(commonEnabledGuardsNoZero);
        commonEnabledGuardsWithZero = BddUtils.free(commonEnabledGuardsWithZero);
        commonEnabledZeroStates = BddUtils.free(commonEnabledZeroStates);
        event1Done = BddUtils.free(event1Done);
        event2Done = BddUtils.free(event2Done);
        event12Done = BddUtils.free(event12Done);
        event21Done = BddUtils.free(event21Done);
    }
}
