//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.controllercheck.mdd;

import static org.eclipse.escet.common.java.Lists.list;
import static org.eclipse.escet.common.java.Maps.map;
import static org.eclipse.escet.common.java.Maps.mapc;
import static org.eclipse.escet.common.java.Sets.intersection;
import static org.eclipse.escet.common.java.Sets.set;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.escet.cif.common.CifCollectUtils;
import org.eclipse.escet.cif.common.CifEventUtils;
import org.eclipse.escet.cif.common.CifTextUtils;
import org.eclipse.escet.cif.metamodel.cif.Specification;
import org.eclipse.escet.cif.metamodel.cif.automata.Assignment;
import org.eclipse.escet.cif.metamodel.cif.automata.Automaton;
import org.eclipse.escet.cif.metamodel.cif.automata.Edge;
import org.eclipse.escet.cif.metamodel.cif.automata.Location;
import org.eclipse.escet.cif.metamodel.cif.automata.Update;
import org.eclipse.escet.cif.metamodel.cif.declarations.Declaration;
import org.eclipse.escet.cif.metamodel.cif.declarations.Event;
import org.eclipse.escet.cif.metamodel.cif.expressions.DiscVariableExpression;
import org.eclipse.escet.cif.metamodel.cif.expressions.Expression;
import org.eclipse.escet.common.java.Assert;
import org.eclipse.escet.common.java.Termination;
import org.eclipse.escet.common.java.output.DebugNormalOutput;
import org.eclipse.escet.common.multivaluetrees.Node;
import org.eclipse.escet.common.multivaluetrees.Tree;

/** MDD-based representation of a CIF specification, for use by MDD-based checks. */
public class CifMddSpec {
    /** Index for denoting reading a variable. */
    public static final int READ_INDEX = 0;

    /** Index for denoting writing a variable. */
    public static final int WRITE_INDEX = 1;

    /** Number of variable indices that exist. */
    private static final int NUM_INDICES = 2;

    /** Cooperative termination query function. */
    private final Termination termination;

    /** Callback to send normal output to the user. */
    private final DebugNormalOutput normalOutput;

    /** Callback to send debug output to the user. */
    private final DebugNormalOutput debugOutput;

    /** Automata of the specification. */
    private List<Automaton> automata;

    /** The set of used controllable events. */
    private Set<Event> controllableEvents = set();

    /** Discrete and input variables of the specification. */
    private List<Declaration> variables;

    /** Global guard of each used controllable event. */
    private Map<Event, Node> globalGuardsByEvent = map();

    /** Updated variables of each used controllable event. */
    private Map<Event, Set<Declaration>> updatedVariablesByEvent = map();

    /** Builder for the MDD tree. */
    private MddSpecBuilder builder;

    /**
     * Constructor for the {@link CifMddSpec} class.
     *
     * @param termination Cooperative termination query function.
     * @param normalOutput Callback to send normal output to the user.
     * @param debugOutput Callback to send debug output to the user.
     */
    public CifMddSpec(Termination termination, DebugNormalOutput normalOutput, DebugNormalOutput debugOutput) {
        this.termination = termination;
        this.normalOutput = normalOutput;
        this.debugOutput = debugOutput;
    }

    /**
     * Extract the events and variables structure from the CIF specification, and organize it into a form usable for the
     * checks.
     *
     * @param spec Specification to analyze.
     */
    public void compute(Specification spec) {
        // Collect automata and controllable events.
        automata = CifCollectUtils.collectAutomata(spec, list());
        Assert.check(!automata.isEmpty());
        Set<Event> allControllableEvents = CifCollectUtils.collectControllableEvents(spec, set());
        if (allControllableEvents.isEmpty()) {
            // All MDD-based checks trivially hold.
            debugOutput.line("No controllable events.");
            return;
        }

        // Collect variables.
        variables = CifCollectUtils.collectDiscAndInputVariables(spec, list());
        termination.throwIfRequested();

        // Construct the MDD tree instance.
        MddCifVarInfoBuilder cifVarInfoBuilder = new MddCifVarInfoBuilder(NUM_INDICES);
        cifVarInfoBuilder.addVariablesGroupOnVariable(variables);
        builder = new MddSpecBuilder(cifVarInfoBuilder, READ_INDEX, WRITE_INDEX);
        termination.throwIfRequested();

        // Compute global guards and updated variables for each event.
        boolean first = true;
        for (Automaton aut: automata) {
            if (!first) {
                debugOutput.line();
            }
            first = false;

            debugOutput.line("Analyzing %s:", CifTextUtils.getComponentText1(aut));
            Set<Event> controllableAutEvents = intersection(CifEventUtils.getAlphabet(aut), allControllableEvents);

            debugOutput.inc();

            processAutomaton(aut, controllableAutEvents);

            debugOutput.dec();
            termination.throwIfRequested();
        }
    }

    /**
     * Analyze one automaton and add its information to the global collections.
     *
     * @param aut Automaton to analyze.
     * @param controllableAutEvents Controllable events of the automaton.
     */
    private void processAutomaton(Automaton aut, Set<Event> controllableAutEvents) {
        Tree tree = builder.tree;

        // Guards of the automaton.
        Map<Event, Node> autGuards = mapc(controllableAutEvents.size());

        boolean debugPrinted = false;

        // Initialize the automaton data for all automata events, and extend the global data for new events.
        for (Event evt: controllableAutEvents) {
            debugOutput.line("Initializing the automaton data for event \"%s\".", CifTextUtils.getAbsName(evt));
            debugPrinted = true;

            autGuards.put(evt, Tree.ZERO);

            if (!controllableEvents.contains(evt)) {
                controllableEvents.add(evt);
                globalGuardsByEvent.put(evt, Tree.ONE);
                updatedVariablesByEvent.put(evt, set());
            }
            termination.throwIfRequested();
        }

        // Process the locations and edges.
        if (!controllableAutEvents.isEmpty()) {
            for (Location loc: aut.getLocations()) {
                debugOutput.line("Processing edges from %s.", CifTextUtils.getLocationText2(loc));
                debugPrinted = true;

                for (Edge edge: loc.getEdges()) {
                    // Filter on relevant events.
                    Set<Event> controllableEdgeEvents = intersection(CifEventUtils.getEvents(edge),
                            controllableAutEvents);
                    if (controllableEdgeEvents.isEmpty()) {
                        continue;
                    }

                    // Compute guard of the edge.
                    Node guard = computeGuard(edge);
                    termination.throwIfRequested();

                    // Compute and mark the updated variables of the edge.
                    markUpdatedVars(edge, controllableEdgeEvents);
                    termination.throwIfRequested();

                    // Add the guard as alternative to the relevant events of the edge.
                    for (Event evt: controllableEdgeEvents) {
                        Node autGuard = autGuards.get(evt);
                        autGuards.put(evt, tree.disjunct(autGuard, guard));
                        termination.throwIfRequested();
                    }
                }
            }
        }

        // At global level, guards of each event must synchronize between participating automata.
        for (Event autEvent: controllableAutEvents) {
            debugOutput.line("Updating global guards for event \"%s\".",
                    CifTextUtils.getAbsName(autEvent));
            debugPrinted = true;

            Node globGuard = globalGuardsByEvent.get(autEvent);
            globalGuardsByEvent.put(autEvent, tree.conjunct(globGuard, autGuards.get(autEvent)));
            termination.throwIfRequested();
        }

        if (!debugPrinted) {
            debugOutput.line("Nothing to process.");
        }
    }

    /**
     * Convert the guard of an edge to an MDD relation.
     *
     * @param edge Edge to use.
     * @return The guard as an MDD tree.
     */
    private Node computeGuard(Edge edge) {
        Node guard = Tree.ONE;
        for (Expression grd: edge.getGuards()) {
            Node node = builder.getExpressionConvertor().convert(grd).get(1);
            termination.throwIfRequested();

            guard = builder.tree.conjunct(guard, node);
            termination.throwIfRequested();
        }
        return guard;
    }

    /**
     * Mark the variables assigned by these updates of the edge as being updated by the events on the edge.
     *
     * @param edge Edge to use.
     * @param controllableEdgeEvents The controllable events of the edge.
     */
    private void markUpdatedVars(Edge edge, Set<Event> controllableEdgeEvents) {
        // Collect assigned variables.
        Set<Declaration> assignedVariables = set();
        for (Update upd: edge.getUpdates()) {
            Assert.check(upd instanceof Assignment);
            Assignment asg = (Assignment)upd;
            Assert.check(asg.getAddressable() instanceof DiscVariableExpression);
            Declaration lhs = ((DiscVariableExpression)asg.getAddressable()).getVariable();
            assignedVariables.add(lhs);
        }

        // Mark the assigned variables as being updated by the event.
        for (Event evt: controllableEdgeEvents) {
            updatedVariablesByEvent.get(evt).addAll(assignedVariables);
        }
    }

    /**
     * Returns the cooperative termination query function.
     *
     * @return The cooperative termination query function.
     */
    public Termination getTermination() {
        return termination;
    }

    /**
     * Returns the callback to send debug output to the user.
     *
     * @return The callback.
     */
    public DebugNormalOutput getDebugOutput() {
        return debugOutput;
    }

    /**
     * Returns the callback to send normal output to the user.
     *
     * @return The callback.
     */
    public DebugNormalOutput getNormalOutput() {
        return normalOutput;
    }

    /**
     * Get the automata of the specification.
     *
     * @return The automata of the specification.
     */
    public List<Automaton> getAutomata() {
        return Collections.unmodifiableList(automata);
    }

    /**
     * Get the used controllable events of the specification.
     *
     * @return The used controllable events of the specification.
     */
    public Set<Event> getControllableEvents() {
        return Collections.unmodifiableSet(controllableEvents);
    }

    /**
     * Get the global guard for each used controllable event.
     *
     * @return The global guard for each used controllable event.
     */
    public Map<Event, Node> getGlobalGuardsByEvent() {
        return Collections.unmodifiableMap(globalGuardsByEvent);
    }

    /**
     * Get the updated variables of each used controllable event.
     *
     * @return The updated variables of each used controllable event.
     */
    public Map<Event, Set<Declaration>> getUpdatedVariablesByEvent() {
        return Collections.unmodifiableMap(updatedVariablesByEvent);
    }

    /**
     * Get the builder of the MDD trees.
     *
     * @return The builder of the MDD trees.
     */
    public MddSpecBuilder getBuilder() {
        return builder;
    }
}
