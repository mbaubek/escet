//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.releng.problemreporter.plugin;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.apache.commons.text.StringEscapeUtils;
import org.apache.maven.execution.MavenSession;
import org.eclipse.escet.releng.problemreporter.plugin.collectors.AsciiDocSourceCheckerProblemCollector;
import org.eclipse.escet.releng.problemreporter.plugin.problems.Problem;
import org.eclipse.escet.releng.problemreporter.plugin.problems.Problems;

import com.google.common.io.CharStreams;

/** Maven build problem reporter. */
public class MavenBuildProblemReporter {
    /** Constructor for the {@link MavenBuildProblemReporter} class. */
    private MavenBuildProblemReporter() {
        // Static class.
    }

    /**
     * Report all problems that occurred during the build.
     *
     * @param session The Maven session for which to report the problems.
     * @param reportPath The path to the report to write.
     * @param infoLogger Info logger.
     * @param errorLogger Error logger.
     * @return The number of problems.
     * @throws IOException In case of an I/O error.
     */
    public static long report(MavenSession session, Path reportPath, Consumer<String> infoLogger,
            Consumer<String> errorLogger) throws IOException
    {
        // Collect all the problems.
        Problems problems = new Problems();
        new AsciiDocSourceCheckerProblemCollector().collect(session, problems);

        // Log number of problems that were found.
        long problemCount = problems.getCount();
        if (problemCount == 0) {
            infoLogger.accept("No problems found.");
        } else {
            errorLogger.accept(
                    String.format(Locale.US, "%d problem%s found.", problemCount, (problemCount == 1) ? "" : "s"));
        }

        // Write problems to report file.
        if (problemCount > 0) {
            Files.createDirectories(reportPath.getParent());
            writeReport(session, problems, reportPath);
        }

        // Return the number of problems.
        return problemCount;
    }

    /**
     * Write an HTML file with the report.
     *
     * @param session The Maven session for which to report the problems.
     * @param problems The problems.
     * @param reportPath The path to the report file.
     * @throws IOException In case of an I/O error.
     */
    private static void writeReport(MavenSession session, Problems problems, Path reportPath) throws IOException {
        // Get report template.
        ClassLoader classLoader = MavenBuildProblemReporter.class.getClassLoader();
        String resourcePath = MavenBuildProblemReporter.class.getPackageName().replace('.', '/') + "/report.html";
        String template;
        try (InputStream stream = new BufferedInputStream(classLoader.getResourceAsStream(resourcePath));
             Reader reader = new InputStreamReader(stream, StandardCharsets.UTF_8))
        {
            template = CharStreams.toString(reader);
        } catch (IOException e) {
            throw new RuntimeException("Failed to get report template.", e);
        }

        // Replace 'build'.
        String buildUrl = session.getSystemProperties().getProperty("env.BUILD_URL");
        String buildTxt = (buildUrl == null) ? "<span class=\"disabled\">n/a (local build)</span>"
                : String.format(Locale.US, "<a href=\"%s\">%s</a>", StringEscapeUtils.escapeHtml4(buildUrl),
                        StringEscapeUtils.escapeHtml4(buildUrl));
        template = template.replace("${build}", buildTxt);

        // Replace 'startTime'.
        Date buildTime = session.getStartTime();
        String startTimeTxt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z").format(buildTime);
        template = template.replace("${startTime}", startTimeTxt);

        // Replace 'problemCount'.
        long problemCount = problems.getCount();
        template = template.replace("${problemCount}", String.format(Locale.US, "%,d", problemCount));

        // Replace 'details'.
        List<String> detailLines = new ArrayList<>();
        if (problemCount > 0) {
            for (String project: problems.getSortedProjects()) {
                // Add project entry.
                detailLines.add("");
                detailLines.add("<hr>");
                detailLines.add("<p><strong>Project " + StringEscapeUtils.escapeHtml4(project) + "</strong></p>");

                // Add files of the project.
                for (String filePath: problems.getSortedFilePaths(project)) {
                    // Get file lines.
                    List<String> fileLines = Files.readAllLines(Paths.get(filePath), StandardCharsets.UTF_8);

                    // Add file entry.
                    Path rootPath = Paths.get(session.getExecutionRootDirectory());
                    Path relPath = rootPath.relativize(Paths.get(filePath));
                    detailLines.add("<p>File " + StringEscapeUtils.escapeHtml4(relPath.toString()) + "</p>");
                    detailLines.add("<ul>");

                    // Get problems of the file, per line.
                    List<Problem> fileProblems = problems.getSortedProblems(project, filePath);
                    Map<Integer, List<Problem>> perLineProblems = fileProblems.stream()
                            .collect(Collectors.groupingBy(p -> p.lineNr));

                    // Add problems, per line.
                    for (int lineNr: perLineProblems.keySet().stream().sorted().toList()) {
                        // Get information.
                        int lineIdx = lineNr - 1;
                        List<Problem> lineProblems = perLineProblems.get(lineNr);

                        // Add item for the line.
                        detailLines.add("<li class=\"line\">");

                        // Add heading for the line. Only include problem if there is just one problem at the line.
                        if (lineProblems.size() == 1) {
                            Problem problem = lineProblems.get(0);
                            detailLines.add(String.format(Locale.US, "Line %d, column %d: %s", problem.lineNr,
                                    problem.columnNr, StringEscapeUtils.escapeHtml4(problem.message)));
                        } else {
                            detailLines.add(String.format(Locale.US, "Line %d", lineNr));
                        }

                        // Add the line.
                        detailLines.add("<br>");
                        if (lineIdx < 0 || lineIdx >= fileLines.size()) {
                            detailLines.add(String.format(Locale.US, "<span class=\"error\">Invalid line %d.</span>",
                                    lineNr));
                        } else {
                            String lineHtml = getHtmlProblemLine(fileLines.get(lineIdx), lineProblems);
                            detailLines.add(
                                    "<pre class=\"line\"><span class=\"line-content\">" + lineHtml + "</span></pre>");
                        }

                        // If there are multiple problems at the line, report each of them.
                        if (lineProblems.size() > 1) {
                            detailLines.add("<ul>");
                            for (Problem problem: lineProblems) {
                                detailLines.add(String.format(Locale.US, "<li>Column %d: %s</li>", problem.columnNr,
                                        StringEscapeUtils.escapeHtml4(problem.message)));
                            }
                            detailLines.add("</ul>");
                        }

                        // End of item for the line.
                        detailLines.add("</li>");
                    }
                    detailLines.add("</ul>");
                }
            }
        }
        template = template.replace("${details}", String.join("\n", detailLines));

        // Write report to file.
        Files.writeString(reportPath, template);
    }

    /**
     * Get HTML text for a problem line, with problem columns highlighted.
     *
     * @param line The text of the line.
     * @param lineProblems The problems on the line.
     * @return The HTML text of the problem line.
     */
    private static String getHtmlProblemLine(String line, List<Problem> lineProblems) {
        // Get unique 0-based problem column indices, in ascending sorted order.
        Set<Integer> columnIdxs = lineProblems.stream().map(p -> p.columnNr - 1).sorted()
                .collect(Collectors.toCollection(() -> new LinkedHashSet<>()));

        // Process the line, per problem column.
        StringBuilder html = new StringBuilder();
        int curIdx = 0;
        for (int columnIdx: columnIdxs) {
            // Add text before the problem column.
            if (curIdx < columnIdx) {
                html.append("<span class=\"regular\">"
                        + StringEscapeUtils.escapeHtml4(line.substring(curIdx, columnIdx)) + "</span>");
            }

            // Add highlighted problem column.
            html.append("<span class=\"highlight\">"
                    + StringEscapeUtils.escapeHtml4(line.substring(columnIdx, columnIdx + 1)) + "</span>");

            // Proceed after the problem column.
            curIdx = columnIdx + 1;
        }

        // Add remaining text, after last problem column.
        if (curIdx < line.length()) {
            html.append("<span class=\"regular\">" + StringEscapeUtils.escapeHtml4(line.substring(curIdx)) + "</span>");
        }

        // Return the HTML text.
        return html.toString();
    }
}
