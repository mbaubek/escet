//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.releng.problemreporter.plugin;

import java.io.File;
import java.util.List;
import java.util.Locale;

import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import com.google.common.base.Verify;

/**
 * Maven build problem reporter Maven mojo.
 *
 * <p>
 * Reports problems collected during the build, at the end of the build. Rather than failing the build at the first
 * problem, this allows to report all problems during a single build, such that they can also be fixed in one go. This
 * saves time.
 * </p>
 */
@Mojo(name = "report-problems")
public class MavenBuildProblemReporterMojo extends AbstractMojo {
    /** The Maven session. */
    @Parameter(defaultValue = "${session}", readonly = true, required = true)
    private MavenSession session;

    /** The path to the HTML file to which to write the report. */
    @Parameter(required = true)
    private File reportPath;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        // Make sure we run as last project in the Maven session, such that we can report all problems.
        List<MavenProject> projects = session.getProjects();
        int actualProjectNr = projects.indexOf(session.getCurrentProject()) + 1;
        int expectedProjectNr = projects.size();
        Verify.verify(actualProjectNr == expectedProjectNr,
                "Maven build reporter must be last project of the Maven session (%s/%s), but it is not (%s/%s).",
                expectedProjectNr, expectedProjectNr, actualProjectNr, expectedProjectNr);

        // Collect all problems and write them to a report.
        Log log = getLog();
        long problemCount;
        try {
            problemCount = MavenBuildProblemReporter.report(session, reportPath.toPath(), log::info, log::error);
        } catch (Throwable e) {
            log.error(e);
            throw new MojoExecutionException("Error while executing Maven plugin.", e);
        }

        // Fail the build if there were any problems.
        if (problemCount > 0) {
            throw new MojoFailureException(String.format(Locale.US,
                    "%,d problem%s occurred during the build. Check the generated problem report at %s for details.",
                    problemCount, (problemCount == 1) ? "" : "s", reportPath));
        }
    }
}
