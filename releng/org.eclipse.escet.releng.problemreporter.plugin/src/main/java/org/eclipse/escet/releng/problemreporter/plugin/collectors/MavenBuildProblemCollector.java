//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2025 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.releng.problemreporter.plugin.collectors;

import java.io.IOException;

import org.apache.maven.execution.MavenSession;
import org.eclipse.escet.releng.problemreporter.plugin.problems.Problems;

/** Maven build problem collector. */
public interface MavenBuildProblemCollector {
    /**
     * Collect problems that occurred during the Maven build.
     *
     * @param session The Maven session for which to collect the problems.
     * @param problems The collection of problems collected so far, to be extended with newly collected problems.
     * @throws IOException In case of an I/O error.
     */
    public void collect(MavenSession session, Problems problems) throws IOException;
}
